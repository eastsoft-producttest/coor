﻿#ifndef COM_PORT_ESPD_H
#define COM_PORT_ESPD_H

#include <QObject>
#include "qextserialport.h"
#include <QThread>
#include <QDebug>

const QRegExp reg_End("sysadm@SCT230A:~[$]");
const QRegExp reg_inputPassword("password for sysadm: ");
const QRegExp reg_login("(SCT230A)\\s+(login)");
const QRegExp reg_loginPassword("Password:");

typedef struct{
    const QString StartContainer = "sudo container start c_base\n";
    const QString StopContainer = "sudo container stop c_base\n";
    const QString CheckContainer = "sudo docker ps -a\n";
    const QString StartApp = "sudo nohup ./adjjc > adjjc.log &\n";
    const QString StopApp = "sudo kill -9 %1\n";  //%1为进程号
    const QString CheckApp = "ps -aux|grep adjjc\n";
    const QString Username = "sysadm\n";
    const QString Password = "Zxbdt@729.TTU\n";
}ESPD_COMM;

class com_port_espd : public QextSerialPort
{
    Q_OBJECT
public:
    com_port_espd(const QString& sPortName, int serial);
    ~com_port_espd();

private:
    QString     m_sPortName;    //串口名
    int         m_nSerial = -1; //串口所对应表位
    ESPD_COMM   m_command;      //指令
    QString     m_recieve;      //串口数据接收
    int m_PID;                  //校表APP进程号
public:
    int start_container(QString &res);      //开启容器
    int stop_container(QString &res);       //停止容器
    int check_container(QString &res, bool isopen);      //查看容器,参数2可选择查看开启状态还是关闭状态
    int start_app(QString &res);            //开启App
    int stop_app(QString &res);             //停止APP
    int check_app(QString &res);            //查看adjjc进程
    int login();                            //登录
    int send_wait(QString str, QString &res);

};

#endif // COM_PORT_ESPD_H
