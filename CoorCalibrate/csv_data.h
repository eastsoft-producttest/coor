﻿#ifndef CSV_DATA_H
#define CSV_DATA_H

#include <QObject>
#include <QFileInfo>
#include <QVector>
#include <memory>
#include <QMutex>
/*
 * 类名：csv_data
 * 功能：提供存储csv格式文件存储功能
 * */

class csv_data : public QObject
{
    Q_OBJECT
public:
    //文件名、表头
    explicit csv_data(QString sFile_Head = "",QString sHead="");
    ~csv_data();
public:
    void set_headers(QVector<QString> headers);
    void set_rowdatas(QVector<QString> datas);
    void set_rowdatas(QStringList datas);
    void set_appendData(QString data);

private:
    QFileInfo m_fileInfo;
    QVector<QString> m_headers;
    QFile* m_csvFile;
    QMutex m_Mutex;

};

#endif // CSV_DATA_H
