#ifndef CALICOORPROCESS_H
#define CALICOORPROCESS_H

#include <QObject>
#include "com_port_coor.h"
#include <QList>
#include <QThread>
#include "simu_coor.h"
#include "log_utility/logModule.h"

#define MAX_COOR_NUM 32

class CaliCoorProcess : public QObject
{
    Q_OBJECT
public:
    explicit CaliCoorProcess(QObject *parent = nullptr);

private:
    //设备在线位图
    unsigned int m_unOnLineBitMap;
    simu_coor *m_pSimu_coor;
    QList<com_port_coor*> m_pCoorPortList;
    QList<QString> m_sPortNameList;
    QList<QThread*> m_pThreadList;
signals:

public slots:
};

#endif // CALICOORPROCESS_H
