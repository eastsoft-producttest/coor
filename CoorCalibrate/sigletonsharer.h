﻿#ifndef SIGLETONSHARER_H
#define SIGLETONSHARER_H

#include <QObject>
#include "ini_profile.h"
#include "mes/json_format.h"
#include "mes/soapPass_USCOREStationSoapProxy.h"
//#include "BoxMes/MesNewWebServiceSoap.nsmap"
//#include "BoxMes/soapMesNewWebServiceSoapProxy.h"

class SigletonSharer : public QObject
{
    Q_OBJECT
private:
    explicit SigletonSharer(QObject *parent = nullptr);
    static SigletonSharer* m_pInstance;


public:
    ~SigletonSharer();

    void soap_set_time(int nWaitTime_s);

public:
    static SigletonSharer* getInstance();

    T_STATION_INFO m_tStationInfo;
    int m_BarReadWay;   //条码读取方式
    int m_MeterType;    //校表类型，0为集中器，1为智能终端
    volatile int m_nStop = 0;
    int m_PackInterval = 0; //上传包装线间隔
    int m_maxFailNum = 0;   //最大失败数量
    int m_FailNum = 0;      //校表失败数量
signals:

public slots:
};

#endif // SIGLETONSHARER_H
