﻿#include "ini_profile.h"
#include <QTextCodec>
#include <QFile>
#include <QMessageBox>
#include <QDebug>
ini_profile::ini_profile(QObject *parent)
{
    //设置map
    if(!QFile::exists(sIniPath))
    {
        QMessageBox::warning(nullptr,"提示","系统文件不完整，请检查！！");
    }
    m_set = new QSettings(sIniPath, QSettings::IniFormat);
    m_set->setIniCodec(QTextCodec::codecForName("UTF-8"));
    //refresh();
}

ini_profile::~ini_profile()
{
    delete m_set;
    m_set = nullptr;
}

void ini_profile::set_ini(QString skey, QString svalue)
{
    m_set->setValue(skey, svalue);
}

QString ini_profile::get_ini(QString skey)
{
    return m_set->value(skey).toString();
}


int ini_profile::get_maxline()
{
    return get_ini("view/list_maxline").toInt();
}

void ini_profile::set_maxline(QString snline)
{
     set_ini("view/list_maxline", snline);
}

//**********表台设置**********//
void ini_profile::set_plat_NO(QString sNo)
{
    set_ini("station/PLAT_NO",sNo);
}
QString ini_profile::get_plat_NO()
{
    return get_ini("station/PLAT_NO");
}
void ini_profile::set_scanner_com(QString sCom)
{
    set_ini("COORCOMPORT/ScanPort",sCom);
}
QString ini_profile::get_scanner_com()
{
    return get_ini("COORCOMPORT/ScanPort");
}
void ini_profile::set_dev_com(QString sCom)
{
    set_ini("COORCOMPORT/DevPort", sCom);
}
QString ini_profile::get_dev_com()
{
    return get_ini("COORCOMPORT/DevPort");
}
void ini_profile::set_work_pattern(QString sPattern)
{
    set_ini("station/WORK_PATTERN", sPattern);
}
int  ini_profile::get_work_pattern()
{
    QString s = get_ini("station/WORK_PATTERN");
    s.remove(" ");
    s = s.left(1);
    return s.toInt();
}
void ini_profile::set_standardMeter(QString meter)
{
    set_ini("station/StandardMeter", meter);
}
QString ini_profile::get_standardMeter()
{
    return get_ini("station/StandardMeter");
}

void ini_profile::set_BarReadWay(int way)
{
    set_ini("station/BarReadWay", QString::number(way));
}
int ini_profile::get_BarReadWay()
{
    return get_ini("station/BarReadWay").toInt();
}
//**********MES设置**********//
void ini_profile::get_MES_setting(T_STATION_INFO& tStationInfo)
{
    tStationInfo.m_sWorkStation = get_ini("station/work_center");
    tStationInfo.m_sWorkStation_pack = get_ini("station/work_center_pack");
    tStationInfo.m_sMo = get_ini("station/order_num");
    tStationInfo.m_sOperator = get_ini("station/operator");
    //qDebug() << get_ini("station/MesEnable") << get_ini("station/PassPack");
    tStationInfo.m_EnableMes = get_ini("MES/MesEnable").toInt();
    tStationInfo.m_PassPack = get_ini("MES/PassPack").toInt();
    //tStationInfo.m_StandardMeter = get_ini("station/StandardMeter");
}

void ini_profile::set_work_center(QString wc_msg)
{
    set_ini("station/work_center", wc_msg);
}

void ini_profile::set_work_center_pack(QString wc_msg)
{
    set_ini("station/work_center_pack", wc_msg);
}

void ini_profile::set_order_num(QString order_num)
{
    set_ini("station/order_num", order_num);
}

void ini_profile::set_operator(QString oper)
{
    set_ini("station/operator", oper);
}

void ini_profile::set_enable_mes(bool b)
{
    if(b)
        set_ini("MES/MesEnable", QString::number(1));
    else
        set_ini("MES/MesEnable", QString::number(0));
}
void ini_profile::set_enable_passpack(bool b)
{
    if(b)
        set_ini("MES/PassPack", QString::number(1));
    else
        set_ini("MES/PassPack", QString::number(0));
}



//设置bitmap
void ini_profile::set_online_bitmap(unsigned int nBit)
{
    unsigned int iBitmaph = (nBit >> 16) & 0x0000ffff;
    unsigned int iBitmapl = nBit & 0x0000ffff;
    set_ini("STATE/online_bipmap_h",QString::number(iBitmaph));
    set_ini("STATE/online_bipmap_l",QString::number(iBitmapl));
}

unsigned int ini_profile::get_online_bitmap()
{
    unsigned int iBitmaph = get_ini("STATE/online_bipmap_h").toInt();
    unsigned int iBitmapl = get_ini("STATE/online_bipmap_l").toInt();
    unsigned int uBitMap = 0;
    uBitMap |= iBitmaph << 16;
    uBitMap |= iBitmapl;
    return uBitMap;
}

void ini_profile::set_com_port_list(const QStringList &sComList)
{
    QString sCom;
    for(QString s : sComList)
    {
        sCom += s + " ";
    }
    sCom.remove(sCom.length() - 1, 1);
    set_ini("COORCOMPORT/ports",sCom);
}

QStringList ini_profile::get_com_port_list()
{
    QString sCom =  get_ini("COORCOMPORT/ports");
    QStringList list = sCom.split(" ");
    return list;
}

void ini_profile::set_ttu_com_port_list(const QStringList &sComList)
{
    QString sCom;
    for(QString s : sComList)
    {
        sCom += s + " ";
    }
    sCom.remove(sCom.length() - 1, 1);
    set_ini("COORCOMPORT/ttu_ports",sCom);
}

QStringList ini_profile::get_ttu_com_port_list()
{
    QString sCom =  get_ini("COORCOMPORT/ttu_ports");
    QStringList list = sCom.split(" ");
    return list;
}

void ini_profile::set_online(const int nOnline)
{
    set_ini("mes/on_line",QString::number(nOnline));
}

int ini_profile::get_online()
{
    return get_ini("mes/on_line").toInt();
}




////校检表参数
//double ini_profile::get_voltErrorLimit()
//{
//    return get_ini("ST_VAL/VoltErrLimit").toDouble();
//}
//void   ini_profile::set_voltErrorLimit(QString sVal)
//{
//    set_ini("ST_VAL/voltErrLimit", sVal);
//}

//double ini_profile::get_CurrErrorLimit()
//{
//    return get_ini("ST_VAL/CurrErrLimit").toDouble();
//}
//void   ini_profile::set_CurrErrorLimit(QString sVal)
//{
//    set_ini("ST_VAL/CurrErrLimit", sVal);
//}

//double ini_profile::get_errLimitQZ()
//{
//    return get_ini("ST_VAL/errLimitQZ").toDouble();
//}
//void   ini_profile::set_errLimitQZ(QString sVal)
//{
//    set_ini("ST_VAL/errLimitQZ", sVal);
//}

//double ini_profile::get_errLimitQX()
//{
//    return get_ini("ST_VAL/errLimitQX").toDouble();
//}
//void   ini_profile::set_errLimitQX(QString sVal)
//{
//    set_ini("ST_VAL/errLimitQX", sVal);
//}

//double ini_profile::get_StdVolt()
//{
//    return get_ini("ST_VAL/StdVolt").toDouble();
//}
//void   ini_profile::set_StdVolt(QString sVal)
//{
//    set_ini("ST_VAL/StdVolt", sVal);
//}

//double ini_profile::get_StdCurr()
//{
//    return get_ini("ST_VAL/StdCurr").toDouble();
//}
//void   ini_profile::set_StdCurr(QString sVal)
//{
//    set_ini("ST_VAL/StdCurr", sVal);
//}

//double ini_profile::get_MaxCurr()
//{
//    return get_ini("ST_VAL/MaxCurr").toDouble();
//}
//void   ini_profile::set_MaxCurr(QString sVal)
//{
//    set_ini("ST_VAL/MaxCurr", sVal);
//}

//double ini_profile::get_Constant()
//{
//    return get_ini("ST_VAL/Constant").toDouble();
//}
//void   ini_profile::set_Constant(QString sVal)
//{
//    set_ini("ST_VAL/Constant", sVal);
//}

//int    ini_profile::get_Pause()
//{
//    return get_ini("ST_VAL/Pause").toInt();
//}
//void   ini_profile::set_Pause(QString sVal)
//{
//    set_ini("ST_VAL/Pause", sVal);
//}

//int    ini_profile::get_TheoryFreq()
//{
//    return get_ini("ST_VAL/TheoryFreq").toInt();
//}
//void   ini_profile::set_TheoryFreq(QString sVal)
//{
//    set_ini("ST_VAL/TheoryFreq", sVal);
//}
T_COOR_CALI_PARA ini_profile::get_coor_cali_para()
{
    T_COOR_CALI_PARA para;
    para.m_nStdVolt = get_ini("ST_VAL/StdVolt");
    para.m_nStdCurr = get_ini("ST_VAL/StdCurr");
    para.m_nImax = get_ini("ST_VAL/Imax");
    para.m_nVoltErrLimit = get_ini("ST_VAL/VoltErrLimit");
    para.m_nCurrErrLimit = get_ini("ST_VAL/CurrErrLimit");
    para.m_nImaxErrLimit = get_ini("ST_VAL/ImaxErrLimit");
    para.m_nLightErrLimit = get_ini("ST_VAL/LightErrLimit");
    para.m_nHexiangErrLimit = get_ini("ST_VAL/HexiangErrLimit");
    para.m_nReactiveErrLimit = get_ini("ST_VAL/ReactiveErrLimit");
    para.m_nConstant = get_ini("ST_VAL/Constant");
    para.m_nPluse = get_ini("ST_VAL/Pluse");
    para.m_nStdClockFreq = get_ini("ST_VAL/StdClockFreq");
    para.m_nClockErrLimit = get_ini("ST_VAL/ClockErrLimit");
    para.m_nClockTestDuration = get_ini("ST_VAL/ClockTestDuration");
    //para.m_nStdDevPort = get_ini("COORCOMPORT/DevPort");
    return para;
}
void ini_profile::set_coor_cali_para(const T_COOR_CALI_PARA& para)
{
    set_ini("ST_VAL/StdVolt", para.m_nStdVolt);
    set_ini("ST_VAL/StdCurr", para.m_nStdCurr);
    set_ini("ST_VAL/Imax", para.m_nImax);
    set_ini("ST_VAL/VoltErrLimit", para.m_nVoltErrLimit);
    set_ini("ST_VAL/CurrErrLimit", para.m_nCurrErrLimit);
    set_ini("ST_VAL/ImaxErrLimit", para.m_nImaxErrLimit);
    set_ini("ST_VAL/LightErrLimit", para.m_nLightErrLimit);
    set_ini("ST_VAL/HexiangErrLimit", para.m_nHexiangErrLimit);
    set_ini("ST_VAL/ReactiveErrLimit", para.m_nReactiveErrLimit);
    set_ini("ST_VAL/Constant", para.m_nConstant);
    set_ini("ST_VAL/Pause", para.m_nPluse);
    set_ini("ST_VAL/StdClockFreq", para.m_nStdClockFreq);
    set_ini("ST_VAL/ClockErrLimit", para.m_nClockErrLimit);

    set_ini("ST_VAL/ClockTestDuration", para.m_nClockTestDuration);
    //set_ini("COORCOMPORT/DevPort",  para.m_nStdDevPort);
}



void ini_profile::set_pack_interval(int interval)
{
    set_ini("TEST/PackInterval", QString::number(interval));
}

int ini_profile::get_pack_interval()
{
    return get_ini("TEST/PackInterval").toInt();
}

void ini_profile::set_MeterType(int type)
{
    set_ini("WORK/meter_type", QString("%1").arg(type));
}

int ini_profile::get_MeterType()
{
    return get_ini("WORK/meter_type").toInt();
}

void ini_profile::set_max_fail_num(int num)
{
    set_ini("WORK/max_fail_num", QString("%1").arg(num));
}

int ini_profile::get_max_fail_num()
{
    return get_ini("WORK/max_fail_num").toInt();
}
