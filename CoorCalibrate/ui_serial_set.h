﻿#ifndef UI_SERIAL_SET_H
#define UI_SERIAL_SET_H

#include <QDialog>

namespace Ui {
class ui_serial_set;
}

class ui_serial_set : public QDialog
{
    Q_OBJECT

public:
    explicit ui_serial_set(QWidget *parent = 0);
    ~ui_serial_set();

private slots:
    void on_pb_save_clicked();

private:
    Ui::ui_serial_set *ui;
};

#endif // UI_SERIAL_SET_H
