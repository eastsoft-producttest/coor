#include "buttondelegate.h"
#include <QPushButton>
#include <QDebug>

ButtonDelegate::ButtonDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

QWidget *ButtonDelegate::createEditor(QWidget *parent,
                                     const QStyleOptionViewItem & /* option */,
                                     const QModelIndex &index) const
{
    QPushButton *btn = new QPushButton("On", parent);
    connect(btn, &QPushButton::clicked, this, &ButtonDelegate::emitClicked);
    return btn;
}

void ButtonDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{

}

void ButtonDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                 const QModelIndex &index) const
{

}

void ButtonDelegate::emitClicked(bool i)
{
    emit commitData(qobject_cast<QWidget *>(sender()));
}
