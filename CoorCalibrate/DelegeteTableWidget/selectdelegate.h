#ifndef SELECTDELEGATE_H
#define SELECTDELEGATE_H

#include <QItemDelegate>
class SelectDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit SelectDelegate(QObject *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

signals:

public slots:
    void emitClicked(bool b);
};

#endif // SELECTDELEGATE_H
