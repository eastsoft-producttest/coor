#include "progressdelegate.h"
#include <QProgressBar>
#include <QDebug>
#include <QPainter>
#include <QApplication>

ProgressDelegate::ProgressDelegate(QObject *parent)
   : QItemDelegate(parent)
{

}

void ProgressDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.column() == 1) {
        int value = index.model()->data(index).toInt();
        QStyleOptionProgressBarV2 progressBarOption;
        progressBarOption.rect = option.rect.adjusted(4, 4, -4, -4);
        progressBarOption.minimum = 0;
        progressBarOption.maximum = 100;
        progressBarOption.textAlignment = Qt::AlignRight;
        progressBarOption.textVisible = true;
        progressBarOption.progress = value;
        progressBarOption.text = tr("%1%").arg(progressBarOption.progress);

        painter->save();
        if (option.state & QStyle::State_Selected) {
            painter->fillRect(option.rect, option.palette.highlight());
                    painter->setBrush(option.palette.highlightedText());
        }
                QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);

        painter->restore();

    } else {
        return QItemDelegate::paint (painter, option, index);
    }
}

QWidget *ProgressDelegate::createEditor(QWidget *parent,
                                     const QStyleOptionViewItem & /* option */,
                                     const QModelIndex &index) const
{
    QProgressBar *progressBar = new QProgressBar(parent);
    progressBar->setRange(0, 100);

    //connect(progressBar, SIGNAL(activated(int)), this, SLOT(emitCommitData()));

    return progressBar;
}

void ProgressDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{
    QProgressBar *progressBar = qobject_cast<QProgressBar *>(editor);
    if (!progressBar)
        return;

    progressBar->setValue(index.model()->data(index).toInt());

}

void ProgressDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                 const QModelIndex &index) const
{
    QProgressBar *progressBar = qobject_cast<QProgressBar *>(editor);
    if (!progressBar)
        return;

    //model->setData(index, progressBar->currentText());
}

void ProgressDelegate::progress_changed(int i)
{
    emit commitData(qobject_cast<QProgressBar *>(sender()));
}
