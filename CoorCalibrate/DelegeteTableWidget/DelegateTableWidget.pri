INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS                += $$PWD/buttondelegate.h \
                          $$PWD/delegatedtableview.h \
                          $$PWD/inputdelegate.h \
                          $$PWD/progressdelegate.h \
    $$PWD/selectdelegate.h

SOURCES                += $$PWD/buttondelegate.cpp \
                          $$PWD/delegatedtableview.cpp \
                          $$PWD/inputdelegate.cpp \
                          $$PWD/progressdelegate.cpp \
    $$PWD/selectdelegate.cpp
