#ifndef PROGRESSDELEGATE_H
#define PROGRESSDELEGATE_H
#include <QItemDelegate>

class ProgressDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit ProgressDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

signals:

public slots:
    void progress_changed(int i);
};

#endif // PROGRESSDELEGATE_H
