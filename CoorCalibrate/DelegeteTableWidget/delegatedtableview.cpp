#include "delegatedtableview.h"
#include <QStandardItemModel>

DelegatedTableView::DelegatedTableView(QWidget *parent)
{
    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

DelegatedTableView::~DelegatedTableView()
{
    for(QTableWidgetItem *item : m_items)
    {
        delete item;
        item = nullptr;
    }
}

void DelegatedTableView::initial(const INITIAL_LIST &list, const int nRowCount)
{
    m_initial_list.clear();
    m_initial_list.append(list);
    m_nRowCount = nRowCount;
    int n = m_initial_list.size();


    this->setColumnCount(n);
    this->setRowCount(nRowCount);

    for(int i = 0; i < nRowCount; i++)
    {

        if(i == 0)//创建表头
        {
            QStringList _header;
            int j = 0;
            for(T_CONTROL t : m_initial_list)
            {
                _header << t.s_header_name;
                switch (t.e_type) {
                case TYPE_CONTROL_TEXT:

                    break;
                case TYPE_CONTROL_SELECT:
                    this->setItemDelegateForColumn(j, new InputDelegate(nullptr));
                    break;
                case TYPE_CONTROL_BUTTON:
                    this->setItemDelegateForColumn(j, new ButtonDelegate(nullptr));
                    break;
                case TYPE_CONTROL_PROGRESS:
                    this->setItemDelegateForColumn(j, new ProgressDelegate(nullptr));
                    break;
                case TYPE_CONTROL_CHECK:
                    this->setItemDelegateForColumn(j, new SelectDelegate(nullptr));
                    break;
                default:
                    break;
                }
                j++;
            }
            this->setHorizontalHeaderLabels(_header);
        }
        for(int k = 0; k < n; k++)
        {
            QTableWidgetItem *item = new QTableWidgetItem(m_initial_list[k].s_defult_text);
            item->setFlags(item->flags() & ~Qt::ItemIsEditable);
            this->setItem(i, k, item);

            this->openPersistentEditor((item));

            m_items.append(item);
            k++;
        }
    }
}

void DelegatedTableView::set_text(int row, int col, const QString &text)
{
    QTableWidgetItem *item = this->item(row, col);
    item->setText(text);
}

void DelegatedTableView::set_process(int row, int col, const int &process)
{
    set_text(row, col, QString::number(process));
}

void DelegatedTableView::set_enable(const bool b, const int row)
{
    QColor color = QColor(255,255,255);
    if(b)
        color = QColor(255,255,255);
    else
        color = QColor(120,120,120);
    int n = this->colorCount();
    for(int i = 0; i < n; i++)
    {
        QTableWidgetItem *item = this->item(row, i);
        item->setBackgroundColor(color);
    }
}
