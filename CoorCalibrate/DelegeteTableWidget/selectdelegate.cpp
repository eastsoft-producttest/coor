#include "selectdelegate.h"
#include <QCheckBox>
#include <QDebug>


SelectDelegate::SelectDelegate(QObject *parent) :
    QItemDelegate(parent)
{

}

QWidget *SelectDelegate::createEditor(QWidget *parent,
                                     const QStyleOptionViewItem & /* option */,
                                     const QModelIndex &index) const
{
    QCheckBox *cbox = new QCheckBox("On", parent);
    connect(cbox, &QCheckBox::clicked, this, &SelectDelegate::emitClicked);
    return cbox;
}

void SelectDelegate::setEditorData(QWidget *editor,
                                  const QModelIndex &index) const
{

}

void SelectDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                 const QModelIndex &index) const
{

}

void SelectDelegate::emitClicked(bool b)
{
    qDebug() << b;
    emit commitData(qobject_cast<QWidget *>(sender()));
}
