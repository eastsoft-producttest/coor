#ifndef DELEGATEDTABLEVIEW_H
#define DELEGATEDTABLEVIEW_H

#include <QTableWidget>
#include <QTableWidgetItem>
#include "inputdelegate.h"
#include "buttondelegate.h"
#include "progressdelegate.h"
#include "selectdelegate.h"

typedef enum{
    TYPE_CONTROL_TEXT,
    TYPE_CONTROL_SELECT,
    TYPE_CONTROL_BUTTON,
    TYPE_CONTROL_PROGRESS,
    TYPE_CONTROL_CHECK,
    TYPE_CONTROL_ERROR,
}E_CONTROL;

typedef struct{
    QString   s_header_name;
    E_CONTROL e_type;
    QString   s_defult_text;
}T_CONTROL;
typedef QList<T_CONTROL> INITIAL_LIST;

class DelegatedTableView : public QTableWidget
{
public:
    DelegatedTableView(QWidget *parent = nullptr);
    ~DelegatedTableView();
    void set_text(int row, int col, const QString &text);
    void initial(const INITIAL_LIST &list, const int nRowCount);
    void set_enable(const bool b, const int row);
    void set_process(int row, int col, const int &process);
private:
    INITIAL_LIST m_initial_list;
    int m_nRowCount;

    QVector<QTableWidgetItem*> m_items;
};

#endif // DELEGATEDTABLEVIEW_H
