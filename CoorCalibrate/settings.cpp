﻿#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    m_ini = new ini_profile();
    ui->sbIntebal->setValue(m_ini->get_pack_interval());
    m_pSharer = SigletonSharer::getInstance();
}

Settings::~Settings()
{
    delete ui;
    delete m_ini;
    m_ini = nullptr;
}


void Settings::on_pb_save_clicked()
{
    QMessageBox mesg;
    if(mesg.question(this, "保存", "确认保存?") == QMessageBox::Yes)
    {
        m_ini->set_pack_interval(ui->sbIntebal->value());
        m_pSharer->m_PackInterval = ui->sbIntebal->value();
        this->close();
    }
}

void Settings::on_pb_cancel_clicked()
{
    this->close();
}
