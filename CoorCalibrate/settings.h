﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "ini_profile.h"
#include <QMessageBox>
#include "sigletonsharer.h"
namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

private slots:
    void on_pb_save_clicked();

    void on_pb_cancel_clicked();

private:
    Ui::Settings *ui;
    ini_profile* m_ini;
    SigletonSharer *m_pSharer;
};

#endif // SETTINGS_H
