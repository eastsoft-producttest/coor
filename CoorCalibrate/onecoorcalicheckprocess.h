﻿#ifndef ONECOORCALICHECKPROCESS_H
#define ONECOORCALICHECKPROCESS_H

#define DO_DEBUG 1

#include <QObject>
#include "com_port_coor.h"
#include <QList>
#include <QThread>
#include <QMutex>
#include "simu_coor.h"
#include "csv_data.h"
#include "ini_profile.h"
//#include "mes/Pass_USCOREStationSoap.nsmap"
#include "sigletonsharer.h"
#include "QsLog/QsLog.h"
#include "QDate"
#include "com_port_espd.h"
using namespace QsLogging;

const int COM_WAIT_TIME = 10000;
#define ERR_DOUBLE_NUMBER  -999999.98

//const QString sArrCode[5] = {QString("PACK_JZQ_Meter"),
//                              QString("CHECK_METER")};

class OneCoorCaliCheckProcess : public QObject
{
    Q_OBJECT
public:
    explicit OneCoorCaliCheckProcess(QObject *parent = nullptr);
    OneCoorCaliCheckProcess(const QString &sPortName, const QString &sTTUPortName, int nSerial, const bool bEnable, const QString PlatNo);
    ~OneCoorCaliCheckProcess();

    //向界面报告进度
    void report_process(QString sProcess, int nProcess)
    {
        QLOG_TRACE() << sProcess << "progress:" << nProcess;
        QStringList list;
        list.append(sProcess);
        emit sig_text(0, list);
        emit sig_process(nProcess);
    }

    //误差开始、读取封装
    int error_start_read(double &dbError, bool isABS = false);

    //int step01_init_supply();//初始化标准源//该函数取消，使用m_pSimu_coor中的接口，同步各线程
    //初始化所有集中器
    int step01_02_init_coors();
    //校准电压电流
    int step02_01_cali_volt_curr(int nPhase = 0);
    int step02_02_cali_curr();
    //功率比差校准，phase = 1,2,3分表代表ABC相
    int step03_cali_power_ratio_1_0(int nPhase, double& dbError);
    int step03_cali_power_phase_0_5L(int nPhase, double& dbError);
    //电压电流确认
    int step_02_03_check_volt_curr(T_Data_Instant & tOut);
    //Imax确认
    int step_02_04_check_Imax(T_Data_Instant & tOut);
    //功率精度误差测试，合相（封装）
    int step_check_power_Hx(float fI, QString sCosP, float fPercent, double& dbError);
    //功率精度误差测试，分相（封装）
    int step_check_power_Fx(float fI, QString sCosP, float fPercent, double& fErrorA, double& fErrorB, double& fErrorC);
    //额定功率合相1.0L误差确认：
    int step_05_1_check_power_toghter_Ib(QString sCosP, double nPercent);
    //额定功率合相IMax = 6.0误差确认：sCosP从常量中取值
    int step_05_4_check_power_toghter_IMax(QString sCosP);
    //无功1.0精度确认：
    int step_06_1_check_reactive_power_1_0L();
    //无功0.5L精度确认：
    int step_06_2_check_reactive_power_0_5L();
    //日计时检测
    int step_07_check_time_count(double& fErrorOut);
    //新加：Imax下示值误差确认
    int step_08_check_Imax();

    //校表入口
    int cali_process();
    //检表入口
    int check_process();
    //MES上传
    int mes_upload_cali_msg();
    int mes_upload_check_msg();
    //MES过包装线
    int mes_pass_package();

    int espd_start();   //智能终端校表前配置
    int espd_restart(); //智能终端重新上电后配置
    int espd_end();     //智能终端校表后配置

public:
    QString get_bar_code(){return m_sBarCode;}
    void set_bar_code(const QString& s){ m_sBarCode = s;}

    //本地error显示到界面
    void set_show_error_cali(const double e1, const double e2 = ERR_DOUBLE_NUMBER, const double e3 = ERR_DOUBLE_NUMBER,
            const double e4 = ERR_DOUBLE_NUMBER, const double e5 = ERR_DOUBLE_NUMBER, const double e6 = ERR_DOUBLE_NUMBER);
    void set_show_error_check(const double e1, const double e2 = ERR_DOUBLE_NUMBER, const double e3 = ERR_DOUBLE_NUMBER,
            const double e4 = ERR_DOUBLE_NUMBER, const double e5 = ERR_DOUBLE_NUMBER, const double e6 = ERR_DOUBLE_NUMBER);
    com_port_espd* m_pEspdPort;     //智能终端串口
private:
    SigletonSharer *m_pSharer;
    Logger *m_pLogger;
private:
    bool m_isEnable;                //是否开启该表位
    bool m_isOpen = false;          //该表位是否正常打开
    QString m_sBarCode;             //条码
    simu_coor *m_pSimu_coor;        //校表台仪器单例
    com_port_coor* m_pCoorPort;     //设备485端口
    QString m_s485PortName;         //集中器485串口名
    QString m_sTTUPortName;         //TTU串口名
    QString m_PlatNo;               //表台位置
    int m_nSerial;                  //设备位置1~32
    QStringList m_CaliInfo;         //校表数据记录
    QStringList m_CheckInfo;        //检表数据记录
    //com_port_espd* m_pEspdPort;     //智能终端串口


    T_COOR_SAVE_ERROR m_SaveError;  //校表误差写入
    T_COOR_CALI_PARA m_Para;        //校/检表参数
    float m_fStdVolt = 220.0;       //默认参数
    float m_fStdCurr = 1.5;
    float m_fMaxCurr = 6.0;
    double m_dbConstant = 6400.0;
    int m_nPluse = 2;
    int m_nDevPort = 11;
    int m_nTheoryFreq = 1;
    int m_nClockTestDuration = 10;
    double m_dbLimitQZ = 0.25;
    double m_dbLimitHX = 0.2;
    double m_dbLimitFX = 0.2;
    double m_dbLimitWG = 0.3;
    float  m_fCurrLimit = 0.02;
    float  m_fVoltLimit = 0.2;
    float  m_fCurrLimit_Imax = 0.6;
    QMutex mutex1;                  //读误差互斥锁
    //soap
    //MES对象,MES组要求数据不要同时上传，做互斥
    Pass_USCOREStationSoapProxy* m_MesUSCore;
    //包装线Mes对象
    //MesNewWebServiceSoapProxy* m_MesWeb;

    csv_data* m_cali_csv;           //校表CSV
    csv_data* m_check_csv;          //检表CSV
public slots:
    void do_cali_process();
    void do_check_process();
    void slot_start_readMac();

    int upload_cali_data(const QStringList& sListAutoCal, const QStringList& CaliInfo, QString& sOut);
    int upload_check_data(const QStringList& sListAutoCheck, const QStringList& CheckInfo, QString& sOut);
    int pass_pack_line(const QString& sSerial, QString& sOut);
    //向集中器存储误差值
    int save_error(T_COOR_SAVE_ERROR tError);
    //获取校表工作中心
    int get_cali_ws(const QString &sWsCali);
    //获取包装线工作中心
    int get_pack_ws(const QString &sWsPack);
    //获取制令单号
    int get_mo(const QString &sMoNum);
    //获取操作员
    int get_operator(const QString &sOp);
signals:
    void sig_process(int nProcess);
    void sig_text(int nStartCol, QStringList sListText);
    void sig_result(int type, int nProcess_ret);
    void sig_port_error(int serial);
signals:
    void sig_power_control(int nType);
};

#endif // ONECOORCALICHECKPROCESS_H
