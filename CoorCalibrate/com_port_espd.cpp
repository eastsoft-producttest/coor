﻿#include "com_port_espd.h"



com_port_espd::com_port_espd(const QString& sPortName, int serial)
{
    m_nSerial = serial;
    m_sPortName = sPortName;
    this->setPortName(m_sPortName);
    this->setParity(ParityType::PAR_NONE);
    this->setDataBits(DataBitsType::DATA_8);
    this->setStopBits(StopBitsType::STOP_1);
    this->setBaudRate(BaudRateType::BAUD115200);
    this->setFlowControl(FLOW_OFF);
}

com_port_espd::~com_port_espd()
{
    if(this->isOpen())
        this->close();
}

/*void com_port_espd::slot_ReadyRead()
{
    QByteArray byteRecv = this->readAll();
    QString strRecv = QString::fromUtf8(byteRecv);
    if(strRecv.isEmpty())
        return;
    m_recieve += strRecv;
    QRegExp reg_log("(SCT230A)\\s+(login)");
    QRegExp reg_password("Password:");
    QRegExp reg_end("(Welcome).*(sysadm@SCT230A:~[$])");
    if(reg_log.indexIn(m_recieve) > -1)
    {
        qDebug() << m_recieve.length();
        qDebug() << "请登录";
        m_recieve.clear();
        this->write("sysadm\n");
        return;
    }
    if(reg_password.indexIn(m_recieve) > -1)
    {
        qDebug() << m_recieve.length();
        qDebug() << "输入密码";
        m_recieve.clear();
        this->write("Zxbdt@729.TTU\n");
        return;
    }
    if(reg_end.indexIn(m_recieve) > -1)
    {
        qDebug() << "登录成功";
        m_recieve.clear();
        disconnect(m_Serial, &QextSerialPort::readyRead, this, &MainWindow::slot_serial_read);
    }
}*/

int com_port_espd::start_container(QString &res)
{
    int iRet;
    iRet = send_wait(m_command.StartContainer, res);
    if(iRet < 0)
        return iRet;
    if (res.indexOf("success") == -1)//先简单检查一下是否关闭成功
    {
        iRet = -3;
    }
    return iRet;
}

int com_port_espd::stop_container(QString &res)
{
    int iRet;
    iRet = send_wait(m_command.StopContainer, res);
    if(iRet < 0)
        return iRet;
    if(res.indexOf("success") == -1)
    {
        iRet = -3;
    }
    return iRet;
}

int com_port_espd::check_container(QString &res, bool isopen)
{
    int iRet;
    iRet = send_wait(m_command.CheckContainer, res);
    if(iRet < 0)
        return iRet;
    if (isopen)  //查看开启状态
    {
        QRegExp reg_OpenStatus("(Up).*(c_base)");
        if (reg_OpenStatus.indexIn(res) == -1)
        {
            iRet = -3;
        }
    }
    else
    {
        QRegExp reg_CloseStatus("(Exited).*(c_base)");
        if (reg_CloseStatus.indexIn(res) == -1)
        {
            iRet = -3;
        }
    }
    return iRet;
}

int com_port_espd::start_app(QString &res)
{
    int iRet;
    iRet = send_wait(m_command.StartApp, res);
    return iRet;
}

int com_port_espd::stop_app(QString &res)
{
    //调用该函数前必须先调用check_app获取PID的值
    int iRet;
    iRet = send_wait(m_command.StopApp.arg(m_PID), res);
    return iRet;
}

int com_port_espd::check_app(QString &res)
{
    int iRet;
    iRet = send_wait(m_command.CheckApp, res);
    if(iRet < 0)
        return iRet;
    res.remove("\033[01;31m\033[K");
    res.remove("\033[m\033[K");
    QRegExp reg_PID("(root\\s+)(\\d+)((\\s+\\S+){8})(\\s+./adjjc)");
    if((reg_PID).indexIn(res) > -1)
    {
        qDebug() << "进程号为:" + reg_PID.cap(2);
        m_PID = reg_PID.cap(2).toInt();
    }
    else
    {
        iRet = -3;
    }
    return iRet;
}

int com_port_espd::login()
{
    int iRet;
    QString res;
    //上电后等30S再尝试登录3次
    for(int i = 0; i < 3; i++)
    {
        iRet = send_wait(m_command.Username, res);
        if(iRet == 1)
            return iRet;
    }
    return iRet;
}

int com_port_espd::send_wait(QString str, QString &res)
{
    if (!this->isOpen())
    {
        qDebug() << "终端串口打开失败";
        return -1;
    }
    QString send = str;
    QByteArray ba;
    this->write(send.toLatin1());
    int MaxNum = 10;
    while (MaxNum--)
    {
        if (this->bytesAvailable())
        {
            ba = this->readAll();
            m_recieve += QString::fromUtf8(ba);
        }

        if(reg_login.indexIn(m_recieve) > -1)
        {
            this->write("sysadm\n");
            MaxNum = 10;
            m_recieve = "";
            continue;
        }
        if(reg_loginPassword.indexIn(m_recieve) > -1)
        {
            this->write("Zxbdt@729.TTU\n");
            MaxNum = 10;
            m_recieve = "";
            continue;
        }

        //自动输入密码
        if (reg_inputPassword.indexIn(m_recieve) > -1)
        {
            this->write(m_command.Password.toLatin1());
            MaxNum = 10;
            m_recieve = "";
            continue;
        }
        //遇到一条指令结尾
        if (reg_End.indexIn(m_recieve) > -1)
        {
            res = m_recieve;
            m_recieve = "";
            return 1;
        }
        QThread::msleep(1000);
    }
    return -2;  //超时
}


