﻿#include "csv_data.h"
#include <QTextStream>
#include <QDebug>
#include <QCoreApplication>
#include <QDateTime>
#include <QDir>

csv_data::csv_data(QString sFile_Head, QString sHead)
{
    QString stime = QDateTime::currentDateTime().toString("yyyy_MM_dd");
    QString sDir = QCoreApplication::applicationDirPath() + "/data_file/";
    QString csv_filepath = QCoreApplication::applicationDirPath() + "/data_file/" + sFile_Head + stime + ".csv";
    QDir dir;
    if (!dir.exists(sDir))
    {
        dir.mkdir(sDir);
    }
    m_csvFile = new QFile(csv_filepath);
    if (!m_csvFile->exists())
    {
        QStringList sHeads = sHead.split(",");
        qDebug() << "csv file not exist";
        QVector<QString> headers;
        for(int i = 0; i < sHeads.length(); i++)
        {
            headers.append(sHeads.at(i));
        }

        this->set_headers(headers);
    }
}

csv_data::~csv_data()
{
    delete m_csvFile;
    m_csvFile = nullptr;
}

void csv_data::set_headers(QVector<QString> headers)
{
    bool bOpen = m_csvFile->open(QIODevice::WriteOnly);
    if(m_csvFile && bOpen)
    {
        QString str_headers = "";
        for (auto header : headers)
        {
            str_headers += header + ",";
        }
        str_headers.remove(str_headers.length() - 1, 1);
        str_headers += "\n";
        QTextStream txtOutPut(m_csvFile);
        txtOutPut << str_headers;
    }
    m_csvFile->close();
}

void csv_data::set_rowdatas(QVector<QString> datas)
{
    m_Mutex.lock();
    bool bOpen = m_csvFile->open(QIODevice::WriteOnly | QIODevice::Append);
    if (m_csvFile && bOpen)
    {
        QString str_datas = "\n";
        for (auto data : datas)
        {
            str_datas += data + ",";
        }
        str_datas.remove(str_datas.length() - 1, 1);
        //str_datas += "\n";
        QTextStream txtOutPut(m_csvFile);
        txtOutPut << str_datas;
    }
    m_csvFile->close();
    m_Mutex.unlock();
}
void csv_data::set_rowdatas(QStringList datas)
{
    m_Mutex.lock();
    bool bOpen = m_csvFile->open(QIODevice::WriteOnly | QIODevice::Append);
    if (m_csvFile && bOpen)
    {
        QString str_datas = "'";
        for(int i = 0; i < datas.length(); i++)
        {
            str_datas += datas.at(i) + ",";
        }
        str_datas.remove(str_datas.length() - 1, 1);
        str_datas += "\n";
        QTextStream txtOutPut(m_csvFile);
        txtOutPut << str_datas;
    }
    m_csvFile->close();
    m_Mutex.unlock();
}
void csv_data::set_appendData(QString data)
{
    m_Mutex.lock();
    bool bOpen = m_csvFile->open(QIODevice::WriteOnly | QIODevice::Append);
    if (m_csvFile && bOpen)
    {
        data = "," + data;
        QTextStream txtOutPut(m_csvFile);
        txtOutPut << data;
    }
    m_csvFile->close();
    m_Mutex.unlock();
}
