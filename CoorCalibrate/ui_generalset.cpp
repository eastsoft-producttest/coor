﻿#include "ui_generalset.h"
#include "ui_ui_generalset.h"

ui_GeneralSet::ui_GeneralSet(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ui_GeneralSet)
{
    ui->setupUi(this);
    m_ini = new ini_profile();

    ui->cb_MeterType->setCurrentIndex(m_ini->get_MeterType());
    ui->sb_maxFailNum->setValue(m_ini->get_max_fail_num());
    m_pSharer = SigletonSharer::getInstance();
}

ui_GeneralSet::~ui_GeneralSet()
{
    delete ui;
}

void ui_GeneralSet::on_pb_save_clicked()
{
    QMessageBox mesg;
    if(mesg.question(this, "保存", "确认保存?") == QMessageBox::Yes)
    {
        m_ini->set_MeterType(ui->cb_MeterType->currentIndex());
        m_ini->set_max_fail_num(ui->sb_maxFailNum->value());
        m_pSharer->m_MeterType = ui->cb_MeterType->currentIndex();
        m_pSharer->m_maxFailNum = ui->sb_maxFailNum->value();
        emit sig_close();
        this->close();
    }
}

void ui_GeneralSet::on_pb_cancel_clicked()
{
    this->close();
}
