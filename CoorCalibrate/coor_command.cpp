﻿#include "coor_command.h"
#include <QDateTime>
#include "data_handle_utility/funlib.h"
#include <QDebug>
namespace coor_command {

static QByteArray InsertCS(QString &sIn)
{
    unsigned char data[100] = {0};
    int dlen = 0;
    StrToBcd_asc(sIn.remove(' '), data, dlen);
    unsigned char cs=chksum(data, dlen);

    data[dlen++] = cs;
    data[dlen++] = 0x16;
    return QByteArray((const char*)data, dlen);
}

static QString Hex2String(QByteArray ba_in)
{
    QDataStream stream(&ba_in, QIODevice::ReadWrite);
    QString sret = "";
    while(!stream.atEnd())
    {
        uint8_t c;
        stream >> c;
        char s[5] = {0};
        sprintf(s,"%02X",c);
        QString str = s;
        sret += str;
    }
    return sret;
}

static QByteArray String2Hex(QString str)
{
    QByteArray baRet;
    QString strTemp;
    for(int i = 0; i < str.length()/2; i++)
    {
        strTemp = str.mid(i*2,2);
        bool ok;
        qint8 c = strTemp.toInt(&ok,16);

        baRet.append(c);
    }
    return baRet;
}
#ifdef METER_BOX_TERMINAL
//进入场内模式
QByteArray get_EnterArea_cmd(const QString &sAddr, bool bAddrReverse)
{
    QDateTime dt = QDateTime::currentDateTime();

    QString sDt = dt.toString("ddMMyy");

    QString addstr = AdjAddr(12, sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr = reverseAddr(addstr);
    sDt = Add33(sDt);//月-日-年，校表日期

    QString str="68"+addstr+"68 14 0C 88 DD FF DD 44 55 66 77 33 33 33 33";

    return InsertCS(str);
}
//退出场内模式
QByteArray get_QuitArea_cmd(const QString &sAddr, bool bAddrReverse)
{
    QDateTime dt = QDateTime::currentDateTime();

    QString sDt = dt.toString("ddMMyy");

    QString addstr = AdjAddr(12, sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr = reverseAddr(addstr);
    sDt = Add33(sDt);//月-日-年，校表日期

    QString str="68"+addstr+"68 14 0C DD 88 FF DD 44 55 66 77 33 33 33 33";

    return InsertCS(str);
}

//获取瞬时量
QByteArray get_instant_val_cmd(const QString &sAddr, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str = "68" + addstr + "68 11 04 33 33 32 15";

    return InsertCS(str);
}

//恢复出厂设置
QByteArray get_reset_cmd(const QString &sAddr, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str="68" + addstr + "68 14 0C 32 32 FF DD 44 55 66 77 33 33 33 33";

    return InsertCS(str);
}
#else
//初始化
QByteArray get_initial_cmd(const QString &sAddr, bool bReverse)
{
    QDateTime dt = QDateTime::currentDateTime();

    QString sDt = dt.toString("ddMMyy");

    QString addstr = AdjAddr(12, sAddr);//地址不足12位前面补零
    if(bReverse)
        addstr = reverseAddr(addstr);
    sDt = Add33(sDt);//月-日-年，校表日期

    QString str="68" + addstr + "68 14 1F 86 78 FF DD 44 55 66 77 88 99 AA BB " + sDt + "32 21 21 21 21 21 21 21 21 21 21 21 21 21 21 21";

    return InsertCS(str);
}
//获取集中器条码
QByteArray get_barcode_cmd()
{
    const unsigned char cart[] = {0x68, 0x25, 0x02, 0x00, 0x00, 0x00, 0x00, 0x68, 0x11, 0x04, 0x34, 0x33, 0x32, 0x21, 0xC6, 0x16};
    QByteArray ba((const char*)cart, 16);
    return ba;
}

//校准电压电流值
QByteArray set_volt_and_curr_cmd(const QString &sAddr, int nPhaseIndex, float fCurr, bool bAddrReverse)
{
    QString addstr=AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);

    QDateTime dt = QDateTime::currentDateTime();
    //QString sDt = dt.toString("ddMMyy");

    QString sCurr;
    sCurr.sprintf("%.04f", fCurr);
    QStringList sLCurr = sCurr.split(".");
    Q_ASSERT(sLCurr.size() >= 2);
    sCurr = reverseAddr(AdjAddr(4, sLCurr.at(0)) + sLCurr.at(1));
    QString str_err=Add33(sCurr);

    QString str="68" + addstr + "68 14 1F 86 78 FF DD 44 55 66 77 88 99 AA BB 34 35 43 " + sPhase[nPhaseIndex] + " 33 33 34 33 33 53 35 " + "33 33 33 33" + " 21 21 21 21";
    //QString str= "68 99 99 99 99 99 99 68 14 1F 86 78 FF DD 33 BB 99 88 87 86 74 78 34 35 43 33 33 33 34 33 33 53 35 33 83 34 33 21 21 21 21";
    return InsertCS(str);
}


//获取电压数据块
QByteArray get_instant_volt_cmd(const QString &sAddr, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str = "68" + addstr + "68 11 04 33 32 34 35";

    return InsertCS(str);
}

//获取电流数据块
QByteArray get_instant_curr_cmd(const QString &sAddr, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str = "68" + addstr + "68 11 04 33 32 35 35";

    return InsertCS(str);
}
QByteArray set_init_power_ratio_differ_cmd(const QString &sAddr, int nPhaseIndex, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str = "68" + addstr + "68 14 1F 86 78 FF DD 33 BB 99 88 87 86 74 78 34 35 43" + sPhase[nPhaseIndex]
            + "34 33 34 33 33 33 33 21 21 21 21 21 21 21 21";

    return InsertCS(str);
}
//校准功率比差
QByteArray set_power_ratio_differ_cmd(const QString &sAddr, int nPhaseIndex, float fDiff, bool bAddrReverse)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString sDt = dt.toString("ddMMyy");

    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);

    QString sErr;
    if(fDiff < 0.0)
    {
        fDiff -= 0.05;
        QString sDiff; sDiff.sprintf("%.04f", 0.0 - fDiff);
        QStringList sLDiff = sDiff.split(".");
        sErr = reverseAddr("80"+AdjAddr(2, sLDiff.at(0)) + sLDiff.at(1));
    }
    else
    {
        fDiff -= 0.05;
        QString sDiff; sDiff.sprintf("%.04f", fDiff);
        QStringList sLDiff = sDiff.split(".");
        sErr = reverseAddr(AdjAddr(2, sLDiff.at(0)) + sLDiff.at(1));
    }

    sErr = Add33(sErr);
    if(sErr.length() == 6)
        sErr += "33";
    QString str ="68"+addstr+"68 14 1F 86 78 FF DD 44 55 66 77 88 99 AA BB "
            + sDt + sPhase[nPhaseIndex] + "34 33 34 " + sErr + "21 21 21 21 21 21 21 21";

    return InsertCS(str);
}
QByteArray set_init_cali_phase_diff_cmd(const QString &sAddr , int nPhaseIndex, bool bAddrReverse )
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str = "68" + addstr + "68 14 1F 86 78 FF DD 33 BB 99 88 87 86 74 78 34 35 43" + sPhase[nPhaseIndex]
            + "35 33 34 33 33 33 33 21 21 21 21 21 21 21 21";

    return InsertCS(str);
}
//校准功率相差
QByteArray set_cali_phase_diff_cmd(const QString &sAddr, int nPhaseIndex, float fDiff, bool bAddrReverse)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString sDt = dt.toString("ddMMyy");

    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);

    QString sErr;
    if(fDiff < 0.0)
    {
        fDiff -= 0.05;
        QString sDiff; sDiff.sprintf("%.04f", 0.0 - fDiff);
        QStringList sLDiff = sDiff.split(".");
        sErr = reverseAddr("80"+AdjAddr(2, sLDiff.at(0)) + sLDiff.at(1));
    }
    else
    {
        fDiff -= 0.05;
        QString sDiff; sDiff.sprintf("%.04f", fDiff);
        QStringList sLDiff = sDiff.split(".");
        sErr = reverseAddr(AdjAddr(2, sLDiff.at(0)) + sLDiff.at(1));
    }
    sErr = Add33(sErr);
    if(sErr.length() == 6)
        sErr += "33";
    QString str ="68"+addstr+"68 14 1F 86 78 FF DD 44 55 66 77 88 99 AA BB "\
            + sDt + sPhase[nPhaseIndex]+"35 33 34 " + sErr + "21 21 21 21 21 21 21 21";

    return InsertCS(str);
}

//读电量（00010000）
QByteArray get_read_meter_cmd(const QString &sAddr, bool bAddrReverse)
{
    QString addstr = AdjAddr(12,sAddr);//地址不足12位前面补零
    if(bAddrReverse)
        addstr=reverseAddr(addstr);
    QString str="68" + addstr + "68 11 04 33 33 34 33";

    return InsertCS(str);
}

QByteArray save_cali_error_cmd(T_COOR_SAVE_ERROR tError)
{
    QString str = "68 DD DD DD DD DD DD 68 14 23 35 33 32 21";
    str += QString(" %1").arg(tError.Plat_No + 0x33, 2, 16, QLatin1Char('0'));
    str += QString(" %1").arg(tError.MeterLoc + 0x33, 2, 16, QLatin1Char('0'));
    char cYear = QString("%1").arg(tError.Year, 4, 10, QLatin1Char('0')).right(2).toInt(nullptr, 16);
    char cMonth = QString("%1").arg(tError.Month, 2, 10, QLatin1Char('0')).toInt(nullptr, 16);
    char cDay = QString("%1").arg(tError.Day, 2, 10, QLatin1Char('0')).toInt(nullptr, 16);
    str += QString(" %1").arg(cYear + 0x33, 2, 16, QLatin1Char('0'));
    str += QString(" %1").arg(cMonth + 0x33, 2, 16, QLatin1Char('0'));
    str += QString(" %1").arg(cDay + 0x33, 2, 16, QLatin1Char('0'));
    str = Error2Byte(str, tError.A_CurrError);
    str = Error2Byte(str, tError.B_CurrError);
    str = Error2Byte(str, tError.C_CurrError);

    str = Error2Byte(str, tError.A_VoltError);
    str = Error2Byte(str, tError.B_VoltError);
    str = Error2Byte(str, tError.C_VoltError);

    str = Error2Byte(str, tError.A_Ib1Error);
    str = Error2Byte(str, tError.B_Ib1Error);
    str = Error2Byte(str, tError.C_Ib1Error);

    str = Error2Byte(str, tError.A_Ib05Error);
    str = Error2Byte(str, tError.B_Ib05Error);
    str = Error2Byte(str, tError.C_Ib05Error);

    str = Error2Byte(str, tError.H_01I1Error);

    return InsertCS(str);
}
#endif

static QByteArray byte_dec33_reverse(const QByteArray& ba)
{
    QByteArray baRet;
    int nLen = ba.length();
    for(int i = 0; i < nLen; i++)
    {
        baRet[i] = ba[nLen - 1 - i] - 0x33;
    }
    return baRet;
}

//该接口有bug，使用Hex2String
static QString bcd_byte_to_string(const QByteArray& ba)
{
    int nLen = ba.length();
    QString sRet;
    for(int i = 0; i < nLen; i++)
    {
        sRet += QString::number(ba[i],16);
    }

    return sRet;
}

static QString byte_dec33_reverse_to_string(const QByteArray& ba)
{
    QByteArray baRet = byte_dec33_reverse(ba);
    return Hex2String(baRet);
}

T_RCV_PACK decode_rcv_pack(const QByteArray baIn)
{
    T_RCV_PACK tRcv;
    tRcv.flag = baIn[8];
    if(0x91 != tRcv.flag && 0xd4 != tRcv.flag)
    {
        return tRcv;
    }
    else if(0xd4 == tRcv.flag)
    {
        tRcv.data_type = 0;
        return tRcv;
    }
    else
    {
        QByteArray baData = baIn.mid(14, baIn.length() - 16);
        //正向有功
        if(baIn[10] == 0x33 && baIn[11] == 0x33 && baIn[12] == 0x34 && baIn[13] == 0x33)
        {
            tRcv.data_type = 1;
            QString sPower = byte_dec33_reverse_to_string(baData);
            sPower = sPower.left(4) + "." + sPower.right(4);
            tRcv.dbPositivePower = sPower.toDouble();
            return tRcv;
        }
        //电压数据块
        else if(baIn[10] == 0x33 && baIn[11] == 0x32 && baIn[12] == 0x34 && baIn[13] == 0x35)
        {
            tRcv.data_type = 2;
            QByteArray baTemp = baData;

            qDebug() << baTemp;
            //Ua
            QString sUa = byte_dec33_reverse_to_string(baTemp.left(2));
            sUa = sUa.left(3) + "." + sUa.right(1);
            sUa.remove("f");
            tRcv.tDataVoltBlock.fUa = sUa.toFloat();
            baTemp.remove(0,2);
            qDebug() << tRcv.tDataVoltBlock.fUa;
            //Ub
            QString sUb = byte_dec33_reverse_to_string(baTemp.left(2));
            qDebug() << sUb;
            sUb = sUb.left(3) + "." + sUb.right(1);
            tRcv.tDataVoltBlock.fUb = sUb.toFloat();
            baTemp.remove(0,2);
            qDebug() << tRcv.tDataVoltBlock.fUb;
            //Uc
            QString sUc = byte_dec33_reverse_to_string(baTemp.left(2));
            sUc = sUc.left(3) + "." + sUc.right(1);
            sUc.remove("f");
            tRcv.tDataVoltBlock.fUc = sUc.toFloat();
            baTemp.remove(0,2);
            qDebug() << tRcv.tDataVoltBlock.fUc;
        }
        //电流数据块
        else if(baIn[10] == 0x33 && baIn[11] == 0x32 && baIn[12] == 0x35 && baIn[13] == 0x35)
        {
            tRcv.data_type = 3;
            QByteArray baTemp = baData;
            //Ua
            QString sIa = byte_dec33_reverse_to_string(baTemp.left(3));
            sIa.remove("f");
            sIa = sIa.left(3) + "." + sIa.right(3);

            tRcv.tDataCurrBlock.fIa = sIa.toFloat();
            baTemp.remove(0,3);
            //Ub
            QString sIb = byte_dec33_reverse_to_string(baTemp.left(3));
            sIb.remove("f");
            sIb = sIb.left(3) + "." + sIb.right(3);

            tRcv.tDataCurrBlock.fIb = sIb.toFloat();
            baTemp.remove(0,3);
            //Uc
            QString sIc = byte_dec33_reverse_to_string(baTemp.left(3));
            sIc.remove("f");
            sIc = sIc.left(3) + "." + sIc.right(3);
            tRcv.tDataCurrBlock.fIc = sIc.toFloat();
            baTemp.remove(0,3);
        }
        //瞬时量，表箱终端需要,集中器不需要
        else if(baIn[10] == 0x33 && baIn[11] == 0x33 && baIn[12] == 0x32 && baIn[13] == 0x15)
        {
            tRcv.data_type = 4;
            QByteArray baTemp = baData;
            //Ua
            QString sUa = byte_dec33_reverse_to_string(baTemp.left(2));
            sUa = sUa.left(3) + "." + sUa.right(1);
            tRcv.tDataInstant.fUa = sUa.toFloat();
            baTemp.remove(0,2);
            //Ub
            QString sUb = byte_dec33_reverse_to_string(baTemp.left(2));
            sUb = sUb.left(3) + "." + sUb.right(1);
            tRcv.tDataInstant.fUb = sUb.toFloat();
            baTemp.remove(0,2);
            //Uc
            QString sUc = byte_dec33_reverse_to_string(baTemp.left(2));
            sUc = sUc.left(3) + "." + sUc.right(1);
            tRcv.tDataInstant.fUc = sUc.toFloat();
            baTemp.remove(0,2);
            //Ia
            QString sIa = byte_dec33_reverse_to_string(baTemp.left(4));
            sIa = sIa.left(5) + "." + sIa.right(3);
            tRcv.tDataInstant.fIa = sIa.toDouble();
            baTemp.remove(0,4);
            //Ib
            QString sIb = byte_dec33_reverse_to_string(baTemp.left(4));
            sIb = sIb.left(5) + "." + sIb.right(3);
            tRcv.tDataInstant.fIb = sIb.toDouble();
            baTemp.remove(0,4);
            //Ic
            QString sIc = byte_dec33_reverse_to_string(baTemp.left(4));
            sIc = sIc.left(5) + "." + sIc.right(3);
            tRcv.tDataInstant.fIc = sIc.toDouble();
            baTemp.remove(0,4);
            //In_s
            QString sIns = byte_dec33_reverse_to_string(baTemp.left(4));
            sIns = sIns.left(5) + "." + sIns.right(3);
            tRcv.tDataInstant.fIns = sIns.toDouble();
            baTemp.remove(0,4);
            //PFa
            QString sFPa = byte_dec33_reverse_to_string(baTemp.left(2));
            sFPa = sFPa.left(1) + "." + sFPa.right(3);
            tRcv.tDataInstant.fPFa = sFPa.toFloat();
            baTemp.remove(0,2);
            //PFb
            QString sPFb = byte_dec33_reverse_to_string(baTemp.left(2));
            sPFb = sPFb.left(1) + "." + sPFb.right(3);
            tRcv.tDataInstant.fPFb = sPFb.toFloat();
            baTemp.remove(0,2);
            //PFc
            QString sPFc = byte_dec33_reverse_to_string(baTemp.left(2));
            sPFc = sPFc.left(1) + "." + sPFc.right(3);
            tRcv.tDataInstant.fPFc = sPFc.toFloat();
            baTemp.remove(0,2);
            //PFt
            QString sPFt = byte_dec33_reverse_to_string(baTemp.left(2));
            sPFt = sPFt.left(1) + "." + sPFt.right(3);
            tRcv.tDataInstant.fPFt = sPFt.toFloat();
            baTemp.remove(0,2);
            return tRcv;
        }
        //MAC条码
        else if(baIn[10] == 0x34 && baIn[11] == 0x33 && baIn[12] == 0x32 && baIn[13] == 0x21)
        {
            tRcv.data_type = 5;
            QByteArray baTemp;
            for(int i = 0; i < baData.length(); i++)
            {
                baTemp.append(baData.at(i) - 0x33);
            }
            char low = baTemp.at(0) & 0x0F;
            char high = (baTemp.at(0) & 0xF0) >> 4;
            baTemp[0] = high + (low << 4);
            QString sBarCode = Hex2String(baTemp);
            tRcv.barcode = sBarCode;
            return tRcv;
        }
        else
        {
            tRcv.data_type = 4;
            return tRcv;
        }
    }
}
}
