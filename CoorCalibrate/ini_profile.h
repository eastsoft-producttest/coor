﻿#ifndef INI_PROFILE_H
#define INI_PROFILE_H

#include <QObject>
#include <QSettings>
#include <QMap>

typedef struct
{
    QString sip;
    QString sport;
    int maxline;
}INI_S;

const QString sIniPath = "settings.ini";

typedef struct
{
    QString m_nStdVolt;//220            标准电压
    QString m_nStdCurr;//1.5            标准电流
    QString m_nImax;//6.0               最大电流
    QString m_nVoltErrLimit;//0.2       电压误差限值
    QString m_nCurrErrLimit;//0.02      Ib误差限值
    QString m_nImaxErrLimit;//0.6系数    Imax误差限值
    QString m_nLightErrLimit;//         轻载误差
    QString m_nHexiangErrLimit;//       合相误差
    QString m_nReactiveErrLimit;//      无功误差
    QString m_nConstant;//              脉冲常数
    QString m_nPluse;//                 圈数
    QString m_nStdClockFreq;//          集中器时钟频率
    QString m_nClockErrLimit;//         日计时误差 s/天 0.3
    QString m_nClockTestDuration;//     时钟测试时长
    //QString m_nStdDevPort;

}T_COOR_CALI_PARA;

class T_STATION_INFO
{
public:
    QString m_sWorkStation;     //工作中心
    QString m_sWorkStation_pack;//包装线工作中心，其他信息一致
    QString m_sMo;              //制令单号
    QString m_sOperator;        //员工号
    QString m_sModelCode;
    int m_EnableMes;
    int m_PassPack;

    QString m_sWorkStationName;
    QString m_WorStationID;
    QString m_sOperatorName;
    QString m_sWorkStationName_pack;
    QString m_WorStationID_pack;

    bool m_bCloseFlag;

    //QString m_StandardMeter;    //标准表
};


class ini_profile : public QObject
{
    Q_OBJECT
public:
    explicit ini_profile(QObject *parent = nullptr);
    ~ini_profile();
    //获取设置行数
    int get_maxline();
    void set_maxline(QString snline);
    //*****表台设置*****//
    //表台序号
    void set_plat_NO(QString sNo);
    QString get_plat_NO();
    //扫枪串口
    void set_scanner_com(QString sCom);
    QString get_scanner_com();
    //标准表设备地址
    void set_dev_com(QString sCom);
    QString get_dev_com();
    //校表or检表
    void set_work_pattern(QString sPattern);
    int  get_work_pattern();
    //获取标准表型号
    void set_standardMeter(QString meter);
    QString get_standardMeter();
    //条码读取方式,0-扫码读取 1-MAC读取
    void set_BarReadWay(int way);
    int get_BarReadWay();
    //*****MES设置*****//
    void get_MES_setting(T_STATION_INFO& tStationInfo);
    //MES参数设置
    void set_work_center(QString wc_msg);
    void set_work_center_pack(QString wc_msg);
    void set_order_num(QString order_num);
    void set_operator(QString oper);
    void set_enable_mes(bool b);
    void set_enable_passpack(bool b);


    //集中器串口列表设置
    void set_com_port_list(const QStringList &sComList);
    QStringList get_com_port_list();
    //智能终端串口列表设置
    void set_ttu_com_port_list(const QStringList &sComList);
    QStringList get_ttu_com_port_list();

    //设置bitmap
    void set_online_bitmap(unsigned int nBit);
    unsigned int get_online_bitmap();
    void set_online(const int nOnline);
    int get_online();


    //***表参数***//
    T_COOR_CALI_PARA get_coor_cali_para();
    void set_coor_cali_para(const T_COOR_CALI_PARA& para);

    //***测试数据***//
    void set_pack_interval(int interval);       //MES上传包装线间隔
    int get_pack_interval();

    //***工作设置***//
    //校表类型，0为集中器，1为台区智能终端
    void set_MeterType(int type);
    int get_MeterType();

    //***设置最大失败数量***//
    void set_max_fail_num(int num);
    int get_max_fail_num();

private:
    void set_ini(QString skey, QString svalue);
    QString get_ini(QString skey);
private:
    QSettings* m_set;
    INI_S m_ini_profile;
    //对应列表
    QMap<QString, QString> m_ini_map;
signals:

public slots:
};

#endif // INI_PROFILE_H
