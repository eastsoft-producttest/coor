#-------------------------------------------------
#
# Project created by QtCreator 2020-08-31T13:19:40
#
#-------------------------------------------------

QT       += core gui serialport texttospeech

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CoorCalibrate
TEMPLATE = app

include(coor_graphics_pri/coor_graphics.pri)
include(extserialport/qextserialport.pri)
include(QsLog/QsLog.pri)
include(DelegateTW/DelegateTW.pri)

RC_FILE += ico.rc
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    simu_coor.cpp \
    com_port_coor.cpp \
    data_handle_utility/funlib.cpp \
    coor_command.cpp \
    packet_chain/packetchain.cpp \
    ini_profile.cpp \
    onecoorcalicheckprocess.cpp \
    CTabWidget/aequilatetabbar.cpp \
    CTabWidget/ctabwidget.cpp \
    sigletonsharer.cpp \
    csv_data.cpp \
    mes/json_format.cpp \
    mes/soapC.cpp \
    mes/soapPass_USCOREStationSoapProxy.cpp \
    mes/stdsoap2.cpp \
    settings.cpp \
    ui_serial_set.cpp \
    ui_generalset.cpp \
    com_port_espd.cpp




HEADERS += \
        mainwindow.h \
    simu_coor.h \
    dll_func_addr.h \
    com_port_coor.h \
    data_handle_utility/funlib.h \
    coor_command.h \
    packet_chain/packetchain.h \
    ini_profile.h \
    datadef.h \
    onecoorcalicheckprocess.h \
    CTabWidget/aequilatetabbar.h \
    CTabWidget/ctabwidget.h \
    sigletonsharer.h \
    csv_data.h \
    mes/CaliCheck.h \
    mes/json_format.h \
    mes/Pass_USCOREStationSoap.nsmap \
    mes/soapH.h \
    mes/soapPass_USCOREStationSoapProxy.h \
    mes/soapStub.h \
    mes/stdsoap2.h \
    settings.h \
    ui_serial_set.h \
    ui_generalset.h \
    com_port_espd.h




FORMS += \
        mainwindow.ui \
    form_list.ui \
    settings.ui \
    ui_serial_set.ui \
    ui_generalset.ui

LIBS += -lpthread libwsock32 libws2_32

RESOURCES += \
    qss.qrc

DISTFILES += \
    mes/WSock32.Lib

