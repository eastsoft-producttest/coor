﻿#include "tableview.h"

#include "tablemodel.h"
#include "progress_delegate.h".h"

TableView::TableView(QWidget *parent) :
    QTableView(parent)
{

}

TableView::TableView(QStringList& ListHead, const int& nCol)
{
    iniData(ListHead, nCol);
}

TableView::~TableView()
{
    delete m_model;
}

void TableView::iniData(QStringList ListHead, int nCol)
{
    m_model = new TableModel();
    this->setModel(m_model);

    ListHead.insert(0, "状态");
    ListHead.insert(1, "进度");
    m_model->setHorizontalHeader(ListHead);

    QVector<QStringList> data;
    for(int i = 0; i < nCol; i++)
    {
        QStringList sl;
        for(int j = 0; j < ListHead.size(); j++)
            sl.append(" ");
        data.append(sl);
    }

    m_model->setLongData(data);

    m_progressBarDelegate = new ProgressBarDelegate(this);
    this->setItemDelegate(m_progressBarDelegate);
    emit m_model->layoutChanged();
    this->setColumnWidth(1, 250);
    this->setColumnWidth(2, 250);
}

void TableView::setText(int row, int col, const QString &text)
{
    QVector<QStringList> datavector = m_model->DataVector();
    QStringList list = datavector.at(row);
    list.replace(col, text);
    datavector.replace(row, list);
    m_model->setLongData(datavector);
    emit m_model->layoutChanged();
}

void TableView::setProcess(int row, const int& nProcess)
{
    setText(row, 1, QString::number(nProcess));
}

void TableView::clear()
{
    QVector<QStringList> data;
    QVector<QStringList> datavector = m_model->DataVector();
    int colNum = datavector[0].length();
    for(int i = 0; i < 32; i++)
    {
        QStringList sl;
        for(int j = 0; j < colNum; j++)
            sl.append(" ");
        data.append(sl);
    }
    m_model->setLongData(data);
    emit m_model->layoutChanged();
}
