INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS                += $$PWD/progress_delegate.h \
                          $$PWD/tablemodel.h \
                          $$PWD/TableView.h \

SOURCES                += $$PWD/progress_delegate.cpp \
                          $$PWD/tablemodel.cpp \
                          $$PWD/TableView.cpp \
