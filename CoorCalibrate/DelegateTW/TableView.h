#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QTableView>

class TableModel;
class ProgressBarDelegate;

class TableView : public QTableView
{
    Q_OBJECT
public:
    explicit TableView(QWidget *parent = 0);
    TableView(QStringList& ListHead, const int& nCol);
    TableModel* tableModel() {return m_model;}
    void iniData(QStringList ListHead, int nCol);
    ~TableView();

    void setText(int row, int col, const QString &text);
    void setProcess(int row, const int& nProcess);
    void clear();
signals:

public slots:

private:


private:
    TableModel *m_model;
    ProgressBarDelegate *m_progressBarDelegate;

};

#endif // TABLEVIEW_H
