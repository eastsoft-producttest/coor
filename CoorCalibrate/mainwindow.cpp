﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QTimer>

#include <QSerialPortInfo>
#include <QMessageBox>
#include "coor_command.h"
#include "com_port_coor.h"
#include <QDir>

using namespace coor_command;

#define DO_DEBUG 1
#include "coorView.h"

#include <QTextCodec>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //*****单例数据交互*****//
    m_pSharer = SigletonSharer::getInstance();
    m_pSharer->m_BarReadWay = m_ini.get_BarReadWay();
    m_pSharer->m_maxFailNum = m_ini.get_max_fail_num();
    m_ini.get_MES_setting(m_pSharer->m_tStationInfo);
    m_pSharer->m_PackInterval = m_ini.get_pack_interval();
    m_pSharer->m_MeterType = m_ini.get_MeterType();
    //*****日志初始化*****//
    m_pLogger = &Logger::instance();
    m_pLogger->setLoggingLevel(QsLogging::TraceLevel);
    const QString sLogPath(QDir(QApplication::applicationDirPath()).filePath("log/log.log"));
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
          sLogPath, EnableLogRotation, MaxSizeBytes(512*1024), MaxOldLogCount(200)));
    //注意，上面的最大log数量修改完后还要将"QLogDestFile.cpp"中的MaxBackupCount修改
    m_pLogger->addDestination(fileDestination);
    DestinationPtr objectDestination(DestinationFactory::MakeFunctorDestination(this, SLOT(slot_write_log(QString,int))));
    m_pLogger->addDestination(objectDestination);


    //**********表台设置参数读取**********//
    //表台编号
    m_sStationNo = m_ini.get_plat_NO();
    ui->cb_StationNo->setCurrentText(m_sStationNo);
    //扫枪串口与标准表设备地址
    QList<QSerialPortInfo> ports_info = QSerialPortInfo::availablePorts();
    foreach(QSerialPortInfo info, ports_info)
    {
        ui->comboBox->addItem(info.portName());
        ui->cb_DevPort->addItem(info.portName());
    }
    QString sScanPort = m_ini.get_scanner_com();
    ui->comboBox->setCurrentText(sScanPort);
    ui->cb_DevPort->setCurrentText(m_ini.get_dev_com());
    //音频设备与音源
    ui->cb_Audio->addItem("Default", QString("default"));
    foreach (QString engine, QTextToSpeech::availableEngines())
        ui->cb_Audio->addItem(engine, engine);
    ui->cb_Audio->setCurrentIndex(0);
    engineSelected(0);
    //流程选择
    ui->cb_pattern->setCurrentIndex(m_ini.get_work_pattern() - 1);
    //标准表型号
    ui->cb_standardMeter->setCurrentText(m_ini.get_standardMeter());
    //获取条码方式
    ui->cb_readBarWay->setCurrentIndex(m_ini.get_BarReadWay());

    //最大运行失败数量

    //**********MES设置**********//

    ui->cb_MesEnable->setChecked(m_pSharer->m_tStationInfo.m_EnableMes);
    ui->cb_PassPack->setChecked(m_pSharer->m_tStationInfo.m_PassPack);
    ui->le_WorkStation->setText(m_pSharer->m_tStationInfo.m_sWorkStation);
    ui->le_WorkStation_pack->setText(m_pSharer->m_tStationInfo.m_sWorkStation_pack);
    ui->le_MO->setText(m_pSharer->m_tStationInfo.m_sMo);
    ui->le_Operator->setText(m_pSharer->m_tStationInfo.m_sOperator);

    //**********业务数据设置**********//
    //业务数据读取
    T_COOR_CALI_PARA para = m_ini.get_coor_cali_para();
    ui->le_StdVolt->setText(para.m_nStdVolt);
    ui->le_StdCurr->setText(para.m_nStdCurr);
    ui->le_MaxCurr->setText(para.m_nImax);
    ui->le_VoltLimit->setText(para.m_nVoltErrLimit);
    ui->le_IbLimit->setText(para.m_nCurrErrLimit);
    ui->le_ImaxLimit->setText(para.m_nImaxErrLimit);
    ui->le_lightErrorLimit->setText(para.m_nLightErrLimit);
    ui->le_ErrorLimitHx->setText(para.m_nHexiangErrLimit);
    ui->le_ErrorLimitReactive->setText(para.m_nReactiveErrLimit);
    ui->le_Constant->setText(para.m_nConstant);
    ui->le_Pause->setText(para.m_nPluse);
    ui->le_Frequence->setText(para.m_nStdClockFreq);
    ui->le_ClockError->setText(para.m_nClockErrLimit);
    ui->le_ClockTestDuration->setText(para.m_nClockTestDuration);

    //**********图形项初始化**********//
    QStringList ListPort = m_ini.get_com_port_list();
    QStringList TTUListPort = m_ini.get_ttu_com_port_list();
    unsigned int OnLineState = m_ini.get_online_bitmap();
    m_grphV = new coorView(ListPort, TTUListPort, this, 32, m_ini.get_online_bitmap());
    ui->verticalLayout->addWidget(m_grphV);

    //**********表格初始化**********//
    ui->tableWidget->iniData(sListAutoCal, 32);
    ui->tableWidget_2->iniData(sListAutoCheck, 32);

    //**********建立集中器对象***********//
    QString PlatNo = m_ini.get_plat_NO();
    for(int i = 0; i < 32; i++)
    {
        bool enable = OnLineState & (0x1 << i);
        OneCoorCaliCheckProcess* process =
                new OneCoorCaliCheckProcess(ListPort[i], TTUListPort[i], i+1, enable, PlatNo);
        QThread* pThread = new QThread(this);
        process->moveToThread(pThread);
        //因espdport在构造中进行了new，所以其实际上是属于mainwindow的
        //这一步是为了将其移至线程中
        //process->m_pEspdPort->moveToThread(pThread);
        connect(this, &MainWindow::start_cali_thread, process, &OneCoorCaliCheckProcess::do_cali_process);
        connect(this, &MainWindow::start_check_thread, process, &OneCoorCaliCheckProcess::do_check_process);
        connect(this, &MainWindow::start_read_mac, process, &OneCoorCaliCheckProcess::slot_start_readMac);
        connect(process, &OneCoorCaliCheckProcess::sig_process, this, &MainWindow::slot_recv_progress);
        connect(process, &OneCoorCaliCheckProcess::sig_text, this, &MainWindow::slot_recv_text);
        connect(process, &OneCoorCaliCheckProcess::sig_result, this, &MainWindow::slot_thread_finished);
        connect(process, &OneCoorCaliCheckProcess::sig_port_error, this, &MainWindow::slot_recv_port_error);
        m_pListProcess.append(process);
        m_pListThreads.append(pThread);
        pThread->start();
    }

    //设置模拟表,其他位置不用设置
    m_pSimu = simu_coor::get_instance();
    if(0 > m_pSimu->simu_coor_load())
    {
        QMessageBox::warning(nullptr, "无法加载dll", "无法加载dll，请检查!");
        QTimer::singleShot(0, this, SLOT(close()));
    }

    //打开扫枪串口
    PortSettings setting;
    setting.BaudRate = BAUD115200;
    setting.DataBits = DATA_8;
    setting.Parity = PAR_EVEN;
    setting.StopBits = STOP_1;
    setting.FlowControl = FLOW_OFF;
    setting.Timeout_Millisec = 10;
    m_pScanSerial = new QextSerialPort(sScanPort, setting);
    connect(m_pScanSerial, &QextSerialPort::readyRead, this, &MainWindow::slot_scan_port_rcv);
    if(!m_pScanSerial->open(QIODevice::ReadWrite))
    {
        QMessageBox::warning(nullptr, "", "扫枪串口打开失败，请确认！");
    }

    //校表检表模式选择
    if(m_ini.get_work_pattern() == 1)
    {
        m_pRunnTableView = ui->tableWidget;
        ui->tabWidget->setTabEnabled(2, false);
        ui->tabWidget->setTabEnabled(1, true);
    }
    else
    {
        m_pRunnTableView = ui->tableWidget_2;
        ui->tabWidget->setTabEnabled(1, false);
        ui->tabWidget->setTabEnabled(2, true);
    }
    //添加菜单栏
    set_menu();
    QLOG_TRACE() << tr("系统启动");
}

MainWindow::~MainWindow()
{
    delete ui;
    //logModule::logClose("logs.log");
    QLOG_TRACE() << tr("系统关闭");
    m_pLogger->destroyInstance();
    for(QThread* p : m_pListThreads)
    {
        p->quit();
        p->wait();
        delete p;
        p = nullptr;
    }
    m_pListThreads.clear();

    for(OneCoorCaliCheckProcess* p:m_pListProcess)
    {
        delete p;
        p = nullptr;
    }
    m_pListProcess.clear();

    m_pScanSerial->close();
    delete m_pScanSerial;
    m_pScanSerial = nullptr;

    delete m_pSimu;
    m_pSimu = nullptr;

    delete m_pSharer;
    m_pSharer = nullptr;

    delete m_grphV;
    m_grphV = nullptr;

}
void MainWindow::closeEvent(QCloseEvent *event)
{
    int button;
    button = QMessageBox::question(this, tr("关闭"),
             QString(tr("确认退出程序？")), QMessageBox::Yes | QMessageBox::No);
    if (button == QMessageBox::No) {
        event->ignore();
    }
    else if (button == QMessageBox::Yes)
    {
        QLOG_TRACE() << tr("系统关闭");
        m_pLogger->destroyInstance();
        event->accept();
        QProcess* pprocess = new QProcess();
        pprocess->execute("Taskkill -f -im CoorCalibrate.exe\r\n");
        //pprocess->execute("Taskkill -f -im Double_4GBD.exe -t\r\n");
    }
}

//统一设置第一列状态
void MainWindow::set_status_all(int nTestType, const QString& sStatus)
{
    unsigned int OnLineState = m_ini.get_online_bitmap();
    for(int i = 0; i < 32; i++)
    {
        if(OnLineState & (1<<i))
        {
            if(nTestType == 1)
            {
                ui->tableWidget->setText(0, i, sStatus);
            }
            if(nTestType == 2)
            {
                ui->tableWidget_2->setText(0, i, sStatus);
            }
        }
    }
}
//每次重新开始测试需要初始化的参数放这里
void MainWindow::initial_params()
{
    m_unScannedIndexBitmap = 0;
    m_nFinishedThreadNun = 0;
    ui->tableWidget->clear();
    ui->textEdit->clear();
}
//
void MainWindow::engineSelected(int index)
{
    QString engineName = ui->cb_Audio->itemData(index).toString();
    delete m_speech;
    if (engineName == "default")
        m_speech = new QTextToSpeech(this);
    else
        m_speech = new QTextToSpeech(engineName, this);
    if (m_speech->state() == QTextToSpeech::BackendError)
    {
        QLOG_TRACE() << "音频设备打开失败-1";
        m_speech_OK = false;
    }
    else
    {
        m_speech->setRate(1 / 10.0);
        m_speech->setPitch(5 / 10.0);
        m_speech->setVolume(80 / 100.0);
        QLocale locale = QVariant("Chinese (China)").toLocale();

        bool is_haveLocale = false;
        if (m_speech->availableLocales().count() == 0)
        {
            QLOG_TRACE() << "音频设备打开失败-2";
            m_speech_OK = false;
            return;
        }
        foreach (QLocale temp, m_speech->availableLocales()) {
            if(temp.name().indexOf("CN") >-1 )
                is_haveLocale = true;
        }
        if(is_haveLocale)
        {
            localeChanged(locale);
        }
        else
        {
            QLOG_TRACE() << "音频设备打开失败-3";
            m_speech_OK = false;
        }
    }
}

void MainWindow::localeChanged(const QLocale &locale)
{
    QVariant localeVariant(locale);
    m_speech->setLocale(locale);
    //ui.language->setCurrentIndex(ui.language->findData(localeVariant));

    disconnect(ui->cb_vioce, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::voiceSelected);
    ui->cb_vioce->clear();
    qDebug() <<  m_speech->availableVoices().count();
    if(m_speech->availableVoices().count() == 0)
    {
        m_speech_OK = false;
        //QMessageBox mesg;
        QLOG_TRACE() << "音源加载失败";
        //mesg.information(this, "通知", "音源加载失败");
        return;
    }
    m_voices = m_speech->availableVoices();
//    QLOG_TRACE() << m_voices.size();
    QVoice currentVoice = m_speech->voice();
    foreach (const QVoice &voice, m_voices) {
        ui->cb_vioce->addItem(QString("%1 - %2 - %3").arg(voice.name())
                          .arg(QVoice::genderName(voice.gender()))
                          .arg(QVoice::ageName(voice.age())));
        if (voice.name() == currentVoice.name())
            ui->cb_vioce->setCurrentIndex(ui->cb_vioce->count() - 1);
    }
    connect(ui->cb_vioce, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &MainWindow::voiceSelected);
    m_speech_OK = true;
}

void MainWindow::voiceSelected(int index)
{
    m_speech->setVoice(m_voices.at(index));
}


void MainWindow::on_pb_StartCali_clicked()
{
    if(m_nMesCheckState <=0)
    {
        QLOG_TRACE() << "获取MES信息尚未成功，请确认是否开始";
        if(m_speech_OK)
            m_speech->say("获取MES信息尚未成功，请确认是否开始");
        int iRet = QMessageBox::warning(nullptr, "", "获取MES信息尚未成功，请确认是否开始", QMessageBox::Ok|QMessageBox::Cancel);
        if(iRet == QMessageBox::Cancel)
        {
            return;
        }
    }

    m_pSharer->m_nStop = 0;
    //当前为扫码模式才会判定数量，否则直接进入流程
    if((m_unScannedIndexBitmap != m_ini.get_online_bitmap()) && (m_pSharer->m_BarReadWay == 0
                      || m_pSharer->m_MeterType == 1))
    {
        QLOG_TRACE() << "扫码数量与设定数量不一致，无法启动";
        if(m_speech_OK)
            m_speech->say("扫码数量与设定数量不一致，无法启动");
        return;
    }
    //几个初始化动作
    m_nFinishedThreadNun = 0;
    ui->tableWidget->clear();
    //ui->textEdit->clear();

    m_pSimu->set_running_num(32);
    emit start_cali_thread();
    m_nRunningFrame = 1;


    QString str = "表台" + m_sStationNo + "开始校表";
    if(m_speech_OK)
        m_speech->say(str);
    m_nFinishedThreadNun = 0;
    QLOG_TRACE() << str;
}


void MainWindow::on_pb_StartCheck_clicked()
{
#ifndef DO_DEBUG
    if(m_nMesCheckState <=0)
    {
        m_speech->say("获取MES信息尚未成功，请确认是否开始");
        int iRet = QMessageBox::warning(nullptr, "", "获取MES信息尚未成功，请确认是否开始", QMessageBox::Ok|QMessageBox::Cancel);
        if(iRet == QMessageBox::Cancel)
        {
            return;
        }
    }
#endif
    m_pSharer->m_nStop = 0;

    if((m_unScannedIndexBitmap != m_ini.get_online_bitmap()) && m_pSharer->m_BarReadWay == 0)
    {
        QLOG_TRACE() << "扫码数量与设定数量不一致，无法启动";
        if(m_speech_OK)
            m_speech->say("扫码数量与设定数量不一致，无法启动");
        return;
    }
    //几个初始化动作
    m_nFinishedThreadNun = 0;

    m_pSimu->set_running_num(32);
    emit start_check_thread();
    m_nRunningFrame = 2;
    QString str = "表台" + m_sStationNo + "开始检表";
    if(m_speech_OK)
        m_speech->say(str);
    QLOG_TRACE() << "str";
    m_nFinishedThreadNun = 0;
}

void MainWindow::slot_recv_progress(int nProgress)
{
    OneCoorCaliCheckProcess *pSender = (OneCoorCaliCheckProcess*)sender();
    int nIndex = m_pListProcess.indexOf(pSender);
    m_pRunnTableView->setProcess(nIndex, nProgress);
//    else
//        QLOG_TRACE() << "未知进度信息";
}

void MainWindow::slot_recv_text(int nStartCol, QStringList sListText)
{
    OneCoorCaliCheckProcess *pSender = (OneCoorCaliCheckProcess*)sender();
    int nIndex = m_pListProcess.indexOf(pSender);
    for(int i = 0; i < sListText.size(); i++)
    {
        m_pRunnTableView->setText(nIndex, nStartCol+i, sListText[i]);
    }
}

void MainWindow::slot_recv_port_error(int n_serial)
{
    QMessageBox mesg;
    mesg.question(this, "询问", QString("%1号表位串口打开失败,是否继续?").arg(n_serial));
}

void MainWindow::slot_thread_finished()
{
    m_nFinishedThreadNun++;
    //m_pSimu->set_running_num(32 - m_nFinishedThreadNun);
    QLOG_TRACE() << "线程退出数量:" <<m_nFinishedThreadNun;
    QString sFeature;
    if(m_nRunningFrame == 1)
        sFeature = "校表";
    else
        sFeature = "检表";
    QString str = "表台" + m_sStationNo + sFeature + "结束， 请操作人员确认！";
    if(m_nFinishedThreadNun == 32)
    {
        QLOG_TRACE() << str;
        if(m_speech_OK)
            m_speech->say(str);
    }
}



eScanType MainWindow::scan_type(QByteArray& ba_toJudge)
{

    //过滤前面的0d0a
    QByteArray ba_check;
    ba_check.append(0x0d);
    //ba_check.append(0x0a);
    int nRNum = ba_toJudge.count(ba_check);
    if(nRNum < 1)
    {
        return ERROR_MSG;
    }
    else
    {
        int nPosStop = ba_toJudge.indexOf(ba_check);
        m_baScannerBuff = m_baScannerBuff.left(nPosStop);
        QString sRight = ba_toJudge;
        if(sRight.indexOf("start work") >= 0)
        {
            return START_WORK;
        }
        else if(sRight.indexOf("start scan") >= 0)
        {
            return START_SCAN;
        }
        else if(sRight.indexOf("stop work") >= 0)
        {
            return STOP_WORK;
        }
        else if(sRight.indexOf("get mes data") >= 0)
        {
            return GET_MES_DATA;
        }
        else
        {
            return BAR_CODE;
        }
    }
}

void MainWindow::slot_scan_port_rcv()
{
    m_baScannerBuff += m_pScanSerial->readAll();
    QLOG_TRACE() << m_baScannerBuff;
    eScanType t = scan_type(m_baScannerBuff);
    if(ERROR_MSG == t)
    {
        QLOG_TRACE() << "扫描结果错误";
        if(m_speech_OK)
            m_speech->say("扫描结果错误，请重新扫描");
        m_baScannerBuff.clear();
        return;
    }
    else if(START_WORK == t)
    {
        m_baScannerBuff.clear();

        //通过bitmap判断是不是可以启动
        if(m_ini.get_work_pattern() == 1)
        {
            this->on_pb_StartCali_clicked();
        }
        else if(m_ini.get_work_pattern() == 2)
        {
            this->on_pb_StartCheck_clicked();
        }
        return;
    }
    else if(START_SCAN == t)
    {
        //如果为读MAC地址则直接返回，不允许扫码
        if(m_pSharer->m_BarReadWay == 1)
        {
            QMessageBox mesg;
            mesg.warning(this, "警告", "当前为MAC读取条码模式");
            return;
        }
        //参数初始化
        this->initial_params();
        QLOG_TRACE() << m_ini.get_plat_NO() + "号表台，请操作员开始扫码";
        if(m_speech_OK)
            m_speech->say(m_ini.get_plat_NO() + "号表台，请操作员开始扫码");
        m_baScannerBuff.clear();
        m_bCanScanBarcode = true;
        return;
    }
    else if(STOP_WORK == t)
    {
        //停止供电等
        m_pSharer->m_nStop = 3;
        m_baScannerBuff.clear();
        return;
    }
    else if(GET_MES_DATA == t)
    {
        //停止供电等
        this->on_pb_check_em_clicked();

        m_baScannerBuff.clear();
        return;
    }
    else//bar code
    {
        //如果为读MAC地址则直接返回，不允许扫码
        if(m_pSharer->m_BarReadWay == 1)
        {
            QMessageBox mesg;
            mesg.warning(this, "警告", "当前为MAC读取条码模式");
            return;
        }
        //未进入scan时无法扫码
        if(!m_bCanScanBarcode)
        {
            m_baScannerBuff.clear();
            QLOG_TRACE() << "无法扫条码，请确认工作状态";
            return;
        }
        //判断是否有重复
        for(int i = 0; i < 32; i++)
        {
            unsigned int b = m_unScannedIndexBitmap & (1 << i);
            if(b)
            {
                if(m_pListProcess[i]->get_bar_code().indexOf(m_baScannerBuff) >= 0)
                {
                    QLOG_TRACE() << "条码重复，请检查！";
                    if(m_speech_OK)
                        m_speech->say("条码重复");
                    m_baScannerBuff.clear();
                    return;
                }
            }
        }
        unsigned int  online_bitmap = m_ini.get_online_bitmap();
        QLOG_TRACE() << online_bitmap;
        int i = 0;
        bool bfind = false;
        while(i < 32)
        {
            unsigned int b1 = online_bitmap & (1 << i);
            unsigned int b2 = m_unScannedIndexBitmap & (1 << i);
            //unsigned int b = b1 & b2;
            if(!b2 && b1)//未扫码
            {
                m_pListProcess[i]->set_bar_code(m_baScannerBuff);
                m_unScannedIndexBitmap |= (1 << i);

                m_pRunnTableView->setText(i,2,m_baScannerBuff);
                QLOG_TRACE() << m_baScannerBuff;
                i++;
                bfind = true;
                break;
            }
            i++;
        }
        unsigned int b = online_bitmap >> i;
        QLOG_TRACE() << "b:" << b;
        if(b == 0 || (bfind && i==32))//扫码完成，确认是否开始
        {
            m_bCanScanBarcode = false;
            QLOG_TRACE() << "扫码完成,请开始测试";
            if(m_speech_OK)
                m_speech->say("扫码完成，请开始测试");
        }
        m_baScannerBuff.clear();
    }
}



void MainWindow::on_cb_pattern_currentIndexChanged(const QString &arg1)
{
    m_ini.set_work_pattern(arg1);
    QLOG_TRACE() << "work pattern:" << arg1;
    if(arg1.indexOf("1-") >= 0)
    {
        m_pRunnTableView = ui->tableWidget;
        ui->tabWidget->setTabEnabled(2, false);
        ui->tabWidget->setTabEnabled(1, true);
    }
    else
    {
        m_pRunnTableView = ui->tableWidget_2;
        ui->tabWidget->setTabEnabled(1, false);
        ui->tabWidget->setTabEnabled(2, true);
    }
}


void MainWindow::on_le_WorkStation_editingFinished()
{
    m_ini.set_work_center(ui->le_WorkStation->text());
}

void MainWindow::on_le_WorkStation_pack_editingFinished()
{
    m_ini.set_work_center_pack(ui->le_WorkStation_pack->text());
}


void MainWindow::on_le_Operator_editingFinished()
{
    m_ini.set_operator(ui->le_Operator->text());
}

void MainWindow::on_le_MO_editingFinished()
{
    m_ini.set_order_num(ui->le_MO->text());
}


void MainWindow::on_pb_StartScan_02_clicked()
{
    m_baScannerBuff = "start scan\r\n";
    slot_scan_port_rcv();
}

void MainWindow::on_pb_StartScan_clicked()
{
    m_baScannerBuff = "start scan\r\n";
    slot_scan_port_rcv();
}

void MainWindow::on_pb_check_em_clicked()
{
    //任意选一个执行对象来进行MES数据检查，数据存储在单例中
    OneCoorCaliCheckProcess* runner = m_pListProcess.at(0);
    int iRet = runner->get_cali_ws(ui->le_WorkStation->text());
    if(iRet != SOAP_OK)
    {

        if(m_speech_OK)
            m_speech->say("获取校表站工作中心信息失败, 请确认！");
        QMessageBox::warning(nullptr, "", "获取校表站工作中心信息失败, 错误码:" + QString::number(iRet));
        QLOG_TRACE() << "获取校表站工作中心信息失败, 错误码:" + QString::number(iRet);
        m_nMesCheckState = -1;
        return;
    }
    iRet = runner->get_pack_ws(ui->le_WorkStation_pack->text());
    if(iRet != SOAP_OK)
    {
        if(m_speech_OK)
            m_speech->say("获取包装线工作中心信息失败, 请确认！");
        QMessageBox::warning(nullptr, "", "获取包装线工作中心信息失败,错误码:" + QString::number(iRet));
        QLOG_TRACE() << "获取包装线工作中心信息失败,错误码:" + QString::number(iRet);
        m_nMesCheckState = -2;
        return;
    }
    iRet = runner->get_mo(ui->le_MO->text());
    if(iRet != SOAP_OK)
    {
        if(m_speech_OK)
            m_speech->say("获取制令单号信息失败, 请确认！");
        QMessageBox::warning(nullptr, "", "获取制令单号信息失败,错误码:" + QString::number(iRet));
        QLOG_TRACE() << "获取制令单号信息失败,错误码:" + QString::number(iRet);
        m_nMesCheckState = -3;
        return;
    }
    iRet = runner->get_operator(ui->le_Operator->text());
    if(iRet != SOAP_OK)
    {
        if(m_speech_OK)
            m_speech->say("获取操作员信息失败, 请确认！");
        QMessageBox::warning(nullptr, "", "获取操作员信息失败,错误码:" + QString::number(iRet));
        QLOG_TRACE() << "获取操作员信息失败,错误码:" + QString::number(iRet);
        m_nMesCheckState = -4;
        return;
    }
    m_nMesCheckState = 1;
    return;
}


void MainWindow::on_pb_terminal_cali_clicked()
{
    m_pSharer->m_nStop = 1;
    QLOG_TRACE() << "系统手动急停";
}

void MainWindow::on_pb_terminal_clicked()
{
    m_pSharer->m_nStop = 2;
    QLOG_TRACE() << "系统手动急停";
}

void MainWindow::on_cb_StationNo_currentIndexChanged(const QString &arg1)
{
    m_ini.set_plat_NO(arg1);
    m_sStationNo = arg1;
}

void MainWindow::on_cb_MesEnable_stateChanged(int arg1)
{
    m_ini.set_enable_mes(arg1);
}

void MainWindow::on_cb_PassPack_stateChanged(int arg1)
{
    m_ini.set_enable_passpack(arg1);
}

void MainWindow::on_cb_standardMeter_currentIndexChanged(int index)
{
    Q_UNUSED(index);
    m_ini.set_standardMeter(ui->cb_standardMeter->currentText());
}

void MainWindow::slot_write_log(const QString& msg, int level)
{
    Q_UNUSED(level);
    ui->textEdit->append(msg );//+ "" + QString::number(level)
    ui->textEdit->document()->setMaximumBlockCount(5000);
    //ui->textEdit->moveCursor(QTextCursor::End);
}

void MainWindow::get_iniSetting()
{
    //获取工作站信息

}
void MainWindow::set_iniSetting()
{

}
#if 0
void MainWindow::on_pb_get_ws_info_clicked()
{
    MesNewWebServiceSoapProxy soap;
    _ns1__GetWorkStation getStation;
    std::wstring sSta = (ui->le_WorkStation->text().toStdWString());
    getStation.workStationCode = &sSta;
    _ns1__GetWorkStationResponse res;
    int iRet = soap.GetWorkStation_(&getStation, res);//(ui->le_MO->text());
    QString sStation = QString::fromStdWString(*(res.GetWorkStationResult));
    if(iRet == SOAP_OK)
    {
        QLOG_TRACE() << sStation;
        sStation.remove("[");
        sStation.remove("]");
        qDebug() << "workstation:" <<sStation;
        if(sStation.length() < 5)
            return ;
        QByteArray baJason = sStation.toUtf8();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return ;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_WorStationID = QString::number(rootObj["WORK_STATIONID"].toInt());
        m_pSharer->m_tStationInfo.m_sWorkStationName = rootObj["WORK_STATIONNAME"].toString();

        QLOG_TRACE() << m_pSharer->m_tStationInfo.m_sWorkStationName << m_pSharer->m_tStationInfo.m_WorStationID;
    }
}

void MainWindow::on_pb_check_em_clicked()
{
    MesNewWebServiceSoapProxy soap;
    _ns1__GetEmp emp;
    std::wstring s1 = (m_pSharer->m_tStationInfo.m_sOperator).toStdWString();
    emp.empCode = &s1;
    _ns1__GetEmpResponse res;
    int bRet = soap.GetEmp(&emp, res);

    QString sOper = QString::fromStdWString(*(res.GetEmpResult));
    if(bRet == SOAP_OK)
    {
        QLOG_TRACE() << sOper;
        sOper.remove("[");
        sOper.remove("]");
        qDebug() << "EMP:" <<sOper;

        if(sOper.length() < 5)
            return;
        QByteArray baJason = sOper.toUtf8();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_sOperatorName = rootObj["EMP_NAME"].toString();
    }
}

void MainWindow::on_pb_check_mo_clicked()
{
    MesNewWebServiceSoapProxy soap;
    _ns1__GetMo mo;
    std::wstring smo = (ui->le_MO->text().toStdWString());
    mo.moCode = &smo;
    _ns1__GetMoResponse res;
    int bRet = soap.GetMo_(&mo, res);//(ui->le_MO->text());
    QString sMo = QString::fromStdWString(*(res.GetMoResult));
    if(bRet == SOAP_OK)
    {
        QLOG_TRACE() << sMo;
        QByteArray baJason = sMo.toLatin1();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return;// false;
        }
        QJsonObject rootObj = jsonDoc.object();

        QString sCloseFlag = rootObj["CLOSE_FLAG"].toString();
        if(sCloseFlag.indexOf("3") >=0 || sCloseFlag.indexOf("6") >=0)
            m_pSharer->m_tStationInfo.m_bCloseFlag = true;// false;
        else
            m_pSharer->m_tStationInfo.m_bCloseFlag = false;// true;
    }
}
#endif





void MainWindow::on_pb_confirm_clicked()
{
    //*****保存表台设置*****//
    //表台编号
    m_ini.set_plat_NO(ui->cb_StationNo->currentText());
    //扫描枪串口
    m_ini.set_scanner_com(ui->comboBox->currentText());
    //标准表设备地址
    m_ini.set_dev_com(ui->cb_DevPort->currentText());
    //流程选择
    m_ini.set_work_pattern(ui->cb_pattern->currentText());
    //标准表型号选择
    m_ini.set_standardMeter(ui->cb_standardMeter->currentText());
    //读取条码方式选择
    m_ini.set_BarReadWay(ui->cb_readBarWay->currentIndex());
    //*****保存MES设置*****//
    m_ini.set_enable_mes(ui->cb_MesEnable->isChecked());
    m_ini.set_enable_passpack(ui->cb_PassPack->isChecked());
    m_ini.set_work_center(ui->le_WorkStation->text());
    m_ini.set_work_center_pack(ui->le_WorkStation_pack->text());
    m_ini.set_order_num(ui->le_MO->text());
    m_ini.set_operator(ui->le_Operator->text());
    //*****保存校检表参数*****//
    T_COOR_CALI_PARA para;
    para.m_nStdVolt = ui->le_StdVolt->text();
    para.m_nStdCurr = ui->le_StdCurr->text();
    para.m_nImax = ui->le_MaxCurr->text();
    para.m_nVoltErrLimit = ui->le_VoltLimit->text();
    para.m_nCurrErrLimit = ui->le_IbLimit->text();
    para.m_nImaxErrLimit = ui->le_ImaxLimit->text();
    para.m_nLightErrLimit = ui->le_lightErrorLimit->text();
    para.m_nHexiangErrLimit = ui->le_ErrorLimitHx->text();
    para.m_nReactiveErrLimit = ui->le_ErrorLimitReactive->text();
    para.m_nConstant = ui->le_Constant->text();
    para.m_nPluse = ui->le_Pause->text();
    para.m_nStdClockFreq = ui->le_Frequence->text();
    para.m_nClockErrLimit = ui->le_ClockError->text();
    para.m_nClockTestDuration = ui->le_ClockTestDuration->text();
    m_ini.set_coor_cali_para(para);

    //*****保存集中器位图*****//
    QStringList list = m_grphV->get_com_list();
    m_ini.set_com_port_list(list);
    QStringList ttuList = m_grphV->get_ttu_com_list();
    m_ini.set_ttu_com_port_list(ttuList);
    unsigned int unState = m_grphV->get_com_state();
    m_ini.set_online_bitmap(unState);

    QMessageBox::warning(nullptr,"提示","参数配置修改，请重启软件");//, QMessageBox::Ok | QMessageBox::Cancel);
//    if(nClick == QMessageBox::Ok)
//    {
//        QLOG_TRACE() << "重启";
//        qApp->exit(901);
//    }
}
//开启所有集中器
void MainWindow::on_pb_OpenAll_clicked()
{
    for(int i = 0; i < 32; i++)
    {
        m_grphV->set_item_state(i, STATE_CLOSE);
    }
    m_grphV->setFocus();
    m_ini.set_online_bitmap(0xffffffff);
}
//关闭所有集中器
void MainWindow::on_pb_CloseAll_clicked()
{
    for(int i = 0; i < 32; i++)
    {
        m_grphV->set_item_state(i, STATE_IGNORE);
    }
    m_grphV->setFocus();
    m_ini.set_online_bitmap(0);
}
//根据第一个集中器的串口加载其他串口
void MainWindow::on_pb_LoadingSerial_clicked()
{
    m_grphV->set_comlist_auto();
    m_grphV->move_item();
    //QStringList list = m_grphV->get_com_list();
    //m_ini.set_com_port_list(list);
}
void MainWindow::on_pb_loadingTermainal_clicked()
{
    m_grphV->set_ttu_comlist_auto();
    m_grphV->move_item();
    //QStringList list = m_grphV->get_ttu_com_list();
    //m_ini.set_ttu_com_port_list(list);
}

void MainWindow::set_menu()
{
    //加载菜单
    m_settingsUI = new Settings(this);
    m_settingsUI->setModal(true);
    m_UISerialSet = new ui_serial_set(this);
    m_UISerialSet->setModal(true);
    m_UIGeneralSet = new ui_GeneralSet(this);
    m_UIGeneralSet->setModal(true);

    QMenu* setting = new QMenu("设置");
    QAction* test_setting = new QAction("测试用");
    QAction* set_general = new QAction("通用设置");
    QAction* set_SerialPara = new QAction("串口设置");

    connect(test_setting, &QAction::triggered, this, &MainWindow::slot_setting_ui);
    connect(set_general, &QAction::triggered, this, &MainWindow::slot_ui_general_set);
    connect(m_UIGeneralSet, &ui_GeneralSet::sig_close, this, &MainWindow::slot_ui_general_changed);
    setting->addAction(test_setting);
    setting->addAction(set_general);
    setting->addAction(set_SerialPara);
    this->menuBar()->addMenu(setting);


    set_TTUCali_show(m_pSharer->m_MeterType);
}
void MainWindow::slot_setting_ui()
{
    m_settingsUI->show();
}
void MainWindow::slot_ui_Serial_set()
{
    m_UISerialSet->show();
}
void MainWindow::slot_ui_general_set()
{
    m_UIGeneralSet->show();
}
void MainWindow::slot_ui_general_changed()
{
    set_TTUCali_show(m_pSharer->m_MeterType);
    QLOG_TRACE() << "当前最大单轮测试容许失败量:" << m_pSharer->m_maxFailNum;
}

void MainWindow::set_TTUCali_show(bool status)
{
    QString str;
    if(status)
        str = "台区智能终端";
    else
        str = "集中器";
    QLOG_TRACE() << "当前校表类型:" + str;
    m_grphV->set_ttu_combox_visiable(status);
    ui->pb_loadingTermainal->setVisible(status);
    ui->lb_loadingTermainal->setVisible(status);
}

