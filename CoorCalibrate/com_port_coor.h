﻿#ifndef COM_PORT_COOR_H
#define COM_PORT_COOR_H
#include "qextserialport.h"

#include <QObject>
#include "coor_command.h"
#include "QsLog/QsLog.h"
#include "sigletonsharer.h"
using namespace QsLogging;

using namespace coor_command;
/*
#-------------------------------------------------
#
# Class:  集中器端口通信类
# Author: lidl
# Date:   2020-09-03
#
#-------------------------------------------------
*/
typedef enum{
    TYPE_NULL = 0,
    TYPE_INITIAL,
    TYPE_GET_VOLT,
    TYPE_GET_CURR,
    TYPE_GET_INSTANT_VAL_OF_BXZD,
    TYPE_SET_VOLT_CURR_DIFF,
    TYPE_SET_POWER_RATIO,
    TYPE_SET_CALI_PHASE_DIFF,
    TYPE_GET_POSITIVE_POWER,//正向有功
    TYPE_INIT_CALI_PHASE_DIFF,
    TYPE_INIT_CALI_POWER_RATIO,
    TYPE_ERROR,
    TYPE_GET_BARCODE        //集中器条码
} COOR_COMM_TYPE;



class com_port_coor : public QextSerialPort
{
    Q_OBJECT
public:
    com_port_coor(const QString& sPortName, int serial);
    ~com_port_coor();
    void set_port_name(const QString &sPortName) {  m_sPortName = sPortName;}
    bool open_coor();
    bool close_coor();
    QByteArray get_right_buffer() { return m_RightPacket;}
    int get_rcv_res(){ return m_nResult;}
    int get_com_serial(){ return m_nSerial;}
    //发送数据
    int send_coor_command(const COOR_COMM_TYPE type, const QString &sAddr = "000000000225");
    int send_coor_command(const COOR_COMM_TYPE type, const int nPhase, const float fDiff,const QString &sAddr = "000000000225");
    //直接输入报文形式
    int send_wait(const QByteArray &bPackSend, const int nCheckPos, const uint8_t flag, QByteArray &bPackRcv,  const int nWaitTime_ms = 10000, const int nSleepTime_ms = 10000);
    //命令类型
    int send_wait(const COOR_COMM_TYPE type, T_RCV_PACK &tPackData, const int nWaitTime_ms = 10000, const QString &sAddr = "000000000225");
    //设置误差类型
    int send_wait(const COOR_COMM_TYPE type, T_RCV_PACK &tPackData, const int nPhase, const float fDiff,const int nWaitTime_ms,const QString &sAddr = "000000000225");
    //写误差
    int send_wait(const T_COOR_SAVE_ERROR  tError, const int nWaitTime_ms);
private:
    //当前发送接收状态
    COOR_COMM_TYPE m_type;
    //自身序列号
    int m_nSerial = -1;
    QByteArray m_RightPacket;
public slots:

    void slot_wait(int serial, const QByteArray &bPackSend, const int nCheckPos, const unsigned char flag, const int nWaitTime_ms = 10000, const int nSleepTime_ms = 10000);
    void slot_get_cmd();
private:
    SigletonSharer *m_pSharer;
    QString m_sPortName;
    QByteArray m_SerialBuff;//串口缓冲区
    int m_nResult = -3;

    //QSLog
    Logger *m_pLogger;
private slots:
    //void on_recv();
};

#endif // COM_PORT_COOR_H
