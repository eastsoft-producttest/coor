#ifndef DATADEF_H
#define DATADEF_H

#endif // DATADEF_H

const QString sArrCode[5] = {QString("PACK_JZQ_Meter"),
                              QString("CHECK_METER")};
//PR:power ratio
//PPH:power phase
//CHP:check power
const QStringList sListAutoCal = QString("条码,Va,Vb,Vc,Ia,Ib,Ic,PR1.0_A,PR1.0_B,PR1.0_C,"
                                         "PPH0.5L_A,PPH0.5L_B,PPH0.5L_C,CHP0.5L_0.1Ib_A,"
                                         "CHP0.5L_0.1Ib_B,CHP0.5L_0.1Ib_C,"
                                         "CHP1.0_Ib_H,CHP0.5L_Ib_H,CHP1.0_0.1Ib_H").split(",");
//const QStringList sListAutoCal = QString("条码,va,vb,vc,ia,ib,ic,ia0,ib0,ic0,ra1,rb1,"
//                                         "rc1,ra2,rb2,rc2,ra3,ra4,ra5,ra6,ra7,log").split(",");
const QStringList sListAutoCheck = QString("条码,Va,Vb,Vc,Ia,Ib,Ic, "
                                           "Va_Imax,Vb_Imax,Vc_Imax,Ia_Imax,Ib_Imax,Ic_Imax,"
                                           "CHP_1.0_Imax_A,CHP_1.0_Imax_B,CHP_1.0_Imax_C,"
                                           "CHP_0.8C_Imax_A,CHP_0.8C_Imax_B,CHP_0.8C_Imax_C,"
                                           "CHP_0.5L_Imax_A,CHP_0.5L_Imax_B,CHP_0.5L_Imax_C,"
                                           "CHP_1.0_Ib_A,CHP_1.0_Ib_B,CHP_1.0_Ib_C,"
                                           "CHP_0.8C_Ib_A,CHP_0.8C_Ib_B,CHP_0.8C_Ib_C,"
                                           "CHP_0.5L_Ib_A,CHP_0.5L_Ib_B,CHP_0.5L_Ib_C,"
                                           "CHP_1.0_0.1Ib_A,CHP_1.0_0.1Ib_B,CHP_1.0_0.1Ib_C,"
                                           "CHP_0.8C_0.1Ib_A,CHP_0.8C_0.1Ib_B,CHP_0.8C_0.1Ib_C,"
                                           "CHP_0.5L_0.1Ib_A,CHP_0.5L_0.1Ib_B,CHP_0.5L_0.1Ib_C,"
                                           "CHP_1.0_Imax_H,CHP_0.8C_Imax_H,CHP_0.5L_Imax_H,"
                                           "CHP_1.0_Ib_H,CHP_0.8C_Ib_H,CHP_0.5L_Ib_H,"
                                           "CHP_1.0_0.1Ib_H,CHP_0.8C_0.1Ib_H,CHP_0.5L_0.1Ib_H,"
                                           "CHP_1.0_0.05Ib_H,CHP_0.8C_0.05Ib_H,CHP_0.5L_0.05Ib_H,TimeCntErr").split(",");

const QStringList sListAutoCheck_ygdx = QString(
                                              "AIm1.0,AIm0.5L,AIb1.0,AIb0.5L,A0.1Ib1.0,A0.2Ib0.5L,"
                                              "BIm1.0,BIm0.5L,BIb1.0,BIb0.5L,B0.1Ib1.0,B0.2Ib0.5L,"
                                              "CIm1.0,CIm0.5L,CIb1.0,CIb0.5L,C0.1Ib1.0,C0.2Ib0.5L").split(",");

const QStringList sListAutoCheck_ygqx = QString(
                                              "Im1.0,Im0.5L,Im0.8C,0.5Im1.0,0.5Im0.5L,"
                                              "0.5Im0.8C,Ib1.0,Ib0.5L,Ib0.8C,"
                                              "0.2Ib0.5L,0.2Ib0.8C,0.1Ib1.0,0.1Ib0.5L,"
                                              "0.1Ib0.8C,0.05Ib1.0").split(",");

const QStringList sListAutoCheck_wgdx = QString(
                                              "AIm1.0,AIm0.5L,AIm0.5C,AIb1.0,AIb0.5L,"
                                              "AIb0.5C,A0.1Ib1.0,A0.2Ib0.5L,A0.2Ib0.5C,"
                                              "BIm1.0,BIm0.5L,BIm0.5C,BIb1.0,BIb0.5L,"
                                              "BIb0.5C,B0.1Ib1.0,B0.2Ib0.5L,B0.2Ib0.5C,"
                                              "CIm1.0,CIm0.5L,CIm0.5C,CIb1.0,CIb0.5L,"
                                              "CIb0.5C,C0.1Ib1.0,C0.2Ib0.5L,C0.2Ib0.5C").split(",");

const QStringList sListAutoCheck_wgqx = QString(
                                              "Im1.0,Im0.5L,Im0.5C,0.5Im1.0,0.5Im0.5L,0.5Im0.5C,"
                                              "Ib1.0,Ib0.5L,Ib0.5C,Ib0.25L,Ib0.25C,0.2Ib0.5L,"
                                              "0.2Ib0.5C,0.1Ib1.0,0.1Ib0.5L,0.1Ib0.5C,0.05Ib1.0").split(",");


const QString asExcelName[4] = {"有功单相","有功全相","无功单相","无功全相"};
