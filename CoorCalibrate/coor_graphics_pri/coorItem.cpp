﻿#include "coorItem.h"
#include <QDebug>
#include <QPainter>
#include <QCursor>
#include <QPen>
#include <QKeyEvent>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>

coorItem::coorItem()
{
    brushcolor = Qt::red;
    //使该图形项可获取焦点
    setFlag(QGraphicsItem::ItemIsFocusable);
}

void coorItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    //画出序号
    painter->setPen(QPen(QColor(80, 80, 80, 200)));
    painter->drawText(QRect(2, 2, 15, 15), QString::number(serial + 1));
    //加载图片并绘制图片
    QPixmap img;
    switch (icoColor) {
    case STATE_IGNORE:
        img.load(":/pic/pics/cco_ignore.png");
        break;
    case STATE_OPEN:
        img.load(":/pic/pics/cco_green.png");
        break;
    case STATE_RUNNING:
        img.load(":/pic/pics/cco_yellow.png");
        break;
    case STATE_CLOSE:
        img.load(":/pic/pics/cco_black.png");
        break;
    case STATE_ERROR:
        img.load(":/pic/pics/cco_red.png");
        break;
    default:
        break;
    }
    //如果当前获取到了焦点，就用框将该图形项框处并放大图片
    if(hasFocus() || isFocusMenu)
    {
        //灰色边框，浅蓝背景
        painter->setPen(QPen(QColor(128, 128, 128, 128)));
        painter->setBrush(QColor(0, 0, 128, 10));
        //获取焦点时图标变大
        img = img.scaled(40, 40);
    }
    else
    {
        painter->setPen(QPen(QColor(255, 255, 255, 128)));
        img = img.scaled(35, 35);

    }
    //抗锯齿渲染
    painter->setRenderHint(QPainter::Antialiasing);
    //根据上面设置的画笔及画刷画出矩形框
    painter->drawRoundedRect(0.0, 0.0, 66.0, 66.0, 10.0, 10.0);
    //图片居中显示
    painter->drawPixmap((66 - img.width()) / 2, (66 - img.height()) / 2, img);

    //QRectF target(QPointF(0.0, 0.0), QSizeF(100.0, 100.0));
    //QRectF source(QPointF(-10.0, -10.0), QSizeF(100.0, 100.0));
    //painter->drawPixmap(target, img, source);
}

QRectF coorItem::boundingRect() const
{
    //标定这个图形项所占的矩形大小及位置
    return QRectF(0, 0, 66, 66);
}

void coorItem::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    //设置焦点
    setFocus();
    //设置箭头样式
    setCursor(Qt::ClosedHandCursor);
    qDebug() << "当前图形项序号为" << serial;
    //控制外部颜色变化，改由外部信号控制
    /*icoColor++;
    if(icoColor == 3)
    {
        icoColor = 0;
    }*/
}

void coorItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *)
{
    //松开鼠标后重置鼠标样式
    setCursor(Qt::ArrowCursor);
    //clearFocus();
}

void coorItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    //创建右键菜单
    isFocusMenu = true;
    QMenu menu;
    QAction* openAction = menu.addAction("enable");
    QAction* closeAction = menu.addAction("disable");
    //获取点击详情
    QAction* selectedAction = menu.exec(event->screenPos());
    if (selectedAction == openAction)
    {
        qDebug() << "enable" << serial << "号集中器";
        this->set_state(STATE_CLOSE);
        emit sig_open_cco(serial);
    }
    else if (selectedAction == closeAction)
    {
        qDebug() << "disable" << serial << "号集中器";
        this->set_state(STATE_IGNORE);
        emit sig_close_cco(serial);
    }
    isFocusMenu = false;
}
