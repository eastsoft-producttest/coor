﻿#ifndef CUSTOMITEM_H
#define CUSTOMITEM_H

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QObject>

typedef enum
{
    STATE_IGNORE,
    STATE_CLOSE,
    STATE_OPEN,
    STATE_RUNNING,
    STATE_ERROR
}COOR_STATE;

class coorItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:
    coorItem();
    //两个必须要重绘的虚函数
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

    int serial = 0;     //序号，用以标记出图形项的顺序
    QString SerialPort = "";    //串口号
    COOR_STATE icoColor = STATE_IGNORE;   //图标颜色,具体见枚举
    void set_state(COOR_STATE state){ icoColor = state;}
protected:
    //鼠标按下与松开事件
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    //右键菜单事件
    void contextMenuEvent(QGraphicsSceneContextMenuEvent* event);
    //鼠标悬浮事件



private:
    QColor brushcolor;
    bool isFocusMenu = false;       //用以避免当焦点移至右键菜单时，图形项因失去焦点而丢失选中框

signals:
    //关闭集中器
    void sig_close_cco(int serial);
    //开启集中器
    void sig_open_cco(int serial);

};

#endif // CUSTOMITEM_H
