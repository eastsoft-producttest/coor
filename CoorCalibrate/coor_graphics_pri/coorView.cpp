﻿#include "coorView.h"
#include <QDebug>
#include <QResizeEvent>
#include <QMainWindow>
#include <QSerialPortInfo>
coorView::coorView(QStringList PortList, QStringList TTUPortList, QWidget *pParent,  const int &nItems, const unsigned int &bitmap)
    : m_nItems(nItems), m_unBitmap(bitmap)
{
    //初始化场景与视图
    m_ObjScene = new QGraphicsScene(pParent);
    m_sComList = PortList;
    m_sTTUComList = TTUPortList;
    this->setScene(m_ObjScene);

    this->setStyleSheet("padding: 0px; border: 0px;background:rgb(	238, 216 ,174);");
    //隐藏横向滚动条
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    add_item();
}
coorView::~coorView()
{
    delete m_ObjScene;
    m_ObjScene = nullptr;
    qDeleteAll(m_ListItems);
}

void coorView::fit_size(QSize parentSize)
{
    //设置场景与视图大小为当前窗体大小
    qDebug() << this->size();
//    this->setFixedSize(parentSize);
//    m_ObjScene->setSceneRect(0, 0, parentSize.width(), parentSize.height());
}



void coorView::add_item()
{
    clear_item();
    int row = 0, column = 0;        //添加的图形项所占得行列数，用以动态设定Scene的大小
    int serial = 0;                 //每个图形项的数量
    int x = 10, y = 0;               //每个图形项在View中的坐标
    int itemSum = m_nItems;
    bool isAddColumn = true;        //该数值用以统计列数，当第一行满之后便置为false
    unsigned int bit = 0x1;
    while(itemSum--)
    {
        //添加图形
        coorItem* item = new coorItem();
        item->serial = serial++;
        item->setPos(x, y);
        m_ObjScene->addItem(item);
        m_ListItems.append(item);
        if(isAddColumn)
        {
            column++;
        }
        x += 80;
        //初始化状态
        if((m_unBitmap >> (m_nItems - itemSum -1)) & bit)
        {
            item->set_state(STATE_CLOSE);
        }
        else
        {
            item->set_state(STATE_IGNORE);
        }

        if(x + 80 > this->width())     //当行满之后，换行，并且column不再增加,定下来列数
        {
            x = 10;
            y += 140;
            isAddColumn = false;
        }
    }
    //添加下拉框，因为涉及到层叠顺序，所以将下拉框单独添加
    int i = 0;
    foreach (coorItem* item, m_ListItems) {
        QComboBox* combox = new QComboBox();
        QComboBox* ttucombox = new QComboBox();
        combox->setGeometry(item->x(), item->y()+66, 66, 27);
        combox->setMaxVisibleItems(8);
        ttucombox->setGeometry(item->x(), item->y() + 95, 66, 27);
        ttucombox->setMaxVisibleItems(8);
        m_ObjScene->addWidget(combox);
        m_ObjScene->addWidget(ttucombox);
        m_ListComBox.append(combox);
        m_TTU_ListComBox.append(ttucombox);
        //应生产需求,不再只加载可用COM口,COM口是否可用由使用者判断
        for (int i = 1; i < 100; i++)
        {
            combox->addItem("COM" + QString::number(i));
            ttucombox->addItem("COM" + QString::number(i));
        }
        //m_ListItems[i]->SerialPort = m_sComList.at(i);
        combox->setCurrentText(m_sComList.at(i));
        ttucombox->setCurrentText(m_sTTUComList.at(i));
        i++;
        //connect(combox, SIGNAL(currentIndexChanged(QString)), this, SLOT(slot_current_com_changed(QString)));
        //connect(ttucombox, SIGNAL(currentIndexChanged(QString)), this, SLOT(slot_current_ttu_com_changed(QString)));
    }

    if(column)          //计算出行数
    {
        row = ceil(m_ListItems.count() / (double)column);
    }
    //绑定信号槽
    foreach (coorItem* item, m_ListItems) {
        connect(item, &coorItem::sig_open_cco, this, &coorView::slot_open_cco);
        connect(item, &coorItem::sig_close_cco, this, &coorView::slot_close_cco);
    }
    m_ObjScene->setSceneRect(0, 0, column * 120, row * 120);     //根据行列数设定Scene大小

}

void coorView::set_item_state(const int index, const COOR_STATE state)
{
    m_ListItems.at(index)->set_state(state);
}

/*void coorView::wheelEvent(QWheelEvent *event)
{
    this->scale(1/m_scale, 1/m_scale);
    if(event->delta() > 0){
        if(m_scale < 10){
            m_scale += 0.2;
        }else{
            m_scale = 10;
        }
    }else{
        if(m_scale > 0.2){
            m_scale -= 0.2;
        }else{
            m_scale = 0.2;
        }
    }
    this->scale(m_scale, m_scale);
    qDebug()<<m_scale;
}*/

void coorView::clear_item()
{
    foreach (coorItem* item, m_ListItems) {
        disconnect(item, &coorItem::sig_open_cco, this, &coorView::slot_open_cco);
        disconnect(item, &coorItem::sig_close_cco, this, &coorView::slot_close_cco);
    }
    foreach (QComboBox* combox, m_ListComBox) {
        disconnect(combox, SIGNAL(currentIndexChanged(QString)), this, SLOT(slot_current_com_changed(QString)));
    }
    QList<coorItem*> empty_itemList;
    m_ListItems.swap(empty_itemList);
    QList<QComboBox*> empty_itemCombox;
    m_ListComBox.swap(empty_itemCombox);
    m_ObjScene->clear();
}

void coorView::resizeEvent(QResizeEvent *event)
{
    move_item();
}

void coorView::move_item()
{
    //当窗口大小改变时需要重新对item布局进行排列
    int row = 0, column = 0;
    int x = 10, y = 0;
    bool isAddColumn = true;
    foreach (coorItem* item, m_ListItems) {
        item->setPos(x, y);
        if(isAddColumn)
        {
            column++;
        }
        x += 80;
        if(x + 80 > this->width())
        {
            x = 10;
            y += 140;
            isAddColumn = false;
        }
    }
    for (int i = 0; i < m_ListItems.count(); i++)
    {
        m_ListComBox.at(i)->setGeometry(m_ListItems.at(i)->x(), m_ListItems.at(i)->y()+67, 66, 27);
        m_ListComBox.at(i)->setCurrentText(m_sComList.at(i));
        m_TTU_ListComBox.at(i)->setGeometry(m_ListItems.at(i)->x(), m_ListItems.at(i)->y() + 95, 66, 27);
        m_TTU_ListComBox.at(i)->setCurrentText(m_sTTUComList.at(i));
    }
    if(column)
    {
        row = ceil(m_ListItems.count() / (double)column);
    }
    m_ObjScene->setSceneRect(0, 0, column * 120, row * 120 + 120);
}

void coorView::slot_open_cco(int serial)
{
    emit sig_open_cco(serial);
}

void coorView::slot_close_cco(int serial)
{
    emit sig_close_cco(serial);
}

QStringList coorView::get_com_list()
{
    QStringList list;
    for(QComboBox* item : m_ListComBox)
    {
        list.append(item->currentText());
    }
    return list;
}
unsigned int coorView::get_com_state()
{
    unsigned int unRet = 0;
    int i = 0;
    for(coorItem* item : m_ListItems)
    {
        unsigned int unSet;
        if(item->icoColor == STATE_IGNORE)
            unSet = 0;
        else
            unSet = 1;
        unRet |= unSet << i;
        i++;
    }
    return unRet;
}

void coorView::set_comlist_auto()
{
    m_sComList[0] = m_ListComBox[0]->currentText();
    QString sFirstCom = m_sComList[0].mid(3, m_sComList[0].length() - 3);
    int ComNum = sFirstCom.toInt();
    for(int i = 1; i < 32; i++)
    {
        m_sComList[i] = "COM" + QString::number(ComNum + i);
    }
    return;
}

QStringList coorView::get_ttu_com_list()
{
    QStringList list;
    for(QComboBox* item : m_TTU_ListComBox)
    {
        list.append(item->currentText());
    }
    return list;
}

void coorView::set_ttu_comlist_auto()
{
    m_sTTUComList[0] = m_TTU_ListComBox[0]->currentText();
    QString sFirstCom = m_sTTUComList[0].mid(3, m_sTTUComList[0].length() - 3);
    int ComNum = sFirstCom.toInt();
    for(int i = 1; i < 32; i++)
    {
        m_sTTUComList[i] = "COM" + QString::number(ComNum + i);
    }
    return;
}

void coorView::set_ttu_combox_visiable(bool isvisible)
{
    for(int i = 0; i < 32; i++)
    {
        m_TTU_ListComBox.at(i)->setVisible(isvisible);
    }
}
