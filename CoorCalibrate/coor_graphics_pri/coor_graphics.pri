INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD


PUBLIC_HEADERS         += $$PWD/myitem.h

HEADERS                += $$PUBLIC_HEADERS \ 
    $$PWD/coorItem.h \
    $$PWD/coorView.h

SOURCES                += \
    $$PWD/coorItem.cpp \
    $$PWD/coorView.cpp

RESOURCES              += $$PWD/graphics.qrc
