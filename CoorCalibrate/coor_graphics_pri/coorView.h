﻿#ifndef SCENE_GRAPHIC_H
#define SCENE_GRAPHIC_H
#include <QGraphicsView>
#include <QGraphicsScene>
#include "coorItem.h"
#include <QMainWindow>
#include <QComboBox>
#include <QStringList>

class coorView : public QGraphicsView
{
    Q_OBJECT
public:
    coorView(QStringList PortList, QStringList TTUPortList, QWidget *pParent = nullptr, const int &nItems = 32, const unsigned int &bitmap = 0xffffffff);
    ~coorView();
    //设置item数量
    void set_item_num(int nNum){ m_nItems = nNum;}
    int get_item_num(){ return m_nItems;}
    void set_bitmap(const unsigned int nBitMap){m_unBitmap = nBitMap;}
    unsigned int get_bitmap(){return m_unBitmap;}
    void set_item_state(const int index, const COOR_STATE state);
    unsigned int get_com_state();   //获取表位图

    QStringList get_com_list();     //获取485串口列表
    void set_comlist_auto();        //根据第一个表位自动设置其他表位485串口

    QStringList get_ttu_com_list(); //获取终端串口列表
    void set_ttu_comlist_auto();    //自动设置终端串口列表

    //移动图形项
    void move_item();
    void set_ttu_combox_visiable(bool isvisible);
signals:
    //该信息负责向外部转发Item传来的信号
    //当鼠标右键选择开启或关闭某个集中器时会触发该信号 参数：集中器序号
    void sig_open_cco(int serial);
    void sig_close_cco(int serial);


private:
    //增加图形项
    void add_item();
    //清空图形项
    void clear_item();

    //窗体大小改变事件，用以排列图形
    void resizeEvent(QResizeEvent *event);

private:
    int m_nItems;
    unsigned int m_unBitmap;//在线集中器位图,表示在线情况
    QList<coorItem*> m_ListItems;           //图形项列表
    QList<QComboBox*> m_ListComBox;         //下拉列表
    QList<QComboBox*> m_TTU_ListComBox;
    qreal m_scale = 1;
    //
    QSize m_InitialSize;
    QGraphicsScene *m_ObjScene;
    //
    QStringList m_sComList;             //集中器串口列表
    QStringList m_sTTUComList;          //终端串口列表
private slots:
    void fit_size(QSize parentSize);
    //void wheelEvent(QWheelEvent *event);
    //开启某个集中器
    void slot_open_cco(int serial);
    //关闭某个集中器
    void slot_close_cco(int serial);
signals:
};

#endif // SCENE_GRAPHIC_H
