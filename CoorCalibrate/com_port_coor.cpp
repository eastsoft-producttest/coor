﻿#include "com_port_coor.h"
#include "packet_chain/packetchain.h"
#include <QTime>
#include <QThread>
//#include "log_utility/logModule.h"


com_port_coor::com_port_coor(const QString& sPortName, int serial): m_nSerial(serial)
{
    m_pSharer = SigletonSharer::getInstance();
    //m_pSerialPort = new QextSerialPort();
    this->setPortName(sPortName);
    //集中器波特率为2400,终端波特率为9600
    if(m_pSharer->m_MeterType == 0)
        this->setBaudRate(BAUD2400);
    else
        this->setBaudRate(BAUD9600);
    this->setDataBits(DATA_8);
    this->setFlowControl(FLOW_OFF);//无流控制
    this->setParity(PAR_EVEN);	//偶校验
    this->setStopBits(STOP_1);

    //connect(this, &QextSerialPort::readyRead, this, &com_port_coor::slot_get_cmd);

    m_pLogger = &Logger::instance();
}
com_port_coor::~com_port_coor()
{
    if(this->isOpen())
        this->close();
}

int com_port_coor::send_coor_command(const COOR_COMM_TYPE type, const QString &sAddr)
{
    m_type = type;
    if(TYPE_NULL == m_type)
    {
        return -1;
    }
    if(!this->isOpen())
    {
        QLOG_WARN() << "设备" << m_nSerial << "未打开";
        return -2;
    }
    QByteArray baCmd;
    if(type == TYPE_INITIAL)
    {
        baCmd = get_initial_cmd(sAddr);
    }
    else if(type == TYPE_GET_VOLT)
    {
        baCmd = get_instant_volt_cmd(sAddr);
    }
    else if(type == TYPE_GET_CURR)
    {
        baCmd = get_instant_curr_cmd(sAddr);
    }
    else if(type == TYPE_GET_POSITIVE_POWER)
    {
        baCmd = get_read_meter_cmd(sAddr);
    }
    else if(type == TYPE_GET_BARCODE)   //获取条码
    {
        baCmd = get_barcode_cmd();
    }

    else
    {
        return -3;
    }
    this->write(baCmd);
    return 0;
}
int com_port_coor::send_coor_command(const COOR_COMM_TYPE type, const int nPhase, const float fDiff,const QString &sAddr)
{
    m_type = type;
    if(TYPE_NULL == m_type)
    {
        return -1;
    }
    if(!this->isOpen())
    {
        QLOG_WARN() << "设备" << m_nSerial << "未打开";
        return -2;
    }
    QByteArray baCmd;
    if(type == TYPE_SET_VOLT_CURR_DIFF)
    {
        baCmd = set_volt_and_curr_cmd(sAddr, nPhase, fDiff);
    }
    else if(type == TYPE_SET_POWER_RATIO)
    {
        baCmd = set_power_ratio_differ_cmd(sAddr, nPhase, fDiff);
    }
    else if(type == TYPE_SET_CALI_PHASE_DIFF)
    {
        baCmd = set_cali_phase_diff_cmd(sAddr, nPhase, fDiff);
    }
    else if(type == TYPE_INIT_CALI_PHASE_DIFF)
    {
        baCmd = set_init_cali_phase_diff_cmd(sAddr, nPhase);
    }
    else if(type == TYPE_INIT_CALI_POWER_RATIO)
    {
        baCmd = set_init_power_ratio_differ_cmd(sAddr, nPhase);
    }
    else
    {
        return -3;
    }
    this->write(baCmd);
    return 0;
}
void com_port_coor::slot_wait(int serial, const QByteArray &bPackSend, const int nCheckPos, const unsigned char flag, const int nWaitTime_ms, const int nSleepTime_ms)
{
    if(serial != m_nSerial && serial != 0)//0时为全部启动
        return;
    m_nResult = -3;
    T_RCV_PACK tPackData;
    m_nResult = send_wait(bPackSend, nCheckPos, flag, m_RightPacket, nWaitTime_ms, nSleepTime_ms);
}
//命令类型
int com_port_coor::send_wait(const COOR_COMM_TYPE type, T_RCV_PACK &tPackData, const int nWaitTime_ms, const QString &sAddr)
{
    send_coor_command(type, sAddr);
    QTime qtStart = QTime::currentTime();
    m_SerialBuff.clear();
    while(qtStart.msecsTo(QTime::currentTime()) <= nWaitTime_ms)
    {
        QThread::msleep(100);
        if(this->bytesAvailable() > 0 || m_SerialBuff.length() > 10)
        {
            PacketChain pc;
            qDebug() << "begin read";
            m_SerialBuff += this->readAll();
            qDebug() << m_SerialBuff;
            int nEndPos = 0;
            QByteArray bPackRcv;
            int nLen = pc.GB645_check((unsigned char*)m_SerialBuff.data(), m_SerialBuff.length(), &nEndPos);
            if(nLen == -1)
            {
                QThread::msleep(10);
                continue;
            }
            else
            {
                bPackRcv = m_SerialBuff.mid(nEndPos - nLen, nLen);
                tPackData = decode_rcv_pack(bPackRcv);
                //此处有bug,tPackData除flag外的成员有未初始化的风险
                //qDebug() << tPackData.data_type;
                //qDebug() << tPackData.dbPositivePower;
                //qDebug() << tPackData.flag;
                if(tPackData.flag == 0xd4)//确认信息
                {
                    //校准指令收到确认
                    if(TYPE_SET_VOLT_CURR_DIFF == type)
                    {
                        QLOG_TRACE() << "校准电压电流成功";
                        return TYPE_SET_VOLT_CURR_DIFF;
                    }
                    else if(TYPE_SET_POWER_RATIO == type)
                    {
                        QLOG_TRACE() << "校准功率成功";
                        return TYPE_SET_POWER_RATIO;
                    }
                    else if(TYPE_INITIAL == type)
                    {
                        QLOG_TRACE() << "初始化成功";
                        return TYPE_INITIAL;
                    }
                    else
                    {
                        m_SerialBuff.remove(0, nEndPos);
                        QThread::msleep(10);
                        continue;
                    }
                }
                else if(tPackData.flag == 0x91) //数据
                {
                    //解析并返回结果
                    if(tPackData.data_type == 1 && type == TYPE_GET_POSITIVE_POWER)
                    {
                        //QLOG_WARN() << "获取正向有功成功" << tPackData.dbPositivePower;
                        return TYPE_GET_POSITIVE_POWER;
                    }
                    else if(tPackData.data_type == 2 && type == TYPE_GET_VOLT)
                    {
                        //QLOG_WARN() << tPackData.tDataVoltBlock.fUa << tPackData.tDataVoltBlock.fUb << tPackData.tDataVoltBlock.fUc;
                        return TYPE_GET_VOLT;
                    }
                    else if(tPackData.data_type == 3 && type == TYPE_GET_CURR)
                    {
                        //QLOG_WARN() << tPackData.tDataCurrBlock.fIa << tPackData.tDataCurrBlock.fIb << tPackData.tDataCurrBlock.fIc;
                        return TYPE_GET_CURR;
                    }
                    else if(tPackData.data_type == 4 && type == TYPE_GET_INSTANT_VAL_OF_BXZD)
                    {
                        //QLOG_WARN() << tPackData.tDataInstant.fIa << tPackData.tDataInstant.fIb << tPackData.tDataInstant.fIc;
                        return TYPE_GET_INSTANT_VAL_OF_BXZD;
                    }
                    else if(tPackData.data_type == 5 && type == TYPE_GET_BARCODE)
                    {
                        return TYPE_GET_BARCODE;
                    }
                    else
                    {
                        m_SerialBuff.remove(0, nEndPos);
                        QThread::msleep(10);
                        continue;
                    }
                }
                else
                {
                    m_SerialBuff.remove(0, nEndPos);
                    QThread::msleep(10);
                    continue;
                }
            }
        }
    }

    return -1;//超时失败
}
//设置误差类型
int com_port_coor::send_wait(const COOR_COMM_TYPE type, T_RCV_PACK &tPackData, const int nPhase, const float fDiff, const int nWaitTime_ms,const QString &sAddr)
{
    send_coor_command(type, nPhase, fDiff, sJCAddr);
    //设置类不需要等待确认报文
    return 1;
#if 0
    QTime qtStart = QTime::currentTime();
    while(qtStart.msecsTo(QTime::currentTime()) <= nWaitTime_ms)
    {
        if(this->bytesAvailable() > 0)
        {
            PacketChain pc;
            m_SerialBuff += this->readAll();
            qDebug() << m_SerialBuff;
            int nEndPos = 0;
            QByteArray bPackRcv;
            int nLen = pc.GB645_check((unsigned char*)m_SerialBuff.data(), m_SerialBuff.length(), &nEndPos);
            if(nLen == -1)
            {
                QThread::msleep(10);
                return -1;
            }
            else
            {
                bPackRcv = m_SerialBuff.mid(nEndPos - nLen, nLen);
                tPackData = decode_rcv_pack(bPackRcv);
                //qDebug() << tPackData.data_type;
                //qDebug() << tPackData.dbPositivePower;
                //qDebug() << tPackData.flag;
                if(tPackData.flag == 0xd4)//确认
                {
                    //校准指令收到确认
                    if(TYPE_SET_VOLT_CURR_DIFF == type)
                    {
                        QLOG_TRACE() << "校准电压/电流成功";
                        return TYPE_SET_VOLT_CURR_DIFF;
                    }
                    else if(TYPE_SET_POWER_RATIO == type)
                    {
                        QLOG_TRACE() << "校准功率成功";
                        return TYPE_SET_POWER_RATIO;
                    }
                    else if(TYPE_INIT_CALI_PHASE_DIFF == type)
                    {
                        QLOG_TRACE() << "启动功率比差校准成功.";
                        return TYPE_INIT_CALI_PHASE_DIFF;
                    }
                    else if(TYPE_INIT_CALI_POWER_RATIO == type)
                    {
                        QLOG_TRACE() << "启动功率相差校准成功.";
                        return TYPE_INIT_CALI_POWER_RATIO;
                    }
                    else
                    {
                        m_SerialBuff.remove(0, nEndPos);
                        QThread::msleep(10);
                        continue;
                    }
                }
                else
                {
                    m_SerialBuff.remove(0, nEndPos);
                    QThread::msleep(10);
                    continue;
                }
            }
        }
    }

    return -1;//超时失败
#endif
}

int com_port_coor::send_wait(const QByteArray &bPackSend, const int nCheckPos, const uint8_t flag, QByteArray &bPackRcv, const int nWaitTime_ms, const int nSleepTime_ms)
{
    //Q_ASSERT(this->isOpen());
    PacketChain pc;
    this->write(bPackSend);
    this->flush();
    //
    QTime qtStart = QTime::currentTime();
    while(qtStart.msecsTo(QTime::currentTime()) <= nWaitTime_ms)
    {
        QThread::msleep(100);
        if(this->bytesAvailable() > 0)
        {
            m_SerialBuff += this->readAll();
            qDebug() << m_SerialBuff;
            int nEndPos = 0;
            int nLen = pc.GB645_check((unsigned char*)m_SerialBuff.data(), m_SerialBuff.length(), &nEndPos);
            if(nLen == -1)
            {
                QThread::msleep(10);
                continue;
            }
            else
            {
                bPackRcv = m_SerialBuff.mid(nEndPos - nLen, nLen);
                if((uint8_t)bPackRcv.at(nCheckPos) == flag)
                {
                    return 0;
                }
                else
                {
                    m_SerialBuff.remove(0, nEndPos);
                    QThread::msleep(10);
                    continue;
                }
            }
        }
    }
    QLOG_WARN() << "获取数据超时";
    return -1;
}

int com_port_coor::send_wait(const T_COOR_SAVE_ERROR tError, const int nWaitTime_ms)
{
    PacketChain pc;
    QByteArray send = save_cali_error_cmd(tError);
    this->write(send);
    this->flush();
    QTime qtStart = QTime::currentTime();
    while(qtStart.msecsTo(QTime::currentTime()) <= nWaitTime_ms)
    {
        QThread::msleep(100);
        if(this->bytesAvailable() > 0)
        {
            m_SerialBuff += this->readAll();
            int nEndPos = 0;
            int nLen = pc.GB645_check((unsigned char*)m_SerialBuff.data(), m_SerialBuff.length(), &nEndPos);
            if(nLen == -1)
            {
                QThread::msleep(10);
                continue;
            }
            else
            {
                //只要有回复就可以认为写入成功
                return 0;
            }
        }
    }
    QLOG_WARN() << "获取数据超时";
    return -1;
}

void com_port_coor::slot_get_cmd()
{
    PacketChain pc;
    m_SerialBuff += this->readAll();
    qDebug() << m_SerialBuff;
    int nEndPos = 0;
    QByteArray bPackRcv;
    int nLen = pc.GB645_check((unsigned char*)m_SerialBuff.data(), m_SerialBuff.length(), &nEndPos);
    if(nLen == -1)
    {
        QThread::msleep(10);
        return;
    }
    else
    {
        bPackRcv = m_SerialBuff.mid(nEndPos - nLen, nLen);
        T_RCV_PACK tPackData = decode_rcv_pack(bPackRcv);
        //qDebug() << tPackData.data_type;
        //qDebug() << tPackData.dbPositivePower;
        //qDebug() << tPackData.flag;
        if(tPackData.flag == 0x94)//确认
        {
            //校准指令收到确认
            if(TYPE_SET_VOLT_CURR_DIFF == m_type)
            {
                QLOG_TRACE() << "校准电压成功";
            }
            if(TYPE_SET_POWER_RATIO == m_type)
            {
                QLOG_TRACE() << "校准功率成功";
            }
        }
        else if(tPackData.flag == 0x91)
        {
            //解析并返回结果
            if(tPackData.data_type == 1)
            {
                QLOG_WARN() << tPackData.dbPositivePower;
            }
            else if(tPackData.data_type == 2)
            {
                QLOG_WARN() << tPackData.tDataVoltBlock.fUa << tPackData.tDataVoltBlock.fUb << tPackData.tDataVoltBlock.fUc;
            }
            else if(tPackData.data_type == 3)
            {
                QLOG_WARN() << tPackData.tDataCurrBlock.fIa << tPackData.tDataCurrBlock.fIb << tPackData.tDataCurrBlock.fIc;
            }
            else if(tPackData.data_type == 4)
            {
                QLOG_WARN() << tPackData.tDataInstant.fIa << tPackData.tDataInstant.fIb << tPackData.tDataInstant.fIc;
            }
            else
            {
                return;
            }
        }
        else
        {
            m_SerialBuff.remove(0, nEndPos);
            QThread::msleep(10);
            return;
        }
    }
}
