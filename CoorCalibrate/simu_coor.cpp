﻿#include "simu_coor.h"
#include <qDebug>
#include <QThread>
//#include "log_utility/logModule.h"

QMutex m_Mutex;
simu_coor::simu_coor(TYPT_SIMU_COOR t_simu)
    : m_tSimu(t_simu)
{
    //获取标准表型号
    m_ini = new ini_profile();
    ba_MeterType = m_ini->get_standardMeter().toLatin1();
    m_MeterType = ba_MeterType.data();
}
//char* sMETERTYPE = "HS5300B";

simu_coor::~simu_coor()
{
    m_Lib_hscom.unload();
}

simu_coor* simu_coor::m_pInstance = new simu_coor(SIMU_5320);

simu_coor* simu_coor::get_instance()
{

    return m_pInstance;
}


int simu_coor::simu_coor_load(QString sDllName)
{
    if(m_Lib_hscom.isLoaded())
    {
        m_Lib_hscom.unload();
    }
    m_Lib_hscom.setFileName(sDllName);
    if(!m_Lib_hscom.load())
    {
        return -1;
    }

    m_fpPower_Off        = (P_Power_Off)m_Lib_hscom.resolve("Power_Off");
    m_fpError_Clear       = (P_Power_Off)m_Lib_hscom.resolve("Error_Clear");
    m_fpError_Read       = (P_Error_Read)m_Lib_hscom.resolve("Error_Read");
    m_fpStdMeter_Read    = (P_StdMeter_Read)m_Lib_hscom.resolve("StdMeter_Read");
    m_fpPort_Close       = (P_Port_Close)m_Lib_hscom.resolve("Dll_Port_Close");
    m_fpAdjust_ui        = (P_Adjust_UI)m_Lib_hscom.resolve("Adjust_UI");
    m_fpPower_Pause      = (P_Power_Pause)m_Lib_hscom.resolve("Power_Pause");
    m_fpSet_PauseChannel = (P_Set_Pulse_Channel)m_Lib_hscom.resolve("Set_Pulse_Channel");
    m_fpError_Start      = (P_Error_Start)m_Lib_hscom.resolve("Error_Start");
    m_fpAdjust_Cust      = (P_Adjust_CUST)m_Lib_hscom.resolve("Adjust_CUST");
    m_fpOpen_485         = (P_Open485CommPort)m_Lib_hscom.resolve("Open485CommPort");
    m_fpClose_485        = (P_Close485CommPort)m_Lib_hscom.resolve("Close485CommPort");
    m_fpSendData_485     = (P_Send485Data)m_Lib_hscom.resolve("Send485Data");
    m_fpRecvData_485     = (P_Recv485Data)m_Lib_hscom.resolve("Recv485Data");
    m_fpSetOn_485        = (P_SetRS485On)m_Lib_hscom.resolve("SetRS485On");
    m_fpSetRefClock      = (PSetRefClock)m_Lib_hscom.resolve("SetRefClock");
    m_fpClockErrorStart  = (PClock_Error_Start)m_Lib_hscom.resolve("Clock_Error_Start");
    m_fpClockErrorRead   = (PClock_Error_Read)m_Lib_hscom.resolve("Clock_Error_Read");
    m_fpConstantStart    = (P_ConstStart)m_Lib_hscom.resolve("ConstTest_Start");
    return 1;
}

int simu_coor::simu_coor_unload()
{
    if(m_Lib_hscom.isLoaded())
        m_Lib_hscom.unload();
    else
    {
        return 0;
    }
    m_fpPower_Off = nullptr;
    m_fpError_Clear = nullptr;
    m_fpError_Read = nullptr;
    m_fpStdMeter_Read = nullptr;
    m_fpPort_Close = nullptr;
    m_fpAdjust_ui = nullptr;
    m_fpPower_Pause = nullptr;
    m_fpSet_PauseChannel = nullptr;
    m_fpError_Start = nullptr;
    m_fpAdjust_Cust = nullptr;
    m_fpOpen_485 = nullptr;
    m_fpClose_485 = nullptr;
    m_fpSendData_485 = nullptr;
    m_fpRecvData_485 = nullptr;
    m_fpSetOn_485 = nullptr;
    return 1;
}

int simu_coor::dll_power_off(int nDevPort)
{ 
    if(!m_fpPower_Off)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpPower_Off(nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_error_clear(int nDevPort)
{
    if(!m_fpError_Clear)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpError_Clear(nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_error_read(char ** psError,int nMeterNo,int nDevPort)
{
    if(!m_fpError_Read)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpError_Read(psError, nMeterNo, nDevPort);
        m_Mutex.unlock();
        return ret;

    }

}

int simu_coor::dll_std_meter_read(char* &psData, char* sModel, int nDevPort)
{
    if(!m_fpStdMeter_Read)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpStdMeter_Read(&psData, m_MeterType, nDevPort);
        m_Mutex.unlock();
        return 1;

    }
}

int simu_coor::dll_port_close()
{
    if(!m_fpPort_Close)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        m_fpPort_Close();
        m_Mutex.unlock();
        return 1;
    }
}

int simu_coor::dll_adjust_ui(int nPhase,double dbRatedVolt,double dbRatedCurr,
                   double dbRatedFreq,int nPhaseSeq,int nCurrDir,
                   double dbVoltPer,double dbCurrPer,char* sIABC,
                   char* sCosP,char* sModel,int nDevPort)
{
    if(!m_fpAdjust_ui)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << " ";
        int ret = m_fpAdjust_ui( nPhase, dbRatedVolt, dbRatedCurr,
                            dbRatedFreq, nPhaseSeq, nCurrDir,
                            dbVoltPer, dbCurrPer, sIABC,
                            sCosP, sModel, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_power_pause(int nDevPort)
{
    if(!m_fpPower_Pause)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpPower_Pause(nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_set_pause_channel(int nMeterNo,int nChannelFlag,int nDevPort)
{
    if(!m_fpSet_PauseChannel)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpSet_PauseChannel( nMeterNo, nChannelFlag, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_error_start(int nMeterNo,double dbConstant,int nPulse,int nDevPort)
{
    if(!m_fpError_Start)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpError_Start( nMeterNo, dbConstant, nPulse, nDevPort);
        QThread::msleep(50);//加100ms延时，防止读不到
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_adjust_cust(int nPhase,double dbRatedFreq,double dbVolt1,
                     double dbVolt2,double dbVolt3,double dbCurr1,
                     double dbCurr2,double dbCurr3,double dbUab,
                     double dbUac,double dbAng1,double dbAng2,
                     double dbAng3,char* sModel,int nDevPort)
{
    if(!m_fpAdjust_Cust)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpAdjust_Cust(nPhase, dbRatedFreq, dbVolt1,
                             dbVolt2, dbVolt3, dbCurr1,
                             dbCurr2, dbCurr3, dbUab,
                             dbUac, dbAng1, dbAng2,
                             dbAng3, sModel, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_open_485_port(DWORD dwLngHdcComm,char * sBaudRate,unsigned char ucMeterPort)
{
    if(!m_fpOpen_485)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpOpen_485(dwLngHdcComm, sBaudRate, ucMeterPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_close_485_port(DWORD dwLngHdcCom)
{
    if(!m_fpClose_485)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpClose_485(dwLngHdcCom);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_send_485_data(DWORD dwLngHdcCom, char* sMeterData)
{
    if(!m_fpSendData_485)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpSendData_485(dwLngHdcCom, sMeterData);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_recv_485_data(DWORD dwLngHdcCom,char * sMeterData,int nDataLen)
{
    if(!m_fpRecvData_485)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpRecvData_485(dwLngHdcCom, sMeterData, nDataLen);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_set_485_on(int nMeterNo,int nIsOnLine,int nDevPort)
{
    if(!m_fpSetOn_485)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpSetOn_485(nMeterNo, nIsOnLine, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}

int simu_coor::dll_set_ref_clock(DLL_Byte ucSetFlag, DLL_Byte ucDevPort)
{
    if(!m_fpSetRefClock)
        {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpSetRefClock(ucSetFlag, ucDevPort);
        m_Mutex.unlock();
        return ret;
    }
}
int simu_coor::dll_clock_error_start(int nMeterNo, double dbTheoryFreq, int nTestTime, int nDevPort)
{
    if(!m_fpClockErrorStart)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";
        int ret = m_fpClockErrorStart(nMeterNo, dbTheoryFreq, nTestTime, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}
int simu_coor::dll_clock_error_read(char* &sMError, double dbTheoryFreq, int nErrorType, int nMeterNo, int nDevPort)
{
    if(!m_fpClockErrorRead)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";// 调试中发现不加qdebug会崩溃，需排查根因
        int ret = m_fpClockErrorRead(&sMError, dbTheoryFreq, nErrorType, nMeterNo, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}
int simu_coor::app_adjust_ui(int nPhase, const double fVolt, const double fCurrent, const double fIPercent, char* sIabc, char* sCosp, const int nDevPort, const double fVoltLimit, const double fCurrentLimit)
{
    QLOG_TRACE() << "初始化--升源--设置电压:" << fVolt <<"，电流: " << fCurrent << "，" << fIPercent << "%, 相位: " << sIabc << "，相角: " << sCosp;
    int iRet = this->dll_adjust_ui(nPhase, fVolt, fCurrent, 50,
                                                        0, 0, 100, fIPercent
                                                        , sIabc, sCosp,m_MeterType, nDevPort);
    if(iRet <= 0)
    {
        QLOG_TRACE() << "设置标准源电压电流失败";
        return FUNCTION_NULL;
    }

    QThread::msleep(2000);
    //取电压并校准,六次以内达到稳定值，否则失败
    int nTimes = 6;
    while(nTimes --)
    {
        QLOG_TRACE() << "取电压电流值";
        char *sData;
        sData = new char[256];
        memset(sData, 0, 256);
        qDebug() << "";
        int ret = this->dll_std_meter_read(sData, m_MeterType, nDevPort);
        if(ret < 0)
        {
            QLOG_TRACE() << "电源取值失败";
            return -9;
        }
        QString str(sData);
        delete []sData; sData = nullptr;
        QStringList sListVal = str.split(",");
        if(sListVal.size() < 6)
        {
            continue;
        }
        float Ua, Ub, Uc, Ia, Ib, Ic;
        Ua = sListVal[0].toFloat();
        Ub = sListVal[1].toFloat();
        Uc = sListVal[2].toFloat();
        Ia = sListVal[3].toFloat();
        Ib = sListVal[4].toFloat();
        Ic = sListVal[5].toFloat();
        //如果没有稳定
        if(strcmp(sIabc, "H") == 0)
        {
            if((fabs(Ua - fVolt) > fVoltLimit) || (fabs(Ub - fVolt) > fVoltLimit) || (fabs(Uc - fVolt) > fVoltLimit)
               ||(fabs(Ia - fCurrent * fIPercent / 100) > fCurrentLimit) || (fabs(Ib - fCurrent * fIPercent / 100) > fCurrentLimit) || (fabs(Ic - fCurrent * fIPercent / 100) > fCurrentLimit))
            {
                QLOG_TRACE() << "电压尚未稳定";
                QThread::msleep(5000);
                continue;//待定
            }
            else
            {
                QLOG_TRACE() << "电源已稳定";
                return 1;
            }
        }
        else if(strcmp(sIabc, "A") == 0)
        {
            if((fabs(Ua - fVolt) > fVoltLimit) ||(fabs(Ia - fCurrent * fIPercent / 100) > fCurrentLimit))
            {
                QLOG_TRACE() << "电压尚未稳定";
                QThread::msleep(5000);
                continue;//待定
            }
            else
            {
                QLOG_TRACE() << "电源已稳定";
                return 1;
            }
        }
        else if(strcmp(sIabc, "B") == 0)
        {
            if((fabs(Ub - fVolt) > fVoltLimit) ||(fabs(Ib - fCurrent * fIPercent / 100) > fCurrentLimit))
            {
                QLOG_TRACE() << "电压尚未稳定";
                QThread::msleep(5000);
                continue;//待定
            }
            else
            {
                QLOG_TRACE() << "电源已稳定";
                return 1;
            }
        }
        else if(strcmp(sIabc, "C") == 0)
        {
            if((fabs(Uc - fVolt) > fVoltLimit) ||(fabs(Ic - fCurrent * fIPercent / 100) > fCurrentLimit))
            {
                QLOG_TRACE() << "电压尚未稳定";
                QThread::msleep(5000);
                continue;//待定
            }
            else
            {
                QLOG_TRACE() << "电源已稳定";
                return 1;
            }
        }
        else
        {
            return -2;//相位输入错误
        }
        QThread::msleep(2000);
    }


    return -1;//未稳定
}
/*******************************************
 * 为达到电源控制同步目的
 * 需要所有线程同步到同一上电节拍
 *
 * */
int simu_coor::slot_power_on(int nPhase, const double fVolt, const double fCurrent, const double fIPercent, char* sIabc, char* sCosp, const int nDevPort, const double fVoltLimit, const double fCurrentLimit, int nSerial)
{
    QThread::msleep(2000);//加延时为了保证上一步该退出的线程全部退出
    m_nPowerRet = -1;
    mutex2.lock();
    thread_count++;

    QLOG_TRACE() << "thread_count: " << thread_count << " serial:" << nSerial;
    QLOG_TRACE() << "m_nRunningNum: " << m_nRunningNum << " serial" << nSerial;
    if(thread_count == m_nRunningNum)
    {
        mutex2.unlock();
        in_count++;

        QLOG_TRACE() << "in_count.." <<in_count;
        QThread::msleep(50);
        mutex1.lock();
        QLOG_TRACE() << "等待完成,serial:" << nSerial;
        //QThread::msleep(100);//替换以下
        m_nPowerRet = this->app_adjust_ui(nPhase, fVolt, fCurrent, fIPercent, sIabc, sCosp, nDevPort,fVoltLimit, fCurrentLimit);
        //电源上电，误差清除
        this->dll_error_clear(nDevPort);
        //QLOG_TRACE() << "waiting over";
        condition1.wakeAll();
        //QLOG_TRACE() << "waiting notify";
        mutex1.unlock();
        QLOG_TRACE() << "全部唤醒,serial:" << nSerial;
        //QLOG_TRACE() << "waiting unlock";
        thread_count = 0;
    }
    else
    {
        mutex2.unlock();
        mutex1.lock();
        QLOG_TRACE() << "开始等待唤醒,serial:" << nSerial;
        condition1.wait(&mutex1);
        mutex1.unlock();
    }

    return m_nPowerRet;
}

int simu_coor::slot_power_off(int nDevPort, int nSerial)
{
    QThread::msleep(2000);//加延时为了保证上一步该退出的线程全部退出
    m_nPowerRet = -2;
    mutex3.lock();
    //QThread::msleep(100);
    thread_count++;

    QLOG_TRACE() << "thread_count: " << thread_count << "nSerial:" << nSerial;
    QLOG_TRACE() << "m_nRunningNum: " << m_nRunningNum << "nSerial" << nSerial;
    if(thread_count == m_nRunningNum || m_nRunningNum == 0)
    {
        mutex3.unlock();
        QLOG_TRACE() << "waiting..";
        QThread::msleep(50);
        mutex1.lock();
        QLOG_TRACE() << "等待完成,serial:" << nSerial;
        //QThread::msleep(100);//替换以下
        m_nPowerRet = this->dll_power_off(nDevPort);
        //QLOG_TRACE() << "waiting over";
        condition1.wakeAll();
        //QLOG_TRACE() << "waiting notify";
        mutex1.unlock();
        QLOG_TRACE() << "全部唤醒,serial:" << nSerial;
        //QLOG_TRACE() << "waiting unlock";
        thread_count = 0;
    }
    else
    {
        mutex3.unlock();
        mutex1.lock();
        QLOG_TRACE() << "开始等待唤醒,serial:" << nSerial;
        condition1.wait(&mutex1);
        QLOG_TRACE() << "waiting over" << nDevPort;
        mutex1.unlock();
    }

    return m_nPowerRet;
}

int simu_coor::dll_constant_start(int nMeterNo,double dbConstant, int nDevPort)
{
    if(!m_fpConstantStart)
    {
        return FUNCTION_NULL;
    }
    else
    {
        m_Mutex.lock();
        qDebug() << "";// 调试中发现不加qdebug会崩溃，需排查根因
        int ret = m_fpConstantStart(nMeterNo, dbConstant, nDevPort);
        m_Mutex.unlock();
        return ret;
    }
}
