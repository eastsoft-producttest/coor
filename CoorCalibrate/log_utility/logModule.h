#include <QCoreApplication>
#include <stdio.h>
#include <stdlib.h>
#include <QDateTime>
#include <QDir>
#include <QMutex>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <windows.h>


namespace logModule
{

#define LOG_DEBUG qDebug() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_INFO qInfo() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_WARN qWarning() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_CRIT qCritical() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "


//初始化log模块
void logInit(const QString &logPath, int logMaxCount);
//关闭log模块
void logClose(const QString &logPath );

    //初始化log模块
   // void logInit(const QString &logPath = QStringLiteral("Log"), int logMaxCount = 1024);
    //关闭log模块
   // void logClose(const QString &logPath = QStringLiteral("Log"));
    //连接输出信息函数
    void MessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
}

