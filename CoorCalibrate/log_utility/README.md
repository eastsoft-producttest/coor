1. 在需要使用到日志的类或CPP文件中引入头文件 `#include "logModule.h"`

2. 在main函数开头或者mainwindows的构造函数中对日志进行初始化`logModule::logInit();`（只需初始化一次，全项目通用）

3. 写日志

   ```c++
   LOG_DEBUG << "debug";
   LOG_INFO << "信息";
   LOG_WARN << "警告";
   LOG_CRIT << "错误";
   ```

   输出格式：  时间，日志类型，文件名，函数名，行号，内容

   ```
   13:32:57.759 Debug : ..\testLog\mainwindow.cpp on_pushButton_clicked 33  :  debug
   13:32:57.763 Info : ..\testLog\mainwindow.cpp on_pushButton_clicked 34  :  信息
   13:32:57.763 Warning : ..\testLog\mainwindow.cpp on_pushButton_clicked 35  :  警告
   13:32:57.763 Critical : ..\testLog\mainwindow.cpp on_pushButton_clicked 36  :  错误
   ```

4. 关闭日志模块

   `logModule::logClose();`

   写在主窗口析构函数中



注：日志内容的获取使用的`qInstallMessageHandler()`函数来截获`qDebug()`、`qInfo()`等调试函数的输出内容及信息。所以在使用`qDebug()`时同样也会将内容写入日志。但因release模式下会丢失文件信息和行号，故使用以下宏定义来代替`qDebug()`等。

```c++
#define LOG_DEBUG qDebug() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_INFO qInfo() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_WARN qWarning() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
#define LOG_CRIT qCritical() << __FILE__ << __FUNCTION__ << __LINE__ <<" : "
```

参考：

https://jaredtao.github.io/2019/04/30/Qt%E8%87%AA%E5%88%B6%E7%AE%80%E6%98%93%E5%A5%BD%E7%9C%8B%E7%9A%84%E6%97%A5%E5%BF%97%E7%B3%BB%E7%BB%9F/

