#include "logModule.h"

namespace logModule
{
static QString glogDir;      //log存放位置
static int gLogMaxCount;     //最大log数量

void logInit(const QString &logPath, int logMaxCount)
{
    //将qDebug(),qInfo()等输出与函数连接
    qInstallMessageHandler(MessageOutput);
    //确定log存放位置
    glogDir = QCoreApplication::applicationDirPath() + "/" + logPath;
    gLogMaxCount = logMaxCount;
    QDir dir(glogDir);
    if(!dir.exists())
    {
        dir.mkpath(dir.absolutePath());
    }
    //约束最大日志数量
    QStringList infoList = dir.entryList(QDir::Files, QDir::Name);
    while(infoList.size() > gLogMaxCount)
    {
        dir.remove(infoList.first());
        infoList.removeFirst();
    }
}

void logClose(const QString &logPath)
{

}

//参数(输出类型；输出信息(位置等)；输出内容)
void MessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    //表示在这个函数里context没有被用到，不要报warning
    Q_UNUSED(context);

    static QMutex mutex;                //互斥锁
    static QFile file;                  //日志文件
    static QTextStream textStream;      //文件流

    //日志类型
    QString logType = "";
    switch (type) {
    case QtDebugMsg:
        logType = "Debug";
        break;
    case QtInfoMsg:
        logType = "Info";
        break;
    case QtWarningMsg:
        logType = "Warning";
        break;
    case QtCriticalMsg:
        logType = "Critical";
        break;
    case QtFatalMsg:
        logType = "Fatal";
        break;
    default:
        break;
    }

    //以小时为单位存储日志
    QString fileNameDt = QDateTime::currentDateTime().toString("yyyy_MM_dd_HH");

    //具体某条日志显示的时间
    QString contentTm = QTime::currentTime().toString("hh:mm:ss.zzz");
    //日志格式及内容填充
    //QByteArray localMsg = msg.toLocal8Bit();
    //时间 所在文件 所在函数 所在行 类型 内容
    QString message = QString("%1 %2 : %3\r\n").arg(contentTm).arg(logType).arg(msg);
    //QString message = contentTm + " " + context.file + " " + context.function + " "
    //        + QString::number(context.line) + " " + logType + " " + ": " + msg + "\n";

    //用以检查当前是否过了一小时
    QString newfileName = QString("%1/%2.log").arg(glogDir).arg(fileNameDt);

    //开始写了
    mutex.lock();
    //如果当前文件名过期了
    if(file.fileName() != newfileName)
    {
        //关闭文件
        if(file.isOpen())
        {
            file.close();
        }
        //设置新文件名并新建文件
        file.setFileName(newfileName);
        file.open(QIODevice::WriteOnly | QIODevice::Append);
        textStream.setDevice(&file);
        textStream.setCodec("UTF-8");
    }
    //通过文件流写入日志
    textStream << message;
    textStream.flush();
    mutex.unlock();
//以下语句用来将日志信息输出到控制台
#ifdef Q_OS_WIN
    ::OutputDebugString(msg.toStdWString().data());
    ::OutputDebugString(L"\r\n");
#else
    fprintf(stderr, message.toStdString().data());
#endif
}
}

