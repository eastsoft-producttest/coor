﻿#include "onecoorcalicheckprocess.h"

#include "datadef.h"

#define NO_TEST_MODE 1 //单独调试误差校准时可屏蔽此宏定义
//此处不要放在类里
QMutex g_CaliCheckMutex;        //校/检表结果上传MES互斥
QMutex g_PakageMutex;           //包装线上传MES互斥
QMutex g_10PercentIb_Hx;
QMutex g_FailNum;               //校表失败数量修改互斥

OneCoorCaliCheckProcess::OneCoorCaliCheckProcess(QObject *parent)
{
    Q_UNUSED(parent);
}
OneCoorCaliCheckProcess::OneCoorCaliCheckProcess(const QString& sPortName, const QString &sTTUPortName, int nSerial, const bool bEnable, const QString PlatNo)
    :m_s485PortName(sPortName), m_sTTUPortName(sTTUPortName) , m_isEnable(bEnable), m_nSerial(nSerial), m_PlatNo(PlatNo)
{
    m_pSimu_coor =  simu_coor::get_instance();
    m_pCoorPort = new com_port_coor(m_s485PortName, nSerial);
    if(m_isEnable)
    {
        m_isOpen = m_pCoorPort->open(QIODevice::ReadWrite);
        if(!m_isOpen)
            QLOG_TRACE() << "端口" << m_nSerial << "打开失败";
    }
    m_pEspdPort = new com_port_espd(m_sTTUPortName, nSerial);
    m_pSharer = SigletonSharer::getInstance();
    //para
    ini_profile ini;
    m_Para = ini.get_coor_cali_para();

    m_fStdVolt = m_Para.m_nStdVolt.toDouble();
    m_fStdCurr = m_Para.m_nStdCurr.toDouble();
    m_fMaxCurr = m_Para.m_nImax.toDouble();
    m_dbConstant = m_Para.m_nConstant.toDouble();
    m_nPluse = m_Para.m_nPluse.toInt();
    m_nTheoryFreq = m_Para.m_nStdClockFreq.toInt();
    m_dbLimitQZ = m_Para.m_nLightErrLimit.toDouble();
    m_dbLimitHX = m_Para.m_nHexiangErrLimit.toDouble();
    m_dbLimitWG = m_Para.m_nReactiveErrLimit.toDouble();
    m_fCurrLimit_Imax = m_Para.m_nImaxErrLimit.toDouble();
    m_nClockTestDuration = m_Para.m_nClockTestDuration.toInt();
    //m_nDevPort = m_Para.m_nStdDevPort.remove("COM").toInt();
    m_nDevPort = ini.get_dev_com().remove("COM").toInt();


    //connect(this, &OneCoorCaliCheckProcess::sig_power_control, m_pSimu_coor, &simu_coor::slot_power_on);
    m_MesUSCore = new Pass_USCOREStationSoapProxy();
    //m_MesWeb = new MesNewWebServiceSoapProxy();

    m_pLogger = &Logger::instance();
    m_cali_csv = new csv_data("cali", "条码,Va,Vb,Vc,Ia,Ib,Ic,PR1.0A,PR1.0B,PR1.0C,PPH0.5L_A,PPH0.5L_B,PPH0.5L_C,CHP0.5L_0.1Ib_A,CHP0.5L_0.1Ib_B,CHP0.5L_0.1Ib_C,CHP1.0_Ib_H,chp0.5L_Ib_H,CHP1.0_0.1Ib_H");
    m_check_csv = new csv_data("check", "条码,Va,Vb,Vc,Ia,Ib,Ic, "
                                        "Va_Imax,Vb_Imax,Vc_Imax,Ia_Imax,Ib_Imax,Ic_Imax,"
                                        "CHP_1.0_Imax_A,CHP_1.0_Imax_B,CHP_1.0_Imax_C,"
                                        "CHP_0.8C_Imax_A,CHP_0.8C_Imax_B,CHP_0.8C_Imax_C,"
                                        "CHP_0.5L_Imax_A,CHP_0.5L_Imax_B,CHP_0.5L_Imax_C,"
                                        "CHP_1.0_Ib_A,CHP_1.0_Ib_B,CHP_1.0_Ib_C,"
                                        "CHP_0.8C_Ib_A,CHP_0.8C_Ib_B,CHP_0.8C_Ib_C,"
                                        "CHP_0.5L_Ib_A,CHP_0.5L_Ib_B,CHP_0.5L_Ib_C,"
                                        "CHP_1.0_0.1Ib_A,CHP_1.0_0.1Ib_B,CHP_1.0_0.1Ib_C,"
                                        "CHP_0.8C_0.1Ib_A,CHP_0.8C_0.1Ib_B,CHP_0.8C_0.1Ib_C,"
                                        "CHP_0.5L_0.1Ib_A,CHP_0.5L_0.1Ib_B,CHP_0.5L_0.1Ib_C,"
                                        "CHP_1.0_Imax_H,CHP_0.8C_Imax_H,CHP_0.5L_Imax_H,"
                                        "CHP_1.0_Ib_H,CHP_0.8C_Ib_H,CHP_0.5L_Ib_H,"
                                        "CHP_1.0_0.1Ib_H,CHP_0.8C_0.1Ib_H,CHP_0.5L_0.1Ib_H,"
                                        "CHP_1.0_0.05Ib_H,CHP_0.8C_0.05Ib_H,CHP_0.5L_0.05Ib_H,TimeCntErr");
}

OneCoorCaliCheckProcess::~OneCoorCaliCheckProcess()
{
    if(m_pCoorPort->isOpen())
        m_pCoorPort->close();
    delete m_pCoorPort;
    m_pCoorPort = nullptr;
    delete m_pCoorPort;
    m_pCoorPort = nullptr;

    delete m_MesUSCore;
    m_MesUSCore = nullptr;

    //delete m_MesWeb;
    //m_MesWeb = nullptr;
}

void OneCoorCaliCheckProcess::set_show_error_cali(const double e1, const double e2, const double e3,
                                             const double e4, const double e5, const double e6)
{
    //数据显示到界面
    QStringList list_e;
    list_e.append(QString::number(e1));
    if(ERR_DOUBLE_NUMBER != e2)
        list_e.append(QString::number(e2));
    if(ERR_DOUBLE_NUMBER != e3)
        list_e.append(QString::number(e3));
    if(ERR_DOUBLE_NUMBER != e4)
        list_e.append(QString::number(e4));
    if(ERR_DOUBLE_NUMBER != e5)
        list_e.append(QString::number(e5));
    if(ERR_DOUBLE_NUMBER != e6)
        list_e.append(QString::number(e6));
    sig_text(3 + m_CaliInfo.size() - 1, list_e);
    m_CaliInfo.append(list_e);
}

void OneCoorCaliCheckProcess::set_show_error_check(const double e1, const double e2, const double e3,
                                             const double e4, const double e5, const double e6)
{
    //数据显示到界面
    QStringList list_e;
    list_e.append(QString::number(e1));
    if(ERR_DOUBLE_NUMBER != e2)
        list_e.append(QString::number(e2));
    if(ERR_DOUBLE_NUMBER != e3)
        list_e.append(QString::number(e3));
    if(ERR_DOUBLE_NUMBER != e4)
        list_e.append(QString::number(e4));
    if(ERR_DOUBLE_NUMBER != e5)
        list_e.append(QString::number(e5));
    if(ERR_DOUBLE_NUMBER != e6)
        list_e.append(QString::number(e6));
    sig_text(3 + m_CheckInfo.size() - 1, list_e);
    m_CheckInfo.append(list_e);
}

//MES上传校表信息
int OneCoorCaliCheckProcess::mes_upload_cali_msg()
{
    return 1;
}
int OneCoorCaliCheckProcess::mes_upload_check_msg()
{
    return 1;
}
//MES过包装线
int OneCoorCaliCheckProcess::mes_pass_package()
{
    return 1;
}

//误差开始、读取封装
int OneCoorCaliCheckProcess::error_start_read(double &dbError, bool isABS)
{
    //循环获取误差，2,0.904中的2是圈数，当圈数大于等于2时便可将误差读取出来
    int maxNum = 60;//最大读取误差时间60*5 = 300秒
    //int iRet = m_pSimu_coor->dll_error_start(m_nSerial, m_dbConstant, m_nPluse, m_nDevPort);
    int iRet = m_pSimu_coor->dll_error_start(m_nSerial, m_dbConstant, 1, m_nDevPort);//CHANGED
    if(iRet < 0)
        return iRet;
    QLOG_TRACE() << "开始读误差,位置:" << m_nSerial;
    mutex1.lock();
    QThread::msleep(30);
    m_pSimu_coor->set_read_start_num_add1();
    mutex1.unlock();
    //在此等待至所有表台都发送完读取误差指令,替换延时等待
    while(m_pSimu_coor->get_read_start_num() != m_pSimu_coor->get_running_num() && m_pSimu_coor->get_read_start_num() != 0)
    {
        QThread::msleep(1000);
    }
    m_pSimu_coor->set_read_start_num(0);
    QLOG_TRACE() << "发送读误差完成,位置:" << m_nSerial;
    QThread::msleep(5000);
    while(maxNum--)
    {
        char *pacError = new char[128];
        iRet = m_pSimu_coor->dll_error_read(&pacError, m_nSerial, m_nDevPort);
        if(pacError == nullptr)
            continue;
        if(iRet < 0)
            return iRet;
        if((pacError[0] - 48) >= 2)
        {
            QLOG_TRACE() << "error: " << pacError << ",位置:" << m_nSerial;
            QString sErrResult((const char*)pacError);
            delete []pacError;
            pacError = nullptr;
            QStringList sErrList = sErrResult.split(",");
            QString sErrorB = sErrList[1];
            dbError = sErrorB.toDouble();
            if(isABS)
                dbError = fabs(dbError);
            return 1;
        }
        QThread::msleep(5000);
    }
    return -1;
}

void OneCoorCaliCheckProcess::do_cali_process()
{
    //**************************************//
    int iRet = cali_process();
    //手动停止情况+运行完成情况，断电
    m_pSimu_coor->set_running_num_dec1();
    if(m_pSharer->m_nStop!=0 || iRet > 0 || m_pSimu_coor->get_running_num() == 0)   //当急停、测试成功或者全部测试结束时断电
    {
        QLOG_TRACE() << "系统检测到断电..";
        QLOG_TRACE() << "断电,位置:" << m_nSerial;
        m_pSimu_coor->slot_power_off(m_nSerial, m_nSerial);
    }
    sig_result(1, iRet);
    if(iRet < 0)
    {
        QStringList list;
        list.append("校表失败:" + QString::number(iRet));
        sig_text(0, list);
        g_FailNum.lock();
        m_pSharer->m_FailNum++;
        if(m_pSharer->m_FailNum > m_pSharer->m_maxFailNum)
        {
            m_pSharer->m_nStop = 1;
            QLOG_TRACE() << "校表失败数量:" << m_pSharer->m_FailNum << ",超过最大允许失败数量,停止校表";
        }
        g_FailNum.unlock();
    }
    else if (iRet > 0)
    {
        if(m_pSharer->m_tStationInfo.m_PassPack)
        {
            QString sOut;
            report_process("过包装线", 97);
            //互斥，不要同时上传，相隔1S上传
            g_PakageMutex.lock();
            iRet = pass_pack_line(m_sBarCode, sOut);
            //QThread::msleep(5500);    //DEBUG
            QThread::msleep(m_pSharer->m_PackInterval); //此处用来测试不同间隔对于包装线上传MES的影响，后期测试完毕可改为固定5S
            g_PakageMutex.unlock();
            if(sOut.indexOf("OK") < 0)
            {
                iRet = -3;
            }
            if(iRet!=SOAP_OK || m_pSharer->m_nStop!=0)
            {
                report_process("过包装线失败", 97);
                QLOG_TRACE() << QString("过包装线失败,位置: %1, 错误码:%2").arg(m_nSerial).arg(iRet);
            }
            else
            {
                report_process("上传包装线成功", 100);
                QLOG_TRACE() << "上传包装线成功, 位置: " + m_nSerial;
            }
        }
        else
        {
            QLOG_TRACE() << "关闭上传包装线,位置:" << m_nSerial;
            report_process("关闭包装线", 100);
        }
    }
}

//校表
int OneCoorCaliCheckProcess::cali_process()
{
    //初始化信息
    m_CaliInfo.clear();
    if(m_pSharer->m_BarReadWay == 0)
    {
        m_CaliInfo.append(m_sBarCode);
        sig_text(2, m_CaliInfo);
    }
    m_pSharer->m_nStop = 0;
    m_pSharer->m_FailNum = 0;

    int iRet;
    double fError = 0.0;
    int nRetVal = 0;
    if(!m_isEnable)
    {
        QLOG_TRACE() << "未启用,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    if(!m_isOpen)
    {
        QLOG_TRACE() << "通信串口打开失败,位置:" << m_nSerial;
        return nRetVal;
    }

//**************1.上电****************//
//***********************************//
#if NO_TEST_MODE
    report_process("电源调整", 3);
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "升源失败,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        QLOG_TRACE() << "升源成功,位置:" << m_nSerial;
        --nRetVal;
    }
    QThread::msleep(20000);

//**************读MAC地址***************//
//*************************************//
    if(m_pSharer->m_BarReadWay == 1 && m_pSharer->m_MeterType == 0)
    {
        T_RCV_PACK tPackData;
        iRet = m_pCoorPort->send_wait(TYPE_GET_BARCODE, tPackData, 10000, s645Addr);
        if (iRet != TYPE_GET_BARCODE || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "读取MAC失败,位置:" << m_nSerial << "错误码" << iRet;
            return nRetVal;
        }
        else
        {
            QLOG_TRACE() << "读取MAC成功,位置:" << m_nSerial << " " << tPackData.barcode;
            --nRetVal;
        }
        m_sBarCode = tPackData.barcode;

        QStringList list;
        list.append(m_sBarCode);
        m_CaliInfo.append(m_sBarCode);
        sig_text(2, list);
        QLOG_TRACE() << "读取MAC地址成功";
        QThread::msleep(3000);
    }
#endif
//**************智能终端配置开始校表*******************//
//**************************************************//
    if(m_pSharer->m_MeterType == 1)
    {
        QThread::msleep(15000);
        iRet = espd_start();
        if(iRet < 0 || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "终端开启校表配置失败,位置:" << m_nSerial;
            return nRetVal;
        }
        else
        {
            QLOG_TRACE() << "终端开启校表配置成功,位置:" << m_nSerial;
            nRetVal--;
        }
        QThread::msleep(5000);
    }
#if NO_TEST_MODE
//**************2.计量参数初始化*********************//
//************************************************//
    iRet = step01_02_init_coors();
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "初始化失败01,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        QLOG_TRACE() << "初始化完成,位置:" << m_nSerial;
        --nRetVal;
    }
    QThread::msleep(5000);
//**************3.1.校准电压电流***********************//
//*************************************************//
    iRet = step02_01_cali_volt_curr();
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "校正电压电流失败,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    QThread::msleep(5000);
//**************3.2.读取电压电流确认是否成功******************//
//***************************************************//
//**********初次检查电压电流**********//
    T_Data_Instant t;
    iRet = step_02_03_check_volt_curr(t);
    if(iRet <0 || m_pSharer->m_nStop!=0)
    {
        //再次校表
        step02_01_cali_volt_curr();
    }
    QLOG_TRACE() << "检查电压电流01,位置:" << m_nSerial;
    QThread::msleep(5000);
//**********断电**********//
    QLOG_TRACE() << "断电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_off(m_nSerial, m_nSerial);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "断电失败,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        QLOG_TRACE() << "断电成功,位置:" << m_nSerial;
        --nRetVal;
    }
    report_process("电压电流检查", 10);
    //////////////////////////////////+ 先检查再断电再检查，可能存在数据写不到EE

    //此处为等待TTU断电延时
    if(m_pSharer->m_MeterType == 1)
    {
        QThread::msleep(60000);
    }
//**********上电**********//
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    QThread::msleep(20000);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }

//**********终端重新配置**********//
    if(m_pSharer->m_MeterType == 1)
    {
        QThread::msleep(15000);
        iRet = espd_restart();
        if(iRet < 0 || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "终端重新开启校表配置失败,位置:" << m_nSerial;
            return nRetVal;
        }
        else
        {
            QLOG_TRACE() << "终端重新开启校表配置成功,位置:" << m_nSerial;
            nRetVal--;
        }
        QThread::msleep(5000);
    }

//**********再次检查电压电流**********//
    iRet = step_02_03_check_volt_curr(t);
    //数据显示到界面
    set_show_error_cali(t.fUa, t.fUb, t.fUc, t.fIa, t.fIb, t.fIc);
    m_SaveError.A_VoltError = (220.0 - t.fUa);  //误差写入
    m_SaveError.B_VoltError = 220.0 - t.fUb;
    m_SaveError.C_VoltError = 220.0 - t.fUc;
    m_SaveError.A_CurrError = 220.0 - t.fIa;
    m_SaveError.B_CurrError = 220.0 - t.fIb;
    m_SaveError.C_CurrError = 220.0 - t.fIc;
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//********************4.校准功率比差********************//
//**************************************************//

    if(0 == m_pSimu_coor->dll_set_pause_channel(m_nSerial, 0, m_nDevPort))
    {
        QLOG_TRACE() << "切换脉冲采样通道失败";
        return -2;
    }
    report_process("功率比差校准 1.0", 17);
    QLOG_TRACE() << "启动功率比差校准,位置:" << m_nSerial;
    //double fError = 0.0;
//**********A相功率比差校准**********//
    iRet = step03_cali_power_ratio_1_0(1, fError);
    set_show_error_cali(fError);
    m_SaveError.A_Ib1Error = fError*1000;   //误差写入
    //nRetVal--;
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//**********B相功率比差校准**********//
    iRet = step03_cali_power_ratio_1_0(2, fError);
    set_show_error_cali(fError);
    m_SaveError.B_Ib1Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//**********C相功率比差校准**********//
    iRet = step03_cali_power_ratio_1_0(3, fError);
    set_show_error_cali(fError);
    m_SaveError.C_Ib1Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//********************5.校准功率相差********************//
//****************************************************//
    //开始校准功率相差
    report_process("功率相差校准0.5L", 35);
//**********A相功率相差校准**********//
    iRet = step03_cali_power_phase_0_5L(1, fError);
    //数据显示到界面
    set_show_error_cali(fError);
    m_SaveError.A_Ib05Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//**********B相功率相差校准**********//
    iRet = step03_cali_power_phase_0_5L(2, fError);
    //数据显示到界面
    set_show_error_cali(fError);
    m_SaveError.A_Ib05Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//**********C相功率相差校准**********//
    iRet = step03_cali_power_phase_0_5L(3, fError);
    //数据显示到界面
    set_show_error_cali(fError);
    m_SaveError.A_Ib05Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }

//********************6.检查分相轻载误差********************//
//****************************************************//
//**********分相轻载误差**********//
    report_process("轻载误差检查", 53);
    double fErrorA, fErrorB, fErrorC;

    iRet = step_check_power_Fx(m_fStdCurr, "0.5L", 10, fErrorA, fErrorB, fErrorC);
    set_show_error_cali(fErrorA, fErrorB, fErrorC);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//*****
    //集中器断电30S
    QLOG_TRACE() << "断电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_off(m_nDevPort, m_nSerial);
    QThread::msleep(30000);
//******************7.合相*************************************
//*************************************************//
    //额定功率合相1.0误差确认：
    report_process("合相1.0误差", 65);
    iRet = step_check_power_Hx(m_fStdCurr, "1.0", 100, fError);
    //数据显示到界面
    set_show_error_cali(fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }


    //额定功率合相0.5L误差确认：
    report_process("合相0.5L误差", 70);
    iRet = step_check_power_Hx(m_fStdCurr, "0.5L", 100, fError);

    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    //数据显示到界面
    set_show_error_cali(fError);

    //10%Ib，1.0合相轻载误差确认:
    report_process("1.0合相10%Ib轻载误差", 75);
    iRet = step_check_power_Hx(m_fStdCurr, "1.0", 10, fError);
    //数据显示到界面
    set_show_error_cali(fError);
    m_SaveError.H_01I1Error = fError*1000;   //误差写入
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }

#endif
    QLOG_TRACE() << "校表完成,位置:" << m_nSerial;

//**************智能终端配置结束校表*******************//
//**************************************************//
    if(m_pSharer->m_MeterType == 1)
    {
        iRet = espd_end();
        if(iRet < 0 || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "终端关闭校表配置失败,位置:" << m_nSerial;
            return nRetVal;
        }
        else
        {
            QLOG_TRACE() << "终端关闭校表配置成功,位置:" << m_nSerial;
            nRetVal--;
        }
    }
#if 0
//*********误差数据写入集中器**********//
//误差写入
    iRet = save_error(m_SaveError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "误差数据写入集中器失败,位置:" << m_nSerial;
    }
//误差写入
#endif

//********************校表完成，上传MES********************//
//******************************************************//
//#ifdef NO_TEST_MODE
    //存储本地
    m_cali_csv->set_rowdatas(m_CaliInfo);
    //上传MES
    QString sOut;
    if(m_pSharer->m_tStationInfo.m_EnableMes)
    {
        report_process("上传MES", 90);

        //互斥，不要同时上传，相隔1S上传
        g_CaliCheckMutex.lock();
        iRet = upload_cali_data(sListAutoCal,m_CaliInfo, sOut);
        QThread::msleep(1000);
        g_CaliCheckMutex.unlock();
        if(iRet!=SOAP_OK || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "校表数据上传MES失败,错误码:" + QString::number(iRet);
            return nRetVal;
        }
        else
        {
            --nRetVal;
        }
#if 0
        if(m_pSharer->m_tStationInfo.m_PassPack)
        {
            report_process("过包装线", 97);
            //互斥，不要同时上传，相隔1S上传
            g_PakageMutex.lock();
            iRet = pass_pack_line(m_sBarCode, sOut);
            //QThread::msleep(5500);    //DEBUG
            QThread::msleep(m_pSharer->m_PackInterval); //此处用来测试不同间隔对于包装线上传MES的影响，后期测试完毕可改为固定5S
            g_PakageMutex.unlock();
            if(sOut.indexOf("OK") < 0)
            {
                iRet = -3;
            }
            if(iRet!=SOAP_OK || m_pSharer->m_nStop!=0)
            {
                report_process("过包装线失败", 97);
                QLOG_TRACE() << "过包装线失败,错误码:" + QString::number(iRet);
                return nRetVal;
            }
        }
        else
        {
            QLOG_TRACE() << "关闭上传包装线,位置:" << m_nSerial;
        }
#endif
    }
    else
    {
        report_process("不连接MES,测试通过", 97);
        return 2;
    }

    report_process("测试通过", 97);
    return 1;

#if 0//无功确认：无功暂时不测，未调试
    iRet = step_06_1_check_reactive_power_1_0L();
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    //无功0.5L精度确认：
    iRet = step_06_2_check_reactive_power_0_5L();
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    //再次电压电流确认：
    iRet = step_02_03_check_volt_curr();
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
#endif
}
void OneCoorCaliCheckProcess::do_check_process()
{
    int iRet = check_process();
    //手动停止情况+运行完成情况，断电
    if(m_pSharer->m_nStop!=0 || iRet > 0)
    {
        QLOG_TRACE() << "系统检测到断电..";
        QLOG_TRACE() << "断电,位置:" << m_nSerial;
        m_pSimu_coor->slot_power_off(m_nSerial, m_nSerial);
    }
    m_pSimu_coor->set_running_num_dec1();
    emit sig_result(2, iRet);
    if(iRet < 0)
    {
        QStringList list;
        list.append("检表失败:" + QString::number(iRet));
        sig_text(0, list);
    }
}
//检表流程
int OneCoorCaliCheckProcess::check_process()
{
    //初始化信息
    m_CheckInfo.clear();
    if (m_pSharer->m_BarReadWay == 0)
    {
        m_CheckInfo.append(m_sBarCode);
        sig_text(2, m_CheckInfo);
    }
    m_pSharer->m_nStop = 0;
    m_pSharer->m_FailNum = 0;

    int iRet;
    double fErrorA, fErrorB, fErrorC;
    double fError;
    int nRetVal = 0;
    QLOG_TRACE() << "ret";
    if(!m_isEnable)
    {
        QLOG_TRACE() << "未启用,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    if(!m_isOpen)
    {
        QLOG_TRACE() << "通信串口打开失败,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
//**************1.上电****************//
//***********************************//
    report_process("电源调整", 3);
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "升源失败,位置:" << m_nSerial;
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    QThread::msleep(20000);
#if NO_TEST_MODE
//**************读MAC地址***************//
//*************************************//
    if(m_pSharer->m_BarReadWay == 1 && m_pSharer->m_MeterType == 0)
    {
        T_RCV_PACK tPackData;
        iRet = m_pCoorPort->send_wait(TYPE_GET_BARCODE, tPackData, 10000, s645Addr);
        if (iRet != TYPE_GET_BARCODE || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "读取MAC失败,位置:" << m_nSerial << "错误码" << iRet;
            return nRetVal;
        }
        else
        {
            QLOG_TRACE() << "读取MAC成功,位置:" << m_nSerial << " " << tPackData.barcode;
        }
        m_sBarCode = tPackData.barcode;

        QStringList list;
        list.append(m_sBarCode);
        sig_text(2, list);
        QLOG_TRACE() << "读取MAC地址成功";
        QThread::msleep(3000);
    }

//**********电压电流检测**********//
    report_process("电压电流检查", 3);
    QLOG_TRACE() << "开始检测电压电流,位置:" << m_nSerial;
    T_Data_Instant t;
    iRet = step_02_03_check_volt_curr(t);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(t.fUa, t.fUb, t.fUc, t.fIa, t.fIb, t.fIc);

//**********Imax误差检测**********//
    report_process("Imax误差检查", 3);
    QLOG_TRACE() << "开始检测Imax误差,位置:" << m_nSerial;
    iRet = step_02_04_check_Imax(t);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(t.fUa, t.fUb, t.fUc, t.fIa, t.fIb, t.fIc);

//**********有功单相检测Imax-Ib-0.1Ib  1.0-0.8C-0.5L**********//

//**********Imax-1.0分相误差**********//
    report_process("Imax-1.0分相误差", 3);
    QLOG_TRACE() << "开始检测Imax-1.0分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "1.0", 400, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********Imax-0.8C**********//
    report_process("Imax-0.8C分相误差", 3);
    QLOG_TRACE() << "开始检测Imax-0.8C分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.8C", 400, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********Imax-0.5L**********//
    report_process("Imax-0.5L分相误差",3);
    QLOG_TRACE() << "开始检测Imax-0.5L分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.5L", 400, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {

        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********Ib-1.0**********//
    report_process("Ib-1.0分相误差",3);
    QLOG_TRACE() << "开始检验Ib-1.0分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "1.0", 100,fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }

    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********Ib-0.8C**********//
    report_process("Ib-0.8C分相误差",3);
    QLOG_TRACE() << "开始检验Ib-0.8C分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.8C", 100, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********Ib-0.5L**********//
    report_process("Ib-0.5L分相误差",3);
    QLOG_TRACE() << "开始检验Ib-0.5L分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.5L", 100, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);
#endif

//**********0.1Ib-1.0**********//
    report_process("0.1Ib-1.0分相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib-1.0分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "1.0", 10, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********0.1Ib_0.8C**********//
    report_process("0.1Ib-0.8C分相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib-0.8C分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.8C", 10, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//**********0.1Ib_0.5L**********//
    report_process("0.1Ib-0.5L分相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib_0.5L分相误差,位置:" << m_nSerial;
    iRet = step_check_power_Fx(m_fStdCurr, "0.5L", 10, fErrorA, fErrorB, fErrorC);//ABC
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fErrorA, fErrorB, fErrorC);

//*********有功全相检测 Imax-Ib-0.1Ib-0.05Ib  1.0-0.8C-0.5L**********//

//**********CHP1.0_ImaxH**********//
    report_process("Imax-1.0合相误差",3);
    QLOG_TRACE() << "开始检验Imax_1.0合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fMaxCurr, "1.0", 400, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.8C_ImaxH**********//
    report_process("Imax-0.8C合相误差",3);
    QLOG_TRACE() << "开始检验Imax_0.8C合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fMaxCurr, "0.8C", 400, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.5L_ImaxH**********//
    report_process("Imax-0.5L合相误差",3);
    QLOG_TRACE() << "开始检验Imax_0.5L合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fMaxCurr, "0.5L", 400, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP1.0_IbH**********//
    report_process("Ib-1.0合相误差",3);
    QLOG_TRACE() << "开始检验Ib_1.0合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "1.0", 100, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.8C_IbH**********//
    report_process("Ib-0.8C合相误差",3);
    QLOG_TRACE() << "开始检验Ib_0.8C合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.8C", 100, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.5L_IbH**********//
    report_process("Ib-0.5L合相误差",3);
    QLOG_TRACE() << "开始检验Ib_0.5L合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.5L", 100, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP1.0_0.1_IbH**********//
    report_process("0.1Ib-1.0合相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib_1.0合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "1.0", 10, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.8C_0.1_IbH**********//
    report_process("0.1Ib-0.8C合相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib_0.8C合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.8C", 10, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.5L_0.1IbH**********//
    report_process("0.1Ib-0.5L合相误差",3);
    QLOG_TRACE() << "开始检验0.1Ib_0.5L合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.5L", 10, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP1.0_0.05IbH**********//
    report_process("0.05Ib-1.0合相误差",3);
    QLOG_TRACE() << "开始检验0.05Ib_1.0合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "1.0", 5, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.8C_0.05IbH**********//
    report_process("0.05Ib-0.8C合相误差",3);
    QLOG_TRACE() << "开始检验0.05Ib_0.8C合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.8C", 5, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);

//**********CHP0.5L_0.05IbH**********//
    report_process("0.05Ib-0.5L合相误差",3);
    QLOG_TRACE() << "开始检验0.05Ib_0.5L合相误差,位置:" << m_nSerial;
    iRet = step_check_power_Hx(m_fStdCurr, "0.5L", 5, fError);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    else
    {
        --nRetVal;
    }
    set_show_error_check(fError);
//**********日计时**********//
    report_process("日计时检测",3);
    QLOG_TRACE() << "开始检验日计时,位置:" << m_nSerial;
    //日计时检测
    double fErr;
    iRet = step_07_check_time_count(fErr);
    set_show_error_check(fErr);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        return nRetVal;
    }
    //存储本地
    m_check_csv->set_rowdatas(m_CheckInfo);
    //上传MES
    if(m_pSharer->m_tStationInfo.m_EnableMes)
    {
        report_process("上传MES", 97);
        QString sOut;
        g_CaliCheckMutex.lock();
        iRet = upload_check_data(sListAutoCheck, m_CheckInfo, sOut);
        QThread::msleep(1000);
        g_CaliCheckMutex.unlock();
        if(iRet!=SOAP_OK || m_pSharer->m_nStop!=0)
        {
            QLOG_TRACE() << "数据上传MES失败,错误码:" + QString::number(iRet);
            return nRetVal;
        }
        else
        {
            --nRetVal;
        }
    }
    else
    {
        report_process("不连接MES,测试通过", 100);
    }
    report_process("检表通过.", 100);
    return 1;
}

#if 0//该函数取消，使用m_pSimu_coor中的接口，同步各线程
int OneCoorCaliCheckProcess::step01_init_supply()
{

    QLOG_TRACE() << "初始化--升源--设置电压220V, 1.5A, 100% IB, 1.0L, ,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->dll_adjust_ui(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 50,
                                                        0, 0, 100, 100
                                                        ,"H", "1.0","HS5320", m_nDevPort);
    if(iRet <= 0)
    {
        QLOG_WARN() << "" << m_nSerial << "设置标准源电压电流失败";
        return FUNCTION_NULL;
    }

    QThread::msleep(2000);
    //取电压并校准,六次以内达到稳定值，否则失败
    int nTimes = 6;
    while(nTimes --)
    {
        QLOG_TRACE() << "取电压电流值,位置:" << m_nSerial;
        char *sData;
        sData = new char[256];
        memset(sData, 0, 256);
        int ret = m_pSimu_coor->dll_std_meter_read(sData, "HS5320", m_nDevPort);
        if(ret < 0)
        {
            QLOG_WARN() << "位置:" << m_nSerial << "取值失败";
            return -9;
        }
        QString str(sData);
        delete []sData; sData = nullptr;
        QStringList sListVal = str.split(",");
        if(sListVal.size() < 6)
        {
            continue;
        }
        float Ua, Ub, Uc, Ia, Ib, Ic;
        Ua = sListVal[0].toFloat();
        Ub = sListVal[1].toFloat();
        Uc = sListVal[2].toFloat();
        Ia = sListVal[3].toFloat();
        Ib = sListVal[4].toFloat();
        Ic = sListVal[5].toFloat();
        //如果没有稳定
        if((fabs(Ua - m_fStdVolt) > m_fVoltLimit) || (fabs(Ub - m_fStdVolt) > m_fVoltLimit) || (fabs(Uc - m_fStdVolt) > m_fVoltLimit)
           ||(fabs(Ia - m_fStdCurr) > m_fCurrLimit) || (fabs(Ib - m_fStdCurr) > m_fCurrLimit) || (fabs(Ic - m_fStdCurr) > m_fCurrLimit))
        {
            QLOG_WARN() << "位置:" << m_nSerial << "尚未稳定";
            QThread::msleep(5000);
            continue;//待定
        }
        else
        {
            QLOG_WARN() << "位置:" << m_nSerial << "已稳定";
            return 1;
        }
        QThread::msleep(2000);
    }


    return -1;//未稳定
}

#endif
int OneCoorCaliCheckProcess::step01_02_init_coors()
{
   T_RCV_PACK tPackData;
   //该指令没回复，iRet为-1，不必判断
   m_pCoorPort->send_wait(TYPE_INITIAL, tPackData, 1000, sJCAddr);
   QThread::msleep(1000);
   m_pCoorPort->send_wait(TYPE_INITIAL, tPackData, 1000, sJCAddr);
   QThread::msleep(1000);
   m_pCoorPort->send_wait(TYPE_INITIAL, tPackData, 1000, sJCAddr);
   QThread::msleep(1000);
   QThread::msleep(2000);

   return 1;
//   if(iRet > 0)
//   {
//       QLOG_TRACE() << "初始化，端口：" << m_nSerial<< "成功";
//       return 1;
//   }
//   QLOG_TRACE() << "初始化，端口：" << m_nSerial<< "失败";
//   return -1;
}
//集中器交采程序不识别数据，固定220V 1.5A
int OneCoorCaliCheckProcess::step02_01_cali_volt_curr(int nPhase)
{
    T_RCV_PACK tPackData;
    int iRet = 0;
    if(nPhase == 0 || nPhase == 1)
    {
        //A、B、C相电压校准
        QLOG_TRACE() << "校准电流电压A相,位置:" << m_nSerial;
        iRet = m_pCoorPort->send_wait(TYPE_SET_VOLT_CURR_DIFF, tPackData, 1, 220, 10000);
        if(iRet < 0)
        {
            QLOG_TRACE() << "校准电流电压A相,位置:" << m_nSerial<< "失败";
            return -1;
        }
        QThread::msleep(3000);
    }
    if(nPhase == 0 || nPhase == 2)
    {
        QLOG_TRACE() << "校准电流电压B相,位置:" << m_nSerial;
        //B相电压校准
        iRet = m_pCoorPort->send_wait(TYPE_SET_VOLT_CURR_DIFF,tPackData, 2, 220, 10000);
        if(iRet < 0)
        {
            QLOG_TRACE() << "校准电流电压B相,位置:" << m_nSerial<< "失败";
            return -2;
        }
        QThread::msleep(3000);
    }
    if(nPhase == 0 || nPhase == 3)
    {
        QLOG_TRACE() << "校准电流电压C相,位置:" << m_nSerial;
        //C相电压校准
        iRet = m_pCoorPort->send_wait(TYPE_SET_VOLT_CURR_DIFF,tPackData, 3, 220, 10000);
        //iRet = m_pCoorPort->send_wait(TYPE_SET_VOLT_CURR_DIFF,tPackData, 3, 220, 10000);
        if(iRet < 0)
        {
            QLOG_TRACE() << "校准电流电压C相,位置:" << m_nSerial<< "失败";
            return -3;
        }
        QThread::msleep(3000);
    }

    return 1;
}

int OneCoorCaliCheckProcess::step_02_03_check_volt_curr(T_Data_Instant& tOut)
{
    //***************检电压************************//
    QLOG_TRACE() << "确认电压,位置:" << m_nSerial;
    T_RCV_PACK tPackData;
    int iRet = m_pCoorPort->send_wait(TYPE_GET_VOLT, tPackData, 30000);
    if(iRet < 0)
    {
        QLOG_WARN() << "校准后获取电压超时,位置:" << m_nSerial;
        return iRet;
    }
    tOut.fUa = tPackData.tDataVoltBlock.fUa;
    tOut.fUb = tPackData.tDataVoltBlock.fUb;
    tOut.fUc = tPackData.tDataVoltBlock.fUc;
    QLOG_TRACE() << tPackData.tDataVoltBlock.fUa << tPackData.tDataVoltBlock.fUb << tPackData.tDataVoltBlock.fUc;
    if(fabs(tPackData.tDataVoltBlock.fUa - m_fStdVolt) > m_fVoltLimit ||
            fabs(tPackData.tDataVoltBlock.fUb - m_fStdVolt) > m_fVoltLimit ||
            fabs(tPackData.tDataVoltBlock.fUc - m_fStdVolt) > m_fVoltLimit)
    {
        QLOG_WARN() << "电压校准失败,位置:" << m_nSerial;
        return -1;
    }
    QThread::msleep(1000);
    m_pCoorPort->readAll();
    //*************检电流************************//
    QLOG_TRACE() << "确认电流,位置:" << m_nSerial;
    iRet = m_pCoorPort->send_wait(TYPE_GET_CURR, tPackData, 30000);
    if(iRet < 0)
    {
        QLOG_WARN() << "校准后获取电流超时,位置:" << m_nSerial;
        return iRet;
    }
    tOut.fIa = tPackData.tDataCurrBlock.fIa;
    tOut.fIb = tPackData.tDataCurrBlock.fIb;
    tOut.fIc = tPackData.tDataCurrBlock.fIc;
    QLOG_TRACE() << tPackData.tDataCurrBlock.fIa << tPackData.tDataCurrBlock.fIb << tPackData.tDataCurrBlock.fIc;
    if(fabs(tPackData.tDataCurrBlock.fIa - m_fStdCurr) > m_fCurrLimit ||
            fabs(tPackData.tDataCurrBlock.fIb - m_fStdCurr) > m_fCurrLimit ||
            fabs(tPackData.tDataCurrBlock.fIc - m_fStdCurr) > m_fCurrLimit)
    {
        QLOG_WARN() << "电流校准失败,位置:" << m_nSerial;
        return -2;
    }
    QLOG_WARN() << "电压电流校准确认成功,位置:" << m_nSerial;
    return 1;
}

int OneCoorCaliCheckProcess::step_02_04_check_Imax(T_Data_Instant & tOut)
{
    //Imax = 6.0
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 400, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    QLOG_TRACE() << "确认电压,位置:" << m_nSerial;
    T_RCV_PACK tPackData;
    int iRet = m_pCoorPort->send_wait(TYPE_GET_VOLT, tPackData, 30000);
    if(iRet < 0)
    {
        QLOG_WARN() << "校准后获取电压超时,位置:" << m_nSerial;
        return iRet;
    }
    tOut.fUa = tPackData.tDataVoltBlock.fUa;
    tOut.fUb = tPackData.tDataVoltBlock.fUb;
    tOut.fUc = tPackData.tDataVoltBlock.fUc;
    if(fabs(tPackData.tDataVoltBlock.fUa - m_fStdVolt) >  m_fVoltLimit||
            fabs(tPackData.tDataVoltBlock.fUb - m_fStdVolt) > m_fVoltLimit ||
            fabs(tPackData.tDataVoltBlock.fUc - m_fStdVolt) > m_fVoltLimit)
    {
        QLOG_WARN() << "Imax电压校准失败,位置:" << m_nSerial;
        return -1;
    }
    QLOG_TRACE() << "确认电流,位置:" << m_nSerial;
    iRet = m_pCoorPort->send_wait(TYPE_GET_CURR, tPackData, 30000);
    if(iRet < 0)
    {
        QLOG_WARN() << "Imax电流示值获取超时,位置:" << m_nSerial;
        return iRet;
    }
    QLOG_TRACE() << "Imax:"<< tPackData.tDataCurrBlock.fIa << tPackData.tDataCurrBlock.fIb << tPackData.tDataCurrBlock.fIc;
    tOut.fIa = tPackData.tDataCurrBlock.fIa;
    tOut.fIb = tPackData.tDataCurrBlock.fIb;
    tOut.fIc = tPackData.tDataCurrBlock.fIc;
    if(fabs(tPackData.tDataCurrBlock.fIa - m_fMaxCurr)*100/1.5 > m_fCurrLimit_Imax ||
            fabs(tPackData.tDataCurrBlock.fIb - m_fMaxCurr)*100/1.5 > m_fCurrLimit_Imax ||
            fabs(tPackData.tDataCurrBlock.fIc - m_fMaxCurr)*100/1.5 > m_fCurrLimit_Imax)
    {
        QLOG_WARN() << "Imax电流校准失败,位置:" << m_nSerial;
        return -2;
    }
    QLOG_WARN() << "Imax电压电流校准确认成功,位置:" << m_nSerial;
    return 1;
}

//校准功率因数1.0时的功率比差
int OneCoorCaliCheckProcess::step03_cali_power_ratio_1_0(int nPhase, double& dbError)
{

    QThread::sleep(1);
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, CASPHASE[nPhase].toLatin1().data(), "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    QLOG_TRACE() << "切换脉冲采样通道：" << "0-有功正向/反向";
//    if(0 == m_pSimu_coor->dll_set_pause_channel(m_nSerial, 0, m_nDevPort))
//    {
//        QLOG_TRACE() << "切换脉冲采样通道失败";
//        return -2;
//    }
    QThread::sleep(5);
    QLOG_TRACE() << "启动功率比差校准,位置:" << m_nSerial << "相位:" << CASPHASE[nPhase];
    T_RCV_PACK tPackData;
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_POWER_RATIO, tPackData, nPhase, 0.0, 10000);
//    QThread::msleep(500);
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_POWER_RATIO, tPackData, nPhase, 0.0, 10000);
//    QThread::msleep(500);
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_POWER_RATIO, tPackData, nPhase, 0.0, 10000);
    QLOG_TRACE() << "校准电流功率比差,位置:" << m_nSerial;
    dbError = 0.0;

    QThread::msleep(8000);
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -1;
    }
    //////////////////////////////////+ 误差稍微放小一点 0.8--->0.75
    QLOG_TRACE() << "功率比差误差值：" << dbError << ",位置:" << m_nSerial;
    if(fabs(dbError) < m_dbLimitFX)
    {
        QLOG_TRACE() << "误差合格，采集下一相";
        return 1;
    }
    QLOG_TRACE() << "误差不合格，开始写入";
    iRet = m_pCoorPort->send_wait(TYPE_SET_POWER_RATIO, tPackData, nPhase, dbError, 10000);

    if(iRet > 0)
        QLOG_TRACE() << "功率比差写入集中器成功，相位" << nPhase;
    else
        QLOG_TRACE() << "功率比差写入集中器失败，相位" << nPhase;

    QThread::msleep(15000);

    //重新读取误差：
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -2;
    }
    if(fabs(dbError) > m_dbLimitFX)
    {
        QLOG_TRACE() << "重新读取误差：" << dbError <<nPhase;
        iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_POWER_RATIO, tPackData, nPhase, 0.0, 10000);
        QThread::msleep(8000);
        error_start_read(dbError);
        QLOG_TRACE() << "重新校准电流功率比差,位置:" << m_nSerial;
        m_pCoorPort->send_wait(TYPE_SET_POWER_RATIO, tPackData, nPhase, dbError, 10000);
        QThread::msleep(8000);

        error_start_read(dbError);
        if(fabs(dbError) > m_dbLimitFX)
        {
            return -3;
        }
        else
        {
            return 2;
        }
    }
    else
    {
        QLOG_WARN() << "误差确认成功,位置:" << m_nSerial;
        return 1;
    }
}

 //功率因数0.5L时的功率相差校准 1.0 0.8C 0.5L
int OneCoorCaliCheckProcess::step03_cali_power_phase_0_5L(int nPhase, double& dbError)
{
    QLOG_TRACE() << "启动功率因数0.5L时功率相差校准,位置:" << m_nSerial << "相位:" << CASPHASE[nPhase];
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, CASPHASE[nPhase].toLatin1().data(), "0.5L", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@功率因数0.5L时功率相差校准";
        return -1;
    }
//    QLOG_TRACE() << "切换脉冲采样通道：" << "0-有功正向/反向";
//    if(0 > m_pSimu_coor->dll_set_pause_channel(m_nSerial, 0, m_nDevPort))
//    {
//        QLOG_TRACE() << "切换脉冲采样通道成功";
//        return -2;
//    }
    QThread::msleep(15000);
    T_RCV_PACK tPackData;
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_PHASE_DIFF, tPackData, nPhase, 0.0, 10000);
//    QThread::msleep(500);
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_PHASE_DIFF, tPackData, nPhase, 0.0, 10000);
//    QThread::msleep(500);
//    iRet = m_pCoorPort->send_wait(TYPE_INIT_CALI_PHASE_DIFF, tPackData, nPhase, 0.0, 10000);
    QLOG_TRACE() << "启动校准电流功率相差,位置:" << m_nSerial;
    dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -1;
    }

    if(fabs(dbError) < m_dbLimitFX)
    {
        QLOG_TRACE() << "误差合格,位置:" << m_nSerial;
        return 1;
    }


    QLOG_TRACE() << "位置:" << m_nSerial<< "功率相差：" << dbError;

    iRet = m_pCoorPort->send_wait(TYPE_SET_CALI_PHASE_DIFF, tPackData, nPhase, dbError, 10000);

    if(iRet > 0)
        QLOG_TRACE() << "功率相差写入集中器成功，相位" << nPhase;
    else
        QLOG_TRACE() << "功率相差写入集中器失败，相位" << nPhase;

    QThread::msleep(15000);
    //重新读取误差：
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -2;
    }
    if(fabs(dbError) > m_dbLimitFX)
    {
        QLOG_TRACE() << "重新读取误差：" << dbError <<nPhase;
        m_pCoorPort->send_wait(TYPE_INIT_CALI_PHASE_DIFF, tPackData, nPhase, 0.0, 10000);
        QThread::msleep(8000);
        error_start_read(dbError);
        QLOG_TRACE() << "重新校准电流功率比差,位置:" << m_nSerial;
        m_pCoorPort->send_wait(TYPE_SET_CALI_PHASE_DIFF, tPackData, nPhase, dbError, 10000);
        QThread::msleep(8000);
        error_start_read(dbError);
        if(fabs(dbError) > m_dbLimitFX)
        {
            return -3;
        }
        else
        {
            return 2;
        }
    }
    else
    {
        return 1;
    }
    return iRet;
}
//功率精度误差测试，合相（封装）
int OneCoorCaliCheckProcess::step_check_power_Hx(float fI, QString sCosP, float fPercent, double& dbError)
{
    double dbLimit = m_dbLimitHX;//合相误差限值
    if(fPercent == 10)  //轻载误差
        dbLimit = m_dbLimitQZ;
    QLOG_TRACE() <<"控制表台标准表切换到100%Ib，合相：" << fI << sCosP << fPercent << ".index:" << m_nSerial;
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, fPercent, "H", sCosP.toLatin1().data(), m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@功率精度误差测试，合相"<< ",位置:" << m_nSerial;
        return -9;
    }
//    if(fPercent == 10)//10% Ib时加互斥，否则误差过大
//    {
//        g_10PercentIb_Hx.lock();
//    }
    QLOG_TRACE() << "切换脉冲采样通道：" << "0-有功正向/反向"<< ",位置:" << m_nSerial;
    if(0 > m_pSimu_coor->dll_set_pause_channel(m_nSerial, 0, m_nDevPort))
    {
        QLOG_TRACE() << "切换脉冲采样通道失败"<< ",位置:" << m_nSerial;
        return -2;
    }

    //4个脉冲后读取误差，确认误差合格，否则直接标记
    QThread::msleep(8000);
    dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败"<< ",位置:" << m_nSerial;
        return -1;
    }

    if(dbLimit < dbError)
    {
        QLOG_WARN() << "额定功率合相1.0L误差确认失败,当前误差:" << dbError<< ",位置:" << m_nSerial;
        return -2;
    }
    QLOG_TRACE() << "额定功率合相1.0L误差确认成功"<< ",位置:" << m_nSerial;
    QLOG_TRACE() << "误差总清,位置:" << m_nSerial;
    //m_pSimu_coor->dll_error_clear(m_nDevPort);

//    if(fPercent == 10)//10% Ib时加互斥，否则误差过大
//    {
//        g_10PercentIb_Hx.unlock();
//    }
    return 1;
}
//功率精度误差测试，分相（封装）
int OneCoorCaliCheckProcess::step_check_power_Fx(float fI, QString sCosP, float fPercent, double& fErrorA, double& fErrorB, double& fErrorC)
{
    double dbLimit = m_dbLimitFX;//误差xian限值
    if(fPercent == 10)
        dbLimit = m_dbLimitQZ;
    QLOG_TRACE() <<"控制表台标准表切换到100%Ib，分相（A加电流）" << fI << sCosP << fPercent << "index:" << m_nSerial;
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, fPercent, "A", sCosP.toLatin1().data(), m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@功率精度误差测试，分相A" << ",位置:" << m_nSerial;
        return -9;
    }
    QLOG_TRACE() << "切换脉冲采样通道：" << "0-有功正向/反向" << ",位置:" << m_nSerial;
    if(0 > m_pSimu_coor->dll_set_pause_channel(m_nSerial, 0, m_nDevPort))
    {
        QLOG_TRACE() << "切换脉冲采样通道成功"<< ",位置:" << m_nSerial;
        return -2;
    }
    //4个脉冲后读取误差，确认误差合格，否则直接标记
    QThread::msleep(8000);
    double dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败"<< ",位置:" << m_nSerial;
        return -1;
    }
    fErrorA = dbError;
    if(dbLimit < dbError)
    {
        QLOG_WARN() << "误差确认失败,当前误差:" << dbError << ",位置:" << m_nSerial;
        return -2;
    }
    QLOG_TRACE() << "A相误差确认成功";
    QLOG_TRACE() <<"控制表台标准表切换到100%Ib，分相（B加电流）" << fI << sCosP << fPercent << ".index:" << m_nSerial;
    QLOG_TRACE() << "上电,位置::" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, fPercent, "B", sCosP.toLatin1().data(), m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@功率精度误差测试，分相B" << ",位置:" << m_nSerial;
        return -9;
    }
    //4个脉冲后读取误差，确认误差合格，否则直接标记
    QThread::msleep(8000);
    dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -1;
    }
    fErrorB = dbError;
    if(dbLimit < dbError)
    {
        QLOG_WARN() << "误差确认失败,当前误差:" << dbError;
        return -2;
    }
    QLOG_TRACE() << "B相误差确认成功" << ",位置:" << m_nSerial;

    QLOG_TRACE() <<"控制表台标准表切换到100%Ib，分相（C加电流）" << fI << sCosP << fPercent << ".index:" << m_nSerial;
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, fPercent, "C", sCosP.toLatin1().data(), m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@功率精度误差测试，分相C" << ",位置:" << m_nSerial;
        return -9;
    }
    //4个脉冲后读取误差，确认误差合格，否则直接标记
    QThread::msleep(8000);
    dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败" << ",位置:" << m_nSerial;
        return -1;
    }
    fErrorC = dbError;
    if(dbLimit < dbError)
    {
        QLOG_WARN() << "误差确认失败,当前误差:" << dbError << ",位置:" << m_nSerial;
        return -2;
    }
    QLOG_TRACE() << "C相误差确认成功" << ",位置:" << m_nSerial;
    return 1;
}
//额定功率合相1.0L误差确认：
int OneCoorCaliCheckProcess::step_05_1_check_power_toghter_Ib(QString sCosP, double nPercent)
{

}

//额定功率合相IMax = 6.0误差确认：
int OneCoorCaliCheckProcess::step_05_4_check_power_toghter_IMax(QString sCosP)
{
    //控制表台标准表切换到100%Imax，合相（ABC都加电流）
    QLOG_TRACE() << "上电,位置:" << m_nSerial;
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 400, "H", sCosP.toLatin1().data(), m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@额定功率合相IMax = 6.0"<< ",位置:" << m_nSerial;
        return -9;
    }
    //2个脉冲后读取误差，确认误差合格，否则直接标记
    QThread::msleep(4000);
    double dbError = 0.0;
    if(error_start_read(dbError, 90000) < 0)
    {
        QLOG_WARN() << "获取误差失败"<< ",位置:" << m_nSerial;
        return -1;
    }

    if(m_dbLimitHX < dbError)
    {
        QLOG_WARN() << "imax额定功率合相1.0误差确认失败"<< ",位置:" << m_nSerial;
        return -2;
    }
    QLOG_WARN() << "额定功率合相1.0误差确认成功"<< ",位置:" << m_nSerial;
    return 1;
}


//日计时检测
int OneCoorCaliCheckProcess::step_07_check_time_count(double& fErrorOut)
{
    fErrorOut = -999.0;

    QLOG_WARN() << "端口" << m_nSerial << "设置脉冲采样通道为4"<< ",位置:" << m_nSerial;
    int iRet = m_pSimu_coor->dll_set_pause_channel(m_nSerial, 4, m_nDevPort);

    iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet < 0)
    {
        QLOG_TRACE() << "升源失败@额定功率合相IMax = 6.0";
        return -9;
    }
    QLOG_WARN() << "位置:" << m_nSerial << "切上标准晶振";
    iRet = m_pSimu_coor->dll_set_ref_clock(1, m_nDevPort);
    QLOG_WARN() << "位置:" << m_nSerial << "读取日计时误差";
    iRet = m_pSimu_coor->dll_clock_error_start(m_nSerial, m_nTheoryFreq, m_nClockTestDuration, m_nDevPort);
    char *sError = new char[256];
    memset(sError, 0, 256);
    QThread::msleep(10000);
    iRet = m_pSimu_coor->dll_clock_error_read(sError, 1, 1, m_nSerial, m_nDevPort);
    //判定标准 正负0.3
    qDebug() << "rjs:" << sError;
    //QLOG_TRACE() << sError;
    delete []sError; sError = nullptr;
    QLOG_WARN() << "位置:" << m_nSerial << "切开标准晶振";
    iRet = m_pSimu_coor->dll_set_ref_clock(0, m_nDevPort);
    fErrorOut = QString(sError).toDouble();
    QLOG_TRACE() << fErrorOut;
    return 1;
}

//无功1.0精度确认：无功暂时不检
int OneCoorCaliCheckProcess::step_06_1_check_reactive_power_1_0L()
{
    //表台标准表切换到  无功，100%Ib，1.0，合相
    //4个脉冲后读取误差，确认是否合格，不合格直接标记
    QThread::msleep(8000);
    double dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -1;
    }

    if(m_dbLimitHX < dbError)
    {
        QLOG_WARN() << "额定功率合相0.5L误差确认失败";
        return -2;
    }
    QLOG_WARN() << "额定功率合相0.5L误差确认成功";
    return 1;
}
//无功0.5L精度确认：
int OneCoorCaliCheckProcess::step_06_2_check_reactive_power_0_5L()
{
    //表台标准表切换到  无功，100%Ib，0.5L，合相

    //2个脉冲后读取误差，确认是否合格，不合格直接标记
    QThread::msleep(4000);
    double dbError = 0.0;
    if(error_start_read(dbError) < 0)
    {
        QLOG_WARN() << "获取误差失败";
        return -1;
    }

    if(m_dbLimitWG < dbError)
    {
        QLOG_WARN() << "额定功率合相0.5L误差确认失败";
        return -2;
    }
    QLOG_WARN() << "额定功率合相0.5L误差确认成功";
    return 1;
}

/////////////////////////////MES相关函数
////放到这里主要是因为写在单例中无法在线程中运行,先解决问题，原因待查20201115
//获取校表工作中心
int OneCoorCaliCheckProcess::get_cali_ws(const QString &sWsCali)
{
    m_pSharer->m_tStationInfo.m_sWorkStation = sWsCali;

    _ns1__GetWorkStation getStation;
    std::wstring sSta = sWsCali.toStdWString();
    getStation.workStationCode = &sSta;
    _ns1__GetWorkStationResponse res;
    int iRet = m_MesUSCore->GetWorkStation_(&getStation, res);//(ui->le_MO->text());
    //QLOG_TRACE() << "222222222";
    //QLOG_TRACE() << "33333333333333";
    if(iRet == SOAP_OK)
    {
        QString sStation = QString::fromStdWString(*res.GetWorkStationResult);
        QLOG_TRACE() << sStation;
        sStation.remove("[");
        sStation.remove("]");
        qDebug() << "workstation:" <<sStation;
        if(sStation.length() < 5)
            return -3;
        QByteArray baJason = sStation.toUtf8();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return -2;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_WorStationID = QString::number(rootObj["WORK_STATIONID"].toInt());
        m_pSharer->m_tStationInfo.m_sWorkStationName = rootObj["WORK_STATIONNAME"].toString();

        QLOG_TRACE() << m_pSharer->m_tStationInfo.m_sWorkStationName << m_pSharer->m_tStationInfo.m_WorStationID;
        return SOAP_OK;
    }
    return iRet;
}

//获取包装线工作中心
int OneCoorCaliCheckProcess::get_pack_ws(const QString &sWsPack)
{
    m_pSharer->m_tStationInfo.m_sWorkStation_pack = sWsPack;

    _ns1__GetWorkStation getStation;
    std::wstring sSta = sWsPack.toStdWString();
    getStation.workStationCode = &sSta;
    _ns1__GetWorkStationResponse res;
    int iRet = m_MesUSCore->GetWorkStation_(&getStation, res);//(ui->le_MO->text());
    if(iRet == SOAP_OK)
    {
        QString sStation = QString::fromStdWString(*(res.GetWorkStationResult));
        QLOG_TRACE() << sStation;
        sStation.remove("[");
        sStation.remove("]");
        qDebug() << "workstation:" <<sStation;
        if(sStation.length() < 5)
            return -3;
        QByteArray baJason = sStation.toUtf8();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return -2;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_WorStationID_pack = QString::number(rootObj["WORK_STATIONID"].toInt());
        m_pSharer->m_tStationInfo.m_sWorkStationName_pack = rootObj["WORK_STATIONNAME"].toString();

        QLOG_TRACE() << m_pSharer->m_tStationInfo.m_sWorkStationName << m_pSharer->m_tStationInfo.m_WorStationID;
        return SOAP_OK;
    }

    return iRet;
}

//获取制令单号
int OneCoorCaliCheckProcess::get_mo(const QString &sMoNum)
{
    m_pSharer->m_tStationInfo.m_sMo = sMoNum;

    _ns1__GetMo mo;
    std::wstring smo = sMoNum.toStdWString();
    mo.moCode = &smo;
    _ns1__GetMoResponse res;
    int bRet = m_MesUSCore->GetMo_(&mo, res);//(ui->le_MO->text());
    if(bRet == SOAP_OK)
    {
        QString sMo = QString::fromStdWString(*(res.GetMoResult));
        QLOG_TRACE() << sMo;
        QByteArray baJason = sMo.toLatin1();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return -2;// false;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_sModelCode = rootObj["MODEL_CODE"].toString();
        QString sCloseFlag = rootObj["CLOSE_FLAG"].toString();
        if(sCloseFlag.indexOf("3") >=0 || sCloseFlag.indexOf("6") >=0)
            m_pSharer->m_tStationInfo.m_bCloseFlag = true;// false;
        else
            m_pSharer->m_tStationInfo.m_bCloseFlag = false;// true;
        return SOAP_OK;
    }
    return bRet;
}

//获取操作员
int OneCoorCaliCheckProcess::get_operator(const QString &sOp)
{
    m_pSharer->m_tStationInfo.m_sOperator = sOp;

    _ns1__GetEmp emp;
    std::wstring s1 = sOp.toStdWString();
    emp.empCode = &s1;
    _ns1__GetEmpResponse res;
    int bRet = m_MesUSCore->GetEmp(&emp, res);
    if(bRet == SOAP_OK)
    {
        QString sOper = QString::fromStdWString(*(res.GetEmpResult));
        QLOG_TRACE() << sOper;
        sOper.remove("[");
        sOper.remove("]");
        qDebug() << "EMP:" <<sOper;

        if(sOper.length() < 5)
            return -3;
        QByteArray baJason = sOper.toUtf8();
        QJsonParseError json_error;
        QJsonDocument jsonDoc(QJsonDocument::fromJson(baJason, &json_error));
        if(json_error.error != QJsonParseError::NoError)
        {
            return  -2;
        }
        QJsonObject rootObj = jsonDoc.object();

        m_pSharer->m_tStationInfo.m_sOperatorName = rootObj["EMP_NAME"].toString();
        return SOAP_OK;
    }
    return bRet;
}

int OneCoorCaliCheckProcess::upload_cali_data(const QStringList& sListAutoCal, const QStringList& CaliInfo, QString& sOut)
{
    QString sJson = JSON_Format::Json_Format_upload(m_pSharer->m_tStationInfo.m_sWorkStation, m_pSharer->m_tStationInfo.m_sMo,
                                                      m_pSharer->m_tStationInfo.m_sOperator,m_pSharer->m_tStationInfo.m_sWorkStationName,
                                                      m_pSharer->m_tStationInfo.m_WorStationID, sListAutoCal, CaliInfo);

    QLOG_TRACE() << "cali上传数据：" <<sJson;
    _ns1__Save_USCOREPass_USCOREStation_USCOREResults up;
    std::wstring s =sJson.toStdWString();
    up.jsonStr = &s;
    std::wstring ob = sArrCode[0].toStdWString();
    up.objectCode = &ob;
    _ns1__Save_USCOREPass_USCOREStation_USCOREResultsResponse down;
    int iRet = m_MesUSCore->Save_USCOREPass_USCOREStation_USCOREResults(&up, down);
    if(iRet != SOAP_OK)
        return iRet;
    if(down.Save_USCOREPass_USCOREStation_USCOREResultsResult)
    {
        sOut = QString::fromStdWString(*down.Save_USCOREPass_USCOREStation_USCOREResultsResult);
    }
    else
    {
        QLOG_TRACE() << "MES ERROR in upload_cali_data";
        return -2;
    }
    QLOG_TRACE() << "cali Mes 回复数据：" <<sOut;
    if(sOut.indexOf("OK") < 0)
    {
        return -3;
    }
    //if(iRet != SOAP_OK)
    //    return -1;
    //else
    return SOAP_OK;
}
int OneCoorCaliCheckProcess::upload_check_data(const QStringList& sListAutoCheck, const QStringList& CheckInfo, QString& sOut)
{
    QString sJson = JSON_Format::Json_Format_upload(m_pSharer->m_tStationInfo.m_sWorkStation, m_pSharer->m_tStationInfo.m_sMo,
                                                      m_pSharer->m_tStationInfo.m_sOperator,m_pSharer->m_tStationInfo.m_sWorkStationName,
                                                      m_pSharer->m_tStationInfo.m_WorStationID, sListAutoCheck, CheckInfo);

    QLOG_TRACE() << "check上传数据：" <<sJson;
    _ns1__Save_USCOREPass_USCOREStation_USCOREResults up;
    std::wstring s =sJson.toStdWString();
    up.jsonStr = &s;
    std::wstring ob = sArrCode[1].toStdWString();
    up.objectCode = &ob;
    _ns1__Save_USCOREPass_USCOREStation_USCOREResultsResponse down;
    int iRet = m_MesUSCore->Save_USCOREPass_USCOREStation_USCOREResults(&up, down);
    if(iRet != SOAP_OK)
        return iRet;
    if(down.Save_USCOREPass_USCOREStation_USCOREResultsResult)
    {
        sOut = QString::fromStdWString(*down.Save_USCOREPass_USCOREStation_USCOREResultsResult);
    }
    else
    {
        QLOG_TRACE() << "MES ERROR in upload_check_data";
        return -2;
    }
    QLOG_TRACE() << "check Mes 回复数据：" <<sOut;
    //if(iRet != SOAP_OK)
    //    return -1;
    //else
    return SOAP_OK;
}
int OneCoorCaliCheckProcess::pass_pack_line(const QString& sSerial, QString& sOut)
{
    QString sJson = JSON_Format::Json_Format_package(m_pSharer->m_tStationInfo.m_sWorkStation_pack, m_pSharer->m_tStationInfo.m_sMo,
                                                      m_pSharer->m_tStationInfo.m_sOperator,m_pSharer->m_tStationInfo.m_sWorkStationName_pack,
                                                      m_pSharer->m_tStationInfo.m_WorStationID_pack, m_pSharer->m_tStationInfo.m_sModelCode,
                                                      m_PlatNo, QString::number(m_nSerial), sSerial);

    QLOG_TRACE() << "packet上传数据：" <<sJson;
    _ns1__AutoPackBOXSN up;
    std::wstring s = sJson.toStdWString();
    up.jsonmeg = &s;
    _ns1__AutoPackBOXSNResponse down;
    qDebug() << s;
    int iRet = m_MesUSCore->AutoPackBOXSN(&up, down);
    if(iRet != SOAP_OK)
        return iRet;
    if(down.AutoPackBOXSNResult)
    {
        sOut = QString::fromStdWString(*down.AutoPackBOXSNResult);
    }
    else
    {
        QLOG_TRACE() << "MES ERROR in pass_pack_line";
        return -2;
    }
    sOut = QString::fromStdWString(*down.AutoPackBOXSNResult);
    QLOG_TRACE() << "packet Mes 回复数据：" <<sOut;
    return SOAP_OK;
}

void OneCoorCaliCheckProcess::slot_start_readMac()
{
    if(!m_isEnable)
    {
        return;
    }
    //上电
    int iRet = m_pSimu_coor->slot_power_on(PHASE_3P4L, m_fStdVolt, m_fStdCurr, 100, "H", "1.0", m_nDevPort, m_fVoltLimit, m_fVoltLimit, m_nSerial);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "升源失败,位置:" << m_nSerial;
        return ;
    }
    else
    {
        QLOG_TRACE() << "升源成功,位置:" << m_nSerial;
    }
    QThread::msleep(3000);
    //读
    T_RCV_PACK tPackData;
    iRet = m_pCoorPort->send_wait(TYPE_GET_BARCODE, tPackData, 10000, s645Addr);
    if (iRet != TYPE_GET_BARCODE)
    {
        QLOG_TRACE() << "读取失败";
    }
    //断电
    QLOG_TRACE() << "断电,位置:" << m_nSerial;
    iRet = m_pSimu_coor->slot_power_off(m_nSerial, m_nSerial);
    if(iRet<0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << "断电失败,位置:" << m_nSerial;
        return;
    }
    else
    {
        QLOG_TRACE() << "断电成功,位置:" << m_nSerial;
    }
    QThread::msleep(50);   //适当延时
    m_sBarCode = tPackData.barcode;

    QStringList list;
    list.append(m_sBarCode);
    sig_text(2, list);
    QLOG_TRACE() << "读取MAC地址成功";
}


int OneCoorCaliCheckProcess::save_error(T_COOR_SAVE_ERROR tError)
{
    tError.Plat_No = m_PlatNo.toInt();
    tError.MeterLoc = m_nSerial;
    QDate date = QDate::currentDate();
    tError.Year = date.year();
    tError.Month = date.month();
    tError.Day = date.day();
    return m_pCoorPort->send_wait(tError, 10000);
}


int OneCoorCaliCheckProcess::espd_start()
{
    //打开串口
    //确认串口
    //登录
    //关闭容器
    //确认容器关闭
    //启动APP
    //确认APP启动

    bool isopen = m_pEspdPort->open(QIODevice::ReadWrite);
    if(isopen == false)
    {
        QLOG_TRACE() << "终端串口连接失败,位置:" << m_nSerial;
        //emit sig_port_error(m_nSerial);
        return -1;
        //暂停流程，等待确认是否继续
    }
    QString res;
    int iRet;
    //登录
    iRet = m_pEspdPort->login();
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("终端登录失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //关闭容器
    iRet = m_pEspdPort->stop_container(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("终端容器发送关闭失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //确认容器
    iRet = m_pEspdPort->check_container(res, 0);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("终端容器关闭失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //开启APP
    iRet = m_pEspdPort->start_app(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP发送开启指令失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //确认APP
    iRet = m_pEspdPort->check_app(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP开启失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //完成
    return 1;
}

int OneCoorCaliCheckProcess::espd_end()
{
    QString res;
    int iRet;
    //关闭APP
    iRet = m_pEspdPort->stop_app(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP发送关闭指令失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //开启容器
    iRet = m_pEspdPort->start_container(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("终端容器发送开启指令失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //确认容器
    iRet = m_pEspdPort->check_container(res, 1);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP开启失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //关闭串口
    if(m_pEspdPort->isOpen())
    {
        m_pEspdPort->close();
    }
    //结束
    return 1;
}

int OneCoorCaliCheckProcess::espd_restart()
{
    QString res;
    int iRet;
    if(!m_pEspdPort->isOpen())
    {
        bool isopen = m_pEspdPort->open(QIODevice::ReadWrite);
        if(isopen == false)
        {
            QLOG_TRACE() << "终端串口连接失败,位置:" << m_nSerial;
            emit sig_port_error(m_nSerial);
            return -1;
            //暂停流程，等待确认是否继续
        }
    }
    //登录
    iRet = m_pEspdPort->login();
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("终端登录失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //开启APP
    iRet = m_pEspdPort->start_app(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP发送开启指令失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //确认APP
    iRet = m_pEspdPort->check_app(res);
    if(iRet < 0 || m_pSharer->m_nStop!=0)
    {
        QLOG_TRACE() << QString("校表APP开启失败,错误码: %1 ,位置: %2").arg(iRet).arg(m_nSerial);
        return -1;
    }
    //结束
    return 1;
}
