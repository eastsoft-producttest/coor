#include "calicoorprocess.h"
#include <QMessageBox>
#include <QTimer>
CaliCoorProcess::CaliCoorProcess(QObject *parent) : QObject(parent)
{
    m_pSimu_coor = new simu_coor(SIMU_5300);
    if(0 != m_pSimu_coor->simu_coor_load())
    {
        QMessageBox::warning(nullptr, "无法加载dll", "无法加载dll，请检查!");
        QTimer::singleShot(0, this, SLOT(close()));
    }

    //创建设备串口列表,不管多少在线，建立32个
    int i = 0;
    unsigned int bit = 0x1;
    while(i < MAX_COOR_NUM)
    {
        com_port_coor* port = new com_port_coor(m_sPortNameList[i],i);
        QThread *pThread = new QThread;
        if((m_unOnLineBitMap >> i ) & bit)
        {
            port->open(QIODevice::ReadWrite);
        }

        m_pCoorPortList.push_back(port);
        i++;
    }
}
