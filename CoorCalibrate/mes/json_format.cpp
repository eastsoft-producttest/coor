﻿#include "json_format.h"
#include <qDebug>

JSON_Format::JSON_Format()
{

}

QString JSON_Format::Json_Format_upload(QString sWs,
                                        QString sMo,
                                        QString sOp,
                                        QString sWs_name,
                                        QString sWs_id,
                                        QStringList sName,
                                        QStringList sUpList)
{
    sWs.remove(" ");
    sMo.remove(" ");
    sOp.remove(" ");

    QVariantHash v_data;
    v_data.insert("M_SN", sUpList.at(0));
    //插入数组数据
    QJsonArray arr;

    QJsonObject obj;
    obj.insert("M_EC_ALL_DATA", "");
    obj.insert("M_EC_QTY_ALL_DATA", "");
    obj.insert("M_EMP", sOp);
    obj.insert("M_MO", sMo);
    obj.insert("M_POINT_ALL_DATA", "");
    obj.insert("M_PRODUCTS_QTY", "1");
    obj.insert("M_WORKSTATION_NAME", sWs_name);
    obj.insert("M_WORKSTATION_SN", sWs);
    obj.insert("M_WORK_STATIONID", sWs_id);

    arr.insert(0,QJsonValue(obj));
    //没有通过数据时，不必上传MES
    v_data.insert("PassStationInfo", arr);
    //数据
    QJsonObject obj_test;
    QJsonArray arr_test;
    QJsonArray arr_trace;
    QStringList listHead = sName;
    for(int i = 0; i< listHead.size()-1; i++)
    {
        obj_test.insert(listHead.at(i+1), sUpList[i+1]);
    }
    arr_test.insert(0,QJsonValue(obj_test));
    //没有通过数据时，不必上传MES
    v_data.insert("TestInfo", arr_test);
    v_data.insert("TraceInfo", arr_trace);
    //转为docment
    QJsonDocument data_doc = QJsonDocument::fromVariant(v_data);
    //data_doc.setObject(data_obj);

    //转为string
    QByteArray ba = data_doc.toJson(QJsonDocument::Compact);
    //20200824新MES上传格式要求必须要在前后两端加中括号
    QString data_str("[" + ba + "]");
    //qDebug() << "data_str" << data_str;
    return data_str;

}

QString JSON_Format:: Json_Format_package(QString sWs, QString sMo,
                                          QString sOp, QString sWs_name,
                                          QString sWs_id, QString sModel,
                                          QString sPlatNo, QString sMeterPos,
                                          QString sSN)
{
    sWs.remove(" ");
    sMo.remove(" ");
    sOp.remove(" ");

    QVariantHash v_data;
    v_data.insert("serialNumber", sSN);
    v_data.insert("strMO", sMo);
    v_data.insert("strModel", sModel);
    v_data.insert("workStationID", sWs_id);
    v_data.insert("workstationSn", sWs);
    v_data.insert("workstationName", sWs_name);
    v_data.insert("position", sMeterPos);
    v_data.insert("watthourMeter", sPlatNo);

    QJsonDocument data_doc = QJsonDocument::fromVariant(v_data);
    //转为string
    QByteArray ba = data_doc.toJson(QJsonDocument::Compact);
    QString data_str(ba);

    return data_str;
}

int JSON_Format::Json_Fromat_down(QString sJson)
{
    int iRet = 0;

    return iRet;
}
