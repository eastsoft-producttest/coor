﻿#ifndef JSON_FORMAT_H
#define JSON_FORMAT_H
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QVariantHash>
#include <QJsonDocument>
#include <QVector>

class JSON_Format
{
public:
    JSON_Format();
public:
    static QString Json_Format_upload(QString sWs,
                                      QString sMo,
                                      QString sOp,
                                      QString sWs_name,
                                      QString sWs_id,
                                      QStringList sName, QStringList sUpList);
    static QString Json_Format_package(QString sWs,
                                      QString sMo,
                                      QString sOp,
                                      QString sWs_name,
                                      QString sWs_id,
                                      QString sModel,
                                      QString sPlatNo, //表台号
                                      QString sMeterPos,//表位
                                      QString sSN);
    static int Json_Fromat_down(QString sJson);
private:
   // DATA_DOWN m_down;
};

#endif // JSON_FORMAT_H
