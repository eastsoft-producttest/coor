﻿#ifndef SIMU_COOR_H
#define SIMU_COOR_H
#include <QString>
#include "dll_func_addr.h"
#include <QLibrary>
#include <QMutex>
#include <QWaitCondition>
#include <QObject>
#include "QsLog.h"
#include "ini_profile.h"
/*
#-------------------------------------------------
#
# Class:  dll函数地址转接类，方便调用
# Author: lidl
# 注意：1. 多线程中使用，注意线程安全
#      2. （重要）上电/停电为所有端口统一操作，需要等待所有活动线程发来上电操作才统一进行上电，并检查电源
#      3. 活动线程数量管理
# Date:   2020-09-03
#
#-------------------------------------------------
*/
const QString DLL_NAME = "hscom.dll";
#define FUNCTION_NULL -60235


enum TYPT_SIMU_COOR
{
    SIMU_5300 = 0,
    SIMU_5320
};
/*
 * 相线
    0 单相
    1 三相四线有功
    2 三相三线有功
    3 90 度无功
    4 60 度无功
    5 四线正弦无功
    6 三线正弦无功
    7 单相无功
*/
enum E_PHASE
{
    PHASE_SINGLE = 0,
    PHASE_3P4L = 1,
    PHASE_3P3L,
    PHASE_90REACTIVE,
    PHASE_60REACTIVE,
    PHASE_4PSINREACTIVE,
    PHASE_3LSINREACTIVE,
    PHASE_SINGLEREACTIVE
};


/*脉冲采样通道 0-有功正向/反
向,1-有功反向,2-无功正向/反
向 ,3- 无 功 反 向 ,4- 时 钟 ,5- 投
切,6-需量.单相表误差脉冲通
道一般发 0 即可，正反向都在
同一个端子输出*/

//负载点功率因数 取值：1.0 0.5L 0.8C
const QString CosP[3] = {"1.0", "0.5L", "0.8C"};

const QString CASPHASE[4] = {"H","A","B","C"};

class simu_coor : public QObject
{
    Q_OBJECT
private:
    simu_coor(TYPT_SIMU_COOR t_simu);

public:
    ~simu_coor();
    static simu_coor* get_instance();
    int simu_coor_load(QString sDllName = DLL_NAME);
    int simu_coor_unload();

    //活动线程管理
    void set_running_num(int n) {m_nRunningNum = n;}
    void set_running_num_dec1() {m_nRunningNum--;}
    int get_running_num(){return m_nRunningNum;}
    void set_read_start_num(int n) {m_ReadStartNum = n;}
    int get_read_start_num(){return m_ReadStartNum;}
    void set_read_start_num_add1() {m_ReadStartNum++;}
    //char *sMETERTYPE = "HS5300B";
    QByteArray ba_MeterType;    //这个中间变量放在类里主要是为了QString转char*时内存不被杀死
    char *m_MeterType;          //标准表型号
private:
    static simu_coor* m_pInstance;
    TYPT_SIMU_COOR m_tSimu;
    QLibrary m_Lib_hscom;
    ini_profile* m_ini;

    QMutex mutex1;

    QWaitCondition condition1;

    QMutex mutex2;
    QMutex mutex3;
    int m_ReadStartNum = 0;     //已经发送读误差指令的线程数量
    int m_nRunningNum = 0;      //当前还在运行的线程总量
    int thread_count = 0;       //执行上电/断电的线程数量
    int in_count = 0;           //调试用
    int m_nPowerRet = 0;        //上电/断电结果
public:

public slots:
    //上电
    int slot_power_on(int nPhase, const double fVolt, const double fCurrent, const double fIPercent, char* sIabc, char* sCosp, const int nDevPort, const double fVoltLimit, const double fCurrentLimit, int nSerial);
    //调整电流电压
    int app_adjust_ui(int nPhase, const double fVolt, const double fCurrent, const double fIPercent, char* sIabc, char* sCosp, const int nDevPort, const double fVoltLimit, const double fCurrentLimit);
    //断电
    int slot_power_off(int nDevPort, int nSerial);
//    int slot_error_clear(int nDevPort);
//    int slot_power_pause();
private:
    //以下函数不允许独立线程单独调用
    int dll_adjust_ui(int nPhase,double dbRatedVolt,double dbRatedCurr,
                       double dbRatedFreq,int nPhaseSeq,int nCurrDir,
                       double dbVoltPer,double dbCurrPer,char* sIABC,
                       char* sCosP,char* sModel,int nDevPort);
    int dll_power_off(int nDevPort);


public:
    //以下三个接口尽量统一调用
    int dll_std_meter_read(char* &psData, char* sModel, int nDevPort);//
    int dll_error_clear(int nDevPort);//
    int dll_power_pause(int nDevPort);//

    int dll_error_read(char ** psError,int nMeterNo,int nDevPort);
    int dll_port_close()    ;
    int dll_set_pause_channel(int nMeterNo,int nChannelFlag,int nDevPort);
    int dll_error_start(int nMeterNo,double dbConstant,int nPulse,int nDevPort);
    int dll_adjust_cust(int nPhase,double dbRatedFreq,double dbVolt1,
                         double dbVolt2,double dbVolt3,double dbCurr1,
                         double dbCurr2,double dbCurr3,double dbUab,
                         double dbUac,double dbAng1,double dbAng2,
                         double dbAng3,char* sModel,int nDevPort);
    int dll_open_485_port(DWORD dwLngHdcComm,char * sBaudRate,unsigned char ucMeterPort);
    int dll_close_485_port(DWORD dwLngHdcCom);
    int dll_send_485_data(DWORD dwLngHdcCom, char* sMeterData);
    int dll_recv_485_data(DWORD dwLngHdcCom,char * sMeterData,int nDataLen);
    int dll_set_485_on(int nMeterNo,int nIsOnLine,int nDevPort);
    int dll_set_ref_clock(DLL_Byte ucSetFlag, DLL_Byte ucDevPort);
    int dll_clock_error_start(int nMeterNo, double dbTheoryFreq, int nTestTime, int nDevPort);
    int dll_clock_error_read(char* &sMError, double dbTheoryFreq, int nErrorType, int nMeterNo, int nDevPort);

    int dll_constant_start(int nMeterNo,double dbConstant, int nDevPort);
private://函数指针
    P_Power_Off         m_fpPower_Off           = nullptr;
    P_Error_Clear       m_fpError_Clear         = nullptr;
    P_Error_Read        m_fpError_Read          = nullptr;
    P_StdMeter_Read     m_fpStdMeter_Read       = nullptr;
    P_Port_Close        m_fpPort_Close          = nullptr;
    P_Adjust_UI         m_fpAdjust_ui           = nullptr;
    P_Power_Pause       m_fpPower_Pause         = nullptr;
    P_Set_Pulse_Channel m_fpSet_PauseChannel    = nullptr;
    P_Error_Start       m_fpError_Start         = nullptr;
    P_Adjust_CUST       m_fpAdjust_Cust         = nullptr;
    P_Open485CommPort   m_fpOpen_485            = nullptr;
    P_Close485CommPort  m_fpClose_485           = nullptr;
    P_Send485Data       m_fpSendData_485        = nullptr;
    P_Recv485Data       m_fpRecvData_485        = nullptr;
    P_SetRS485On        m_fpSetOn_485           = nullptr;
    PSetRefClock        m_fpSetRefClock         = nullptr;
    PClock_Error_Start  m_fpClockErrorStart     = nullptr;
    PClock_Error_Read   m_fpClockErrorRead      = nullptr;
    P_ConstStart        m_fpConstantStart       = nullptr;
};

#endif // SIMU_COOR_H
