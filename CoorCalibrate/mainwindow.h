﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "datadef.h"
#include "ini_profile.h"
#include "coorView.h"
#include "onecoorcalicheckprocess.h"
#include <QThread>
#include <qextserialport.h>
#include "TableModel.h"
#include <QTextToSpeech>
#include <TableView.h>
#include "sigletonsharer.h"
#include "QsLog/QsLog.h"
#include <QCloseEvent>
#include <QProcess>
#include <QMenu>
#include <QAction>
#include "settings.h"
#include "ui_serial_set.h"
#include "ui_generalset.h"
using namespace QsLogging;

namespace Ui {
class MainWindow;
}

typedef enum {
    START_WORK = 0,
    START_SCAN,
    STOP_WORK,
    GET_MES_DATA,
    BAR_CODE,
    ERROR_MSG,
    READ_MAC
}eScanType;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void closeEvent(QCloseEvent* event);
    ~MainWindow();
private:
    eScanType scan_type(QByteArray &ba_toJudge);
    void set_status_all(int nTestType, const QString& sStatus);
    void initial_params();
private:
    Ui::MainWindow *ui;
    //unsigned int m_unOnLineBitMap = 0;
    ini_profile m_ini;
    coorView *m_grphV;                //集中器视图
    simu_coor* m_pSimu;
    QList<OneCoorCaliCheckProcess*> m_pListProcess;
    QByteArray m_baScannerBuff;     //扫枪串口读取的数据
    QByteArray m_Metertype;
    //已扫描的index
    unsigned int m_unScannedIndexBitmap = 0;

    //char *sMETERTYPE = "HS5300B";

private:
    SigletonSharer *m_pSharer;
    //QSLog
    Logger *m_pLogger;
private:
    QList<QThread*> m_pListThreads;
    int m_nFinishedThreadNun;//已完成测试线程数量
    //扫码使能
    bool m_bCanScanBarcode = false;
    //扫枪串口
    QextSerialPort *m_pScanSerial;
    QTextToSpeech *m_speech = nullptr;
    QVector<QVoice> m_voices;

    QString m_sStationNo;
    int m_nRunningFrame = 0;        //当前在运行的检测 1：校表 2：检表 0:未运行
    TableView * m_pRunnTableView;   //流程视图

    int m_nMesCheckState = 0;

    bool m_speech_OK = false;       //语音功能是否好用
    Settings* m_settingsUI;         //测试配置界面
    ui_GeneralSet* m_UIGeneralSet;  //通用设置界面
    ui_serial_set* m_UISerialSet;   //串口配置界面

private:
    //将当前界面参数更新至ini文件
    void set_iniSetting();
    //从ini配置文件取出参数来更新
    void get_iniSetting();
    //菜单类设置
    void set_menu();
    //终端校表额外控件显示
    void set_TTUCali_show(bool status);

signals:
    void start_cali_thread();
    void start_check_thread();
    void start_read_mac();

private slots:
    void slot_write_log(const QString& msg, int level);
    void slot_scan_port_rcv();
    void slot_thread_finished();
    void slot_recv_progress(int nProgress);
    void slot_recv_text(int nStartCol, QStringList sListText);
    //终端串口连接失败槽函数
    void slot_recv_port_error(int n_serial);

    void engineSelected(int index);
    void localeChanged(const QLocale &locale);
    void voiceSelected(int index);

    void on_pb_confirm_clicked();
    void on_pb_StartCali_clicked();
    void on_pb_StartCheck_clicked();
    void on_cb_StationNo_currentIndexChanged(const QString &arg1);
    void on_cb_pattern_currentIndexChanged(const QString &arg1);
    void on_le_WorkStation_editingFinished();
    void on_le_Operator_editingFinished();
    void on_pb_StartScan_02_clicked();

    void on_le_MO_editingFinished();
    void on_pb_StartScan_clicked();
    void on_pb_check_em_clicked();
    void on_le_WorkStation_pack_editingFinished();
    void on_pb_terminal_cali_clicked();
    void on_pb_terminal_clicked();
    void on_cb_MesEnable_stateChanged(int arg1);
    void on_cb_PassPack_stateChanged(int arg1);
    void on_cb_standardMeter_currentIndexChanged(int index);
    void on_pb_OpenAll_clicked();
    void on_pb_CloseAll_clicked();
    void on_pb_LoadingSerial_clicked();

    void slot_setting_ui();         //显示测试设置界面
    void slot_ui_general_set();     //显示通用设置界面
    void slot_ui_Serial_set();      //显示串口设置界面
    void slot_ui_general_changed(); //通用设置界面参数发生改变槽

    void on_pb_loadingTermainal_clicked();
};

#endif // MAINWINDOW_H
