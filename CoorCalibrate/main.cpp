﻿#include "mainwindow.h"
#include <QApplication>
#include <QProcess>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString qss;
    //QFile qssFile("styles.css");//styles.css
    QFile qssFile(":/QSS/qss/styles.css");
    qssFile.open(QFile::ReadOnly);
    if (qssFile.isOpen())
    {
        qss = QLatin1String(qssFile.readAll());
        a.setStyleSheet(qss);
        qssFile.close();
    }

    MainWindow w;
    w.show();

//    int e = a.exec();
//    if(e == 888)
//    {
//       QProcess::startDetached(qApp->applicationFilePath(), QStringList());
//       return 0;
//    }

    w.setWindowTitle("终端设备校表/检表");
    w.setWindowState(Qt::WindowMaximized);
    //logModule::logInit("logs.log",10000);
    //LOG_INFO << "系统开始运行";
    return a.exec();
}
