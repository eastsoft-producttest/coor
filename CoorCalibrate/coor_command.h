﻿#ifndef COOR_COMMAND_H
#define COOR_COMMAND_H
#include <QString>

const QString sJCAddr = "999999999999";
const QString s645Addr = "000000000225";
namespace coor_command {

    //回收结果
    typedef struct{
        float fUa;
        float fUb;
        float fUc;
        double fIa;
        double fIb;
        double fIc;
        double fIns;
        float fPFa;
        float fPFb;
        float fPFc;
        float fPFt;

    }T_Data_Instant;//瞬时量
    typedef struct{
        float fUa;
        float fUb;
        float fUc;
    }T_Data_Volt_Block;//电压数据块
    typedef struct{
        float fIa;//每三个字节为一组数据
        float fIb;
        float fIc;
    }T_Data_Curr_Block;//电流数据块
    typedef struct{
        uint8_t flag;//0x91:sack, 0x94:数据
        uint8_t data_type;//0：无数据, 1:正向有功数据, 2：电压数据块， 3：电流数据块，4：瞬时量
        double dbPositivePower;//正向有功电量,data_type=1时有效
        T_Data_Volt_Block tDataVoltBlock;//data_type=2时有效
        T_Data_Curr_Block tDataCurrBlock;//data_type=3时有效
        T_Data_Instant tDataInstant;//瞬时量，data_type=4时有效
        QString barcode;        //MAC条码，data_type=5时有效
    }T_RCV_PACK;

    typedef struct{
        int Plat_No;    //表台号
        int MeterLoc;   //表位置
        int Year;
        int Month;
        int Day;
        int A_VoltError;    //A相电压误差
        int B_VoltError;    //B相电压误差
        int C_VoltError;    //C相电压误差
        int A_CurrError;    //A相电流误差
        int B_CurrError;
        int C_CurrError;
        int A_Ib1Error;     //标准电流1.0分相误差
        int B_Ib1Error;
        int C_Ib1Error;
        int A_Ib05Error;    //标准电流0.5分相误差
        int B_Ib05Error;
        int C_Ib05Error;
        int H_01I1Error;   //轻载1.0合相误差
    }T_COOR_SAVE_ERROR;     //向集中器内保存误差等信息*/

    T_RCV_PACK decode_rcv_pack(const QByteArray baIn);

    const QString sPhase[5] = {"0","33", "34", "35", "36"};

#ifdef METER_BOX_TERMINAL


    //进入场内模式
    QByteArray get_EnterArea_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //退出场内模式
    QByteArray get_QuitArea_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //获取瞬时量
    QByteArray get_instant_val_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //恢复出厂设置
    QByteArray get_reset_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
#else
    //初始化
    QByteArray get_initial_cmd(const QString &sAddr = sJCAddr, bool bAddrReverse = true);
    //获取集中器条码
    QByteArray get_barcode_cmd();
    //获取电压数据块值
    QByteArray get_instant_volt_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //获取电流数据块值
    QByteArray get_instant_curr_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //校准电压电流值
    QByteArray set_volt_and_curr_cmd(const QString &sAddr = sJCAddr, int nPhaseIndex = 0, float fCurr = 0.0, bool bAddrReverse = true);
    //开始校准功率比差
    QByteArray set_init_power_ratio_differ_cmd(const QString &sAddr = sJCAddr, int nPhaseIndex = 0, bool bAddrReverse = true);
    //校准功率比差
    QByteArray set_power_ratio_differ_cmd(const QString &sAddr = sJCAddr, int nPhaseIndex = 0, float fDiff = 0.0, bool bAddrReverse = true);
    //开始校准功率相差
    QByteArray set_init_cali_phase_diff_cmd(const QString &sAddr = sJCAddr, int nPhaseIndex = 0, bool bAddrReverse = true);
    //校准功率相差
    QByteArray set_cali_phase_diff_cmd(const QString &sAddr = sJCAddr, int nPhaseIndex = 0, float fDiff = 0.0, bool bAddrReverse = true);
    //读电量（00010000）
    QByteArray get_read_meter_cmd(const QString &sAddr = "000000000225", bool bAddrReverse = true);
    //存储误差
    QByteArray save_cali_error_cmd(T_COOR_SAVE_ERROR tError);
#endif
}

#endif // COOR_COMMAND_H
