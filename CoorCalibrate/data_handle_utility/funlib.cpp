﻿#include "funlib.h"
#include <QTime>
#include <qDebug>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

static unsigned short fcstab[256] = {
0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

unsigned char chksum (const unsigned char *data, int len)
{
  unsigned char cs = 0;

  while( len-- > 0 )
    cs += *data++;
  return(cs);
}

void StrToBcd_desc(QString originstr, unsigned char *targetstr, int &size)
{
    originstr = originstr.replace(" ","");
    originstr = originstr.toUpper();

    QByteArray ba = originstr.toLatin1();
    AsciiToBcd_desc(ba.data(), targetstr, size);
}

void BcdToStr_desc(const unsigned char *originstr, int size, QString &str)
{
    char targetstr[1024];

    BcdToAscii_desc(originstr, size, targetstr);
    str = QString(QLatin1String(targetstr));
}

void StrToBcd_asc(QString originstr, unsigned char *targetstr, int &size)
{
    originstr = originstr.toUpper();

    QByteArray ba = originstr.toLatin1();
    AsciiToBcd_asc(ba.data(), targetstr, size);
}

void BcdToStr_asc(const unsigned char *originstr, int size, QString &str)
{
    char targetstr[1024];

    BcdToAscii_asc(originstr, size, targetstr);
    str = QString(QLatin1String(targetstr));
}

void AsciiToBcd_desc(const char *originstr, unsigned char *targetstr, int &size)
{
    unsigned char bcd;
    int			  times, pos;
    size = 0;
    pos   = strlen(originstr) - 1;
    times = strlen(originstr) / 2;

    for (int i = 0; i < times; ++i, pos -= 2)
    {
        if (originstr[pos - 1] - 0x41 < 0)	//	高位是数字
        {
            if (originstr[pos] - 0x41 < 0)	//	低位是数字
            {
                bcd  = (originstr[pos - 1] - 0x30) << 4;
                bcd |= originstr[pos] - 0x30;
                targetstr[size++] = bcd;
            }
            else	//	低位是字母
            {
                bcd  = (originstr[pos - 1] - 0x30) << 4;
                bcd |= originstr[pos] - 0x37;
                targetstr[size++] = bcd;
            }
        }
        else	//	高位是字母
        {
            if (originstr[pos] - 0x41 < 0)	//	低位是数字
            {
                bcd  = (originstr[pos - 1] - 0x37) << 4;
                bcd |= originstr[pos] - 0x30;
                targetstr[size++] = bcd;
            }
            else	//	低位是字母
            {
                bcd  = (originstr[pos - 1] - 0x37) << 4;
                bcd |= originstr[pos] - 0x37;
                targetstr[size++] = bcd;
            }
        }
    }
}

void BcdToAscii_desc(const unsigned char *originstr, int size, char *targetstr) //16????×a×?·?′?
{
    int tarpos = 0;
    for (int i = 0, pos = size - 1; i < size; ++i, --pos)
    {
        if (originstr[pos] >> 4 <= 9)	//	高位是数字
        {
            if ((originstr[pos] & 0x0F) <= 9)	//	低位是数字
            {
                targetstr[tarpos++] = (originstr[pos] >> 4) + 0x30;
                targetstr[tarpos++] = (originstr[pos] & 0x0F) + 0x30;
            }
            else //	低位是字母
            {
                targetstr[tarpos++] = (originstr[pos] >> 4) + 0x30;
                targetstr[tarpos++] = (originstr[pos] & 0x0F) + 0x37;
            }
        }
        else	//	高位是字母
        {
            if ((originstr[pos] & 0x0F) <= 9)	//	低位是数字
            {
                targetstr[tarpos++] = (originstr[pos] >> 4) + 0x37;
                targetstr[tarpos++] = (originstr[pos] & 0x0F) + 0x30;
            }
            else	//	低位是字母
            {
                targetstr[tarpos++] = (originstr[pos] >> 4) + 0x37;
                targetstr[tarpos++] = (originstr[pos] & 0x0F) + 0x37;
            }
        }
    }

    targetstr[tarpos] = 0;
}

void AsciiToBcd_asc(const char *originstr, unsigned char *targetstr, int &size)
{
    unsigned char bcd;
    int			  times;
    size = 0;
    times = strlen(originstr) / 2;

    for (int i = 0; i < times; ++i)
    {
        if (originstr[i*2] - 0x41 < 0)	//	高位是数字
        {
            if (originstr[i*2+1] - 0x41 < 0)	//	低位是数字
            {
                bcd  = (originstr[i*2] - 0x30) << 4;
                bcd |= originstr[i*2+1] - 0x30;
                targetstr[size++] = bcd;
            }
            else	//	低位是字母
            {
                bcd  = (originstr[i*2] - 0x30) << 4;
                bcd |= originstr[i*2+1] - 0x37;
                targetstr[size++] = bcd;
            }
        }
        else	//	高位是字母
        {
            if (originstr[i*2+1] - 0x41 < 0)	//	低位是数字
            {
                bcd  = (originstr[i*2] - 0x37) << 4;
                bcd |= originstr[i*2+1] - 0x30;
                targetstr[size++] = bcd;
            }
            else	//	低位是字母
            {
                bcd  = (originstr[i*2] - 0x37) << 4;
                bcd |= originstr[i*2+1] - 0x37;
                targetstr[size++] = bcd;
            }
        }
    }
}

void BcdToAscii_asc(const unsigned char *originstr, int size, char *targetstr) //16????×a×?·?′?
{
    int tarpos = 0;
    for (int i = 0; i < size; ++i)
    {
        if (originstr[i] >> 4 <= 9)	//	高位是数字
        {
            if ((originstr[i] & 0x0F) <= 9)	//	低位是数字
            {
                targetstr[tarpos++] = (originstr[i] >> 4) + 0x30;
                targetstr[tarpos++] = (originstr[i] & 0x0F) + 0x30;
            }
            else //	低位是字母
            {
                targetstr[tarpos++] = (originstr[i] >> 4) + 0x30;
                targetstr[tarpos++] = (originstr[i] & 0x0F) + 0x37;
            }
        }
        else	//	高位是字母
        {
            if ((originstr[i] & 0x0F) <= 9)	//	低位是数字
            {
                targetstr[tarpos++] = (originstr[i] >> 4) + 0x37;
                targetstr[tarpos++] = (originstr[i] & 0x0F) + 0x30;
            }
            else	//	低位是字母
            {
                targetstr[tarpos++] = (originstr[i] >> 4) + 0x37;
                targetstr[tarpos++] = (originstr[i] & 0x0F) + 0x37;
            }
        }
    }

    targetstr[tarpos] = 0;
}

QString AdjAddr(int num, QString str)
{
    QString defaultnum = "",src=str;

    for (int i=0;i<num;i++)
        defaultnum +="0";
    if(str.length()>num)
        src = str.right(num);
    else if(str.length()<num)
        src = defaultnum.left(num-str.length())+str;

    return src;
}

int ROUTER_decry(unsigned char *frm, int len)
{
    unsigned char csum = 0x00, cxor = 0x00;

    if (len < ROUTER_DEFAULT_LEN || frm[0] != 0x9F)
        return -1;

    /* decry */

    for (int i = len - 3; i > 3; i--)
        frm[i] ^= frm[i - 1];
    frm[3] ^= frm[len - 2] ^ frm[len - 1];

    for (int i = 3; i < len - 2; ++i)                               /* check csum and cxor */
    {
        csum += frm[i];
        cxor ^= frm[i];
    }

    if (csum != frm[len - 2] || cxor != frm[len - 1])
        return -1;

    return 0;
}

int ROUTER_check(unsigned char *frm, int len)
{
    unsigned char tmp[65536];
    unsigned short datalen;

    if (frm[0] != 0x9F)
        return -1;
    datalen = (frm[2] << 8) | frm[1];
    datalen = (datalen - 0x0A5A) ^ 0x2129;
    if (datalen < 1)
        return -1;
    if (datalen + 5 != len)
        return -1;
    memcpy(tmp, frm, len);

    if (ROUTER_decry(tmp, len) == 0)
        return 0;

    return -1;
}

void org_uart_frame(unsigned char ctrl, UString &cmd) //×é?ˉuart±¨??
{
    unsigned char s = 0x9F;
    cmd.insert(0, &s, 1); // 0x9F
    s = ctrl;
    cmd.insert(1, &s, 1); // ctrl
    s = 0x00;
    cmd.insert(2, &s, 1); //info
    cmd.insert(3, &s, 1);
    unsigned char cmdlen[] = {0x00, 0x00};//len
    *(short int *) cmdlen = cmd.length() - 1;
    cmd.insert(1, cmdlen, 2);
    unsigned char csum = 0x00, cxor = 0x00;

    for (int i = 3; i < cmd.length(); i++)
    {
        csum += cmd.at(i);
        cxor ^= cmd.at(i);
    }

    cmd += csum;
    cmd += cxor;

    short int Len = *(short int *) cmd.substr(1, 2).c_str();
    Len = (Len ^ 0x2129) + 0x0A5A;
    cmd[1] = Len & 0xFF;
    cmd[2] = (Len >> 8) & 0xFF;

    unsigned char Seed = csum ^ cxor;
    for(int i = 3; i < cmd.length() - 2; i++)
    {
        Seed = Seed^cmd[i];
        cmd[i] = Seed;
    }
}

void displayLog(QTextEdit *list, QString type, QString str)
{
    QString preText,time;
    time = (QTime::currentTime()).toString("hh:mm:ss:zzz ");
    preText =  time;
    preText += type + " ";
    preText += str;

    if(type == "接收")
    {
        list->setTextColor(QColor(255, 0, 0));
    }
    else if(type == "发送")
    {
        list->setTextColor(QColor(0, 0, 255));
    }
    else
    {
        list->setTextColor(QColor(0, 0, 0));
    }

    //list->moveCursor(QTextCursor::End);
    list->append(preText);
}

void outputexecle(QTableWidget *tableWidget)
{
    QString fileName = QFileDialog::getSaveFileName(NULL,
            "Save",
            "报文信息", "Excel 文件(*.xls *.xlsx)");
//            tr("All Files (*.txt)"));
    QString row_str;
    if (!fileName.isNull())//fileName是文件名
    {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly|QIODevice::Text))
        {
            QMessageBox::information(NULL, "提示", "无法创建文件");
            return;
        }
        QTextStream out(&file);

        QString header = "";
        for(int i=0;i<tableWidget->columnCount();i++)
        {
            header += tableWidget->horizontalHeaderItem(i)->text();
            header += "\t";
        }
        out<<header<<endl;

        for (int i=0;i<tableWidget->rowCount();i++)
        {
            row_str.clear();
            for(int j = 0;j<tableWidget->columnCount();j++)
            {
                if(tableWidget->item(i,j) == 0)
                {
                    row_str +="\t";
                    break;
                }
                row_str += tableWidget->item(i,j)->text();
                row_str +="\t";
            }
            out<<row_str<<endl;
        }
        out.flush();
        file.close();
    }
    else
    {

    }
}

unsigned short pppfcs16(unsigned short fcs, unsigned char cp[], unsigned int len)
{
   while (len--)
   {
       fcs = (fcs >> 8) ^ fcstab[(fcs ^ *cp++) & 0xff];
   }

   return (fcs);
}

unsigned short get_fcs16(unsigned char cp[], unsigned int len)
{
    unsigned short trialfcs;

    trialfcs = pppfcs16(PPPINITFCS16, cp, len);
    trialfcs ^= 0xffff;

    return trialfcs;// /* complement */
}

unsigned short crc16(const unsigned char *frm, int dataLen)
{
  unsigned short crc = 0xFFFF;

  while (dataLen--)
    {
      crc ^= *frm;
      for (int i = 0; i < 8; i ++)
        {
          if ((crc & 0x0001) != 0)
            {
              crc >>= 1;
              crc ^= 0x8408;
            }
          else
            crc >>= 1;
        }

      ++frm;
    }

  return crc;
}

int Update55_decry(unsigned char *frm, int len)
{
    if (frm[0] != 0x7F)
        return -1;

    /* decry */

    unsigned short crc = (frm[len-1] << 8) | frm[len-2];
    unsigned char seed = ((crc >> 8) & 0xFF) ^ ((crc) & 0xFF);
    for (int i = len - 3; i > 8; i--)
        frm[i] ^= frm[i - 1];
    frm[8] ^= seed;

    unsigned short tcrc = crc16(frm+8,len-10);

    if (tcrc != crc)
    {
         qDebug()<<"Update55_check tcrc error";
        return -1;
    }

    return 0;
}



int Update55_check(unsigned char *frm, int len)
{
    unsigned char tmp[65536];
    unsigned short datalen;

    if (frm[0] != 0x7F)
        return -1;
    if (len < 10)
        return -1;
    datalen = (frm[3] << 8) | frm[2];
    datalen = (datalen - 0x0A5A) ^ 0x2129;  //cxh

    if (datalen < 1)
        return -1;
    if (datalen + 10 != len)
        return -1;

    memcpy(tmp, frm, datalen + 10);
    if (Update55_decry(tmp, datalen + 10) == 0)
        return 0;
    return -1;
}


//字符串按照字节倒序
QString reverseAddr(QString str)
{
    QString src="";
    int num=str.length();

    for (int i=2;(num-i)>=0;i=i+2)
    {

        src+=str.right(2);
        str=str.left(num-i);
    }


    return src;
}
//字符串转字节加33h
QString Add33(QString str)
{
    QString src="";
    QString cstr="";
    unsigned char data[500];
    int dlen = 0;
    StrToBcd_asc(str.remove(' '),data,dlen);
    unsigned char cs = 0;
    for (int i=1;(dlen-i)>=0;i++)
    {
       cs= *data+0x33;
       cstr=cstr+src.sprintf("%.02X",cs);
       *data=*(data+i);
    }

    return cstr;
}
//字符串转字节减33h
QString Dec33(QString str)
{
    QString src="";
    QString cstr="";
    unsigned char data[500];
    int dlen = 0;
    StrToBcd_asc(str.remove(' '),data,dlen);
    unsigned char cs = 0;
    for (int i=1;(dlen-i)>=0;i++)
    {
       cs= *data-0x33;
       cstr=cstr+src.sprintf("%.02X",cs);
       *data=*(data+i);
    }

    return cstr;
}

//字符串加空格
QString AddSpace(QString recstr)
{
    int len=recstr.length();
    QString str="";
    for (int i =2;(len-i)>=0; i=i+2)
    {
        str=str+recstr.left(2)+" ";
        recstr=recstr.right(len-i);
    }
    return str;

}

QString Error2Byte(QString str, int Error)
{
    if(Error >= 0)
    {
        uchar low, high;
        low = Error + 0x33;
        high = (Error >> 8) + 0x33;

        str += QString(" %1").arg(low, 2, 16, QLatin1Char('0'));
        str += QString(" %1").arg(high, 2, 16, QLatin1Char('0'));
    }
    else
    {
        Error = 0 - Error;
        uchar low, high;
        low = Error + 0x33;
        high = (Error >> 8) + 0x33;
        high |= 0x80;

        str += QString(" %1").arg(low, 2, 16, QLatin1Char('0'));
        str += QString(" %1").arg(high, 2, 16, QLatin1Char('0'));
    }

    return str;
}
