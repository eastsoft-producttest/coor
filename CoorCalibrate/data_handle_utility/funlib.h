﻿#ifndef COMFUNC_H
#define COMFUNC_H

#include <QString>
#include <QTextEdit>
#include <QTableWidget>

#define PPPINITFCS16 0xffff

#define ROUTER_DEFAULT_LEN 8
typedef std::basic_string<unsigned char> UString;

unsigned char chksum (const unsigned char *data, int len);

void StrToBcd_desc(QString originstr, unsigned char *targetstr, int &size);

void BcdToStr_desc(const unsigned char *originstr, int size, QString &str);

void StrToBcd_asc(QString originstr, unsigned char *targetstr, int &size);

void BcdToStr_asc(const unsigned char *originstr, int size, QString &str);

void AsciiToBcd_desc(const char *originstr, unsigned char *targetstr, int &size);

void BcdToAscii_desc(const unsigned char *originstr, int size, char *targetstr);

void AsciiToBcd_asc(const char *originstr, unsigned char *targetstr, int &size);

void BcdToAscii_asc(const unsigned char *originstr, int size, char *targetstr);

QString AdjAddr(int num, QString str);

int ROUTER_decry(unsigned char *frm, int len);

int ROUTER_check(unsigned char *frm, int len);

void org_uart_frame(unsigned char ctrl, UString &cmd);

void displayLog(QTextEdit *list, QString type, QString str);

void outputexecle(QTableWidget *tableWidget);

unsigned short get_fcs16(unsigned char cp[], unsigned int len);

unsigned short crc16(const unsigned char *frm, int dataLen);

int Update55_check(unsigned char *frm, int len);

QString reverseAddr(QString str);
QString Add33(QString str);
QString Dec33(QString str);
QString AddSpace(QString recstr);
QString Error2Byte(QString str, int Error); //将误差转为两字节的hex并加到str上
#endif // COMFUNC_H
