﻿#ifndef UI_GENERALSET_H
#define UI_GENERALSET_H

#include <QDialog>
#include <QMessageBox>
#include "ini_profile.h"
#include "sigletonsharer.h"
namespace Ui {
class ui_GeneralSet;
}

class ui_GeneralSet : public QDialog
{
    Q_OBJECT

public:
    explicit ui_GeneralSet(QWidget *parent = 0);
    ~ui_GeneralSet();

private slots:
    void on_pb_save_clicked();

    void on_pb_cancel_clicked();

private:
    Ui::ui_GeneralSet *ui;
    ini_profile* m_ini;
    SigletonSharer *m_pSharer;

signals:
    void sig_close();
};

#endif // UI_GENERALSET_H
