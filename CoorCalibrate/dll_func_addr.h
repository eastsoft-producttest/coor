#ifndef DLL_FUNC_ADDR_H
#define DLL_FUNC_ADDR_H
#include <minwindef.h>

#define DLL_Byte unsigned char

typedef bool ( *P_Power_Off)(int); //定义降电压电流函数
typedef bool ( *P_Error_Clear)(int); //定义误差总清函数
// HINSTANCE hDLL;
typedef bool ( *P_Error_Read)(char**,int,int);//定义读误差函数指针
typedef bool ( *P_StdMeter_Read)(char**, char*,int); //定义指示仪表读取函数
typedef void ( *P_Port_Close)(); //定义关闭设备串口函数
typedef bool ( *P_Adjust_UI)(int,double,double,
                             double,int,int,double,
                             double,char*,char*,char*,int) ; //定义负载点调整函数
typedef bool ( *P_Power_Pause)(int); //定义降电流，电压保持函数指针
typedef bool ( *P_Set_Pulse_Channel)(int,int,int);//定义脉冲采样通道切换指针
typedef bool ( *P_Error_Start)(int,double,int,int);//定义开始测试误差指针
typedef bool ( *P_Adjust_CUST)(int,double,double,
                               double,double,double,
                               double,double,double,
                               double,double,double,double,
                               char*,int); //定义负载点调整(可任意设定,四线状态)函数
typedef bool ( *P_Open485CommPort)(DWORD,char *,unsigned char);//定义打开485通信口函数
typedef bool ( *P_Close485CommPort)(DWORD);//定义关闭485通信口函数
typedef bool ( *P_Send485Data)(DWORD,char*);//定义发送485数据函数
typedef bool ( *P_Recv485Data)(DWORD,char *,int);//定义接收485数据函数
typedef bool ( *P_SetRS485On)(int,int,int);//定义设置终端RS485接入函数

typedef bool (*PSetRefClock)(DLL_Byte, DLL_Byte);//标准时钟仪切换
typedef bool (*PClock_Error_Start)(int, double, int, int);//开始测试时钟误差
typedef bool (*PClock_Error_Read)(char**, double, int, int, int);//读取时钟误差

typedef bool ( *P_ConstStart)(int,double,int);//定义设置终端RS485接入函数
#endif // DLL_FUNC_ADDR_H
