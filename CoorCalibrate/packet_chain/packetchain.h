#ifndef PACKETCHAIN_H
#define PACKETCHAIN_H

#include <QString>
#include <QDebug>

#define PROCNT 1//5//5
class PacketChain;
struct appinfo
{
    int (PacketChain::*check)(unsigned char *frm, int len, int *end); /* check function of protocol */
    int (PacketChain::*proc)(unsigned char *frm, int len); /* protocol process */
};
struct proinfo //cxh
{
    int frmlen;
    int startindex;
};

class PacketChain
{
public:
    PacketChain();
    ~PacketChain();
public:
    /*
     * 功能：从串口缓存获取报文
     * 输入：buffer，缓存
     *      bufflen，缓存长度
     * 输出：packet，报文，空指针
     *      paktlen，报文长度
     *      buffoffset, 缓存向后偏移
     */
	void GetPacket(const char* buffer, unsigned int bufflen, char* packet, unsigned int *pktlen, unsigned int *buffoffset);

    //int aprtcl;
    struct appinfo apps[PROCNT];

    int app_check(int index, unsigned char *frame, int len, int *end);

    int is_valid645(unsigned char *frm, int len);
    int GB645_check(unsigned char *frm, int len, int *end);
    int GB645_proc(unsigned char *frm, int len);

    int is_valid3762(unsigned char *frm, int len);
    int GW3762_check(unsigned char *frm, int len, int *end);
    int GW3762_proc(unsigned char *frm, int len);
    unsigned char chksum (const unsigned char *data, int len);

};

#endif // PACKETCHAIN_H
