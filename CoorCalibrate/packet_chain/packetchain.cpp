#include "packetchain.h"


#define STARTCH             0x68
#define ENDCH               0x16
#define MAXBUF              0x800
#define GW3762_DEFAULT_LEN  12

PacketChain::PacketChain()
{
    //aprtcl = -1;

    struct appinfo tapps[] = {
                              {&PacketChain::GB645_check, &PacketChain::GB645_proc},

                                };
    for (int i = 0; i < sizeof(tapps)/sizeof(tapps[0]); i++)
    {
        apps[i] = tapps[i];
    }
}

PacketChain::~PacketChain()
{

}

void PacketChain::GetPacket(const char* buffer, unsigned int bufflen, char* packet, unsigned int *pktlen, unsigned int *buffoffset)
{
	int end=0, start=0, frmlen=0;

    *pktlen = 0;
    *buffoffset = 0;

    struct proinfo pros[PROCNT];
    for (int i=0; i < sizeof(pros)/sizeof(pros[0]); ++i)
    {
        pros[i].frmlen = -1;
        pros[i].startindex = 0;
    }

    for (int i = 0; i < sizeof(apps)/sizeof(apps[0]); ++i)
    {
        if ((frmlen = app_check(i, (unsigned char *)buffer, bufflen, &end)) >= 0)
        {
            pros[i].frmlen = frmlen;
            pros[i].startindex = end - frmlen;

            //qDebug()<<"i"<<i<<"frmlen"<<pros[i].frmlen<<"startindex"<<pros[i].startindex;
        }
    }

    frmlen = -1; start = -1; end = -1;
    for (int i=0; i < sizeof(pros)/sizeof(pros[0]); ++i)
    {
        if (pros[i].frmlen < 0)
            continue;
        if (frmlen == -1)
        {
            frmlen = pros[i].frmlen;
            start = pros[i].startindex;
            continue;
        }
        if (pros[i].startindex < start)
        {
            frmlen = pros[i].frmlen;
            start = pros[i].startindex;
            continue;
        }
        if (pros[i].startindex == start)
        {
            if (pros[i].frmlen > 0) //优先
            {
                frmlen = pros[i].frmlen;
                start = pros[i].startindex;
            }
        }
    }

    //qDebug()<<"frmlen"<<frmlen<<"start"<<start;
    if (frmlen > 0)
    {
        memcpy(packet, buffer+start, frmlen);
        *pktlen = frmlen;
        *buffoffset = start+frmlen;
    }
}

int PacketChain::app_check(int index, unsigned char *frame, int len, int *end)
{
    int frmlen = 0;

    frmlen = (this->*apps[index].check)(frame, len, end);
    //aprtcl = -1;
    if (frmlen > 0)
        (this->*apps[index].proc)(frame, frmlen);
    //else if (frmlen == 0)// && uart_get_tick(CHAN_APP) > UART_OVER_TICK - 10)
    //    aprtcl = index;

   return frmlen;
}

int PacketChain::GB645_check(unsigned char *frm, int len, int *end)
{
    int datalen = 0, i = 0;
    int result = -1;

    for (i = 0; i < len; ++i)
    {
        if ((datalen = is_valid645(frm + i, len - i)) < 0)
            continue;
        /* cxh
        if (datalen == 0)
        {
            result = 0;
            continue;
        }
        */

        *end = i + datalen;
        return datalen;
    }

    return result;
}

int PacketChain::GB645_proc(unsigned char *frm, int len)
{
    return 0;
}

int PacketChain::GW3762_check(unsigned char *frm, int len, int *end)
{
    int datalen = 0, i = 0;
    int result = -1;

    for (i = 0; i < len; ++i)
    {
        if ((datalen = is_valid3762(frm + i, len - i)) < 0)
            continue;
        if (datalen == 0)
        {
            if (result == -1)
            {
                *end = i; //cxh
            }
            result = 0;
            continue;
        }

        *end = i + datalen;
        return datalen;
    }

    return result;
}

int PacketChain::GW3762_proc(unsigned char *frm, int len)
{
    return 0;
}

unsigned char PacketChain::chksum (const unsigned char *data, int len)
{
    unsigned char cs = 0;

    while( len-- > 0 )
        cs += *data++;
    return(cs);
}

int PacketChain::is_valid645(unsigned char *frm, int len)
{
    int datalen, index = 0;
    unsigned char *msg;

    if(len < 12) return -1;
    msg = frm;
    datalen = msg[9];
    if (msg[0] != 0x68 || msg[7] != 0x68)
        return -1;
    if (index + datalen + 12 > len)
        return -2;//return 0; //cxh
    if (msg[datalen + 11] != 0x16)
        return -3;
    if (msg[datalen + 10] != chksum(msg, datalen + 10))
        return -4;
    return index + datalen + 12;
}

int PacketChain::is_valid3762(unsigned char *frm, int len)
{
    if (len < GW3762_DEFAULT_LEN ||frm[0] != 0x68)//
    {
        return -1;
    }
    int dlen = (frm[2] << 8) | frm[1];
    if (dlen < GW3762_DEFAULT_LEN)
    {
        return -1;
    }

    if (dlen > len)
    {
        return 0;
    }
    if (frm[dlen - 2] != chksum(frm + 3, dlen - 5))
    {
        return -1;
    }
    if (frm[dlen - 1] != 0x16)
    {
        return -1;
    }

    return dlen;
}


